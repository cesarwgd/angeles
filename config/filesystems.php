<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'revista' => [
            'driver' => 'local',
            'root' => storage_path('app/revista'),
        ],

        'users' => [
            'driver' => 'local',
            'root' => public_path('images/users'),
            'url' => env('APP_URL').'/images/users',            
            'visibility' => 'public',
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => 'https://www.angelesrevista.com/storage',//env('APP_URL').'/storage', 
            'visibility' => 'public',
        ],

        'blog' => [
            'driver' => 'local',
            'root' => public_path('images/blog'),
            'url' => 'https://www.angelesrevista.com/images/blog',//env('APP_URL').'/images/blog',            
            'visibility' => 'public',
        ],

        'home' => [
            'driver' => 'local',
            'root' => public_path('images/home'),
            'url' => 'https://www.angelesrevista.com/images/home',//env('APP_URL').'/images/home',            
            'visibility' => 'public',
        ],

        'hotmas' => [
            'driver' => 'local',
            'root' => public_path('images/angeles'),
            'url' => 'https://www.angelesrevista.com/images/angeles',//env('APP_URL').'/images/angeles',            
            'visibility' => 'public',
        ],

        'products' => [
            'driver' => 'local',
            'root' => public_path('products'),
            'url' => env('APP_URL').'/products',//'https://www.angelesrevista.com/products/products',//env('APP_URL').'/products', 'https://www.angelesrevista.com/products'         
            'visibility' => 'public',
        ],

        'rev_thumbs' => [
            'driver' => 'local',
            'root' => public_path('ang/thumbnail'),
            'url' => 'https://www.angelesrevista.com/ang/thumbnail',//env('APP_URL').'/ang/thumbnail',            
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
        ],

    ],

];
