$(document).ready(function(){

	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

	// termino de cargar
	$(window).load(function(){
		$(".loading-issue").fadeOut();

		// cambiar el tipo de proporcion de imagen

		setWidthImageMobile();

	});

	$(window).resize(function(){
		setWidthImageMobile();
	});

	// mostrar todas sus revistas
	$(".issue-menu-box li.mis-revistas-show a").click(function(){
		if($(".choose-page").is(":visible")){
			$(".choose-page").hide();
			$(".choose-page").removeClass('show-their-issues');
		}
		$(".choose-issue").removeAttr('style');
		$(".choose-issue").toggleClass('show-their-issues');
		
		$(this).parents(".issue-menu-box").removeClass('black-issuem-bg2');
		$(this).parents(".issue-menu-box").toggleClass('black-issuemenu-bg');
		
		return false;
	});

	// mostrar paginas

	$(".issue-menu-box li.show-revista-paginas a").click(function(){
		if($(".choose-issue").is(":visible")){
			$(".choose-issue").hide();
			$(".choose-issue").removeClass('show-their-issues');
		}
		$(".choose-page").removeAttr('style');
		$(".choose-page").toggleClass('show-their-issues');
		$(this).parents(".issue-menu-box").removeClass('black-issuemenu-bg');
		$(this).parents(".issue-menu-box").toggleClass('black-issuem-bg2');
		return false;
	});

	// cerrar password
	$('.close-password').click(function(){
		$('.issue-video-password').fadeOut();
		return false;
	});

	function setWidthImageMobile(){
		
		var vpWidth = $(window).width();
		var vpHeight = $(window).height();
		var laImagen = $('.view-page img');
		var laImagenWidth = laImagen.width();
		var laImagenHeight = laImagen.height();

		if(isMobile && vpHeight > vpWidth){
			if(laImagenWidth > laImagenHeight){
				$('.view-page img').css({
					'height' : 'auto',
					'width' : '120%',
					'top' : '50%',
					'transform' : 'translate(-50%, -50%)'

				});
			}
		}else{
			$('.view-page img').removeAttr('style');
		}

	}

	// imageViewer para desktops

	if(!isMobile){
		var viewer = ImageViewer({
			snapView: false,
			maxZoom : 300
		});
	    $('.overlay-block').click(function () {
	        var imgSrc = $(".view-page img").attr('src');         
	 
	        viewer.show(imgSrc);
	    });
		
	}

	// full screen para mobiles

	/*function toggleFullScreen(imageElement) {
	    if (!document.mozFullScreen && !document.webkitFullScreen) {
	      if (imageElement.mozRequestFullScreen) {
	        imageElement.mozRequestFullScreen();
	      } else {
	        imageElement.webkitRequestFullScreen();
	      }
	    } else {
	      if (document.mozCancelFullScreen) {
	        document.mozCancelFullScreen();
	      } else {
	        document.webkitCancelFullScreen();
	      }
	    }
	  }

	if(isMobile){

	  var imageElement = document.getElementById("mobileFullScreen");  
	  var activador = $("a.open-fullimage-mobile");

	  activador.show();
	  
	  activador.click(function(){
	  	toggleFullScreen(imageElement);
	  	console.log("hi");
	  });

	}*/

});