$(document).ready(function(){


	$(window).resize(function(){

		// video del banner
		$(window).load(function(){

			var bannerW = $("#home-video-banner").width(),
				bBoxW = $(".banner-home-video").width();				
			
			if(bannerW > bBoxW){
				var videoLeft = (bannerW - bBoxW) / 2;
				$("#home-video-banner").css({'margin-left' : '-' + videoLeft + "px"});	
			} 			
			
			//videos del home
			$(".video-homepage").each(function(){
				var videoW = $(this).find('.video-ang-h').width(),
					boxW = $(this).width(),
					ancho = videoW - boxW;					

				if(ancho > 0){
					var left = ancho / 2;
					$(this).find('.video-ang-h').css({'left' : '-' + left + 'px'});
				}

			});		
			
		});


	}).resize();


	// hacemos que el video empiece al momento de poner el cursor encima solo si no es movil

	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

    if(!isMobile){   
        if($(".video-homepage").length > 0){
        	$(".video-homepage").mouseenter( function(){
	            //$(this).find('.imagen-home-s').hide(); 
	            $(this).find('.video-ang-h').get(0).play();	

	        }).mouseleave(function(){
	            $(this).find('.video-ang-h').get(0).pause(); 
	        });
        }
    }

    // portadas de videos para moviles

    // videos para home
	  $(window).load(function(){
	  	if(isMobile){
	  		var currentVideo;
	  		//remover el loop
	  		$('.video-ang-h').removeAttr('loop');
	  		$('.video-ang-h').attr('playsinline');

	  		//video del banner
	  		$(".video-portada, .banner-video-portada").show();
	  		//play video
	  		$(".video-portada a").click(function(){
	  			$(this).parent().hide();
	  			$(this).parents('.video-homepage').find('.video-ang-h').get(0).play();
	  			currentVideo = $(this).parents('.video-homepage').find('.video-ang-h').attr('id');	  			
	  			$("#"+currentVideo).on('ended', function(){
		  			$(this).parent().find('div.video-portada').show();		  			
		  		});
	  			return false;	
	  		});
	  		
	  	}
	  	
	  }); 


});