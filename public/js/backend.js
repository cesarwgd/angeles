$(document).ready(function(){
	// nav tabs

	$("ul.nav-tabs li a").click(function(){
		var tab = $(this).attr('href');

		$("ul.nav-tabs li").removeClass("active");
		$(".tab-box").hide();
		$(this).parent().addClass('active');

		$(tab).fadeIn();
		
		return false;
	});


});