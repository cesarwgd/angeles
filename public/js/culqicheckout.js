var form = $("#culqi-card-form");

form.submit(function(event) {
	form.find('button').prop('disabled', true);
    $('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Procesando compra<br>Por favor espere un momento..</div></div></div>");
	Culqi.createToken();																

	return false;
	
});

function culqi() {
    if(Culqi.token) { // ¡Token creado exitosamente!
        // Get the token ID:
        var token = Culqi.token.id;
        form.find('#culqi-token').val(token);        
        form.get(0).submit();

    }else{ // ¡Hubo algún problema!
        // Mostramos JSON de objeto error en consola
        //console.log(Culqi.error);
        //alert(Culqi.error.mensaje);
        $('body').find('.procesando-compra').remove();
        $('.culqi-errors').show().html(Culqi.error.merchant_message);

        form.find('button').prop('disabled', false);
    }
};