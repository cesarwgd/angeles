<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user/apprequest', 'Api\UserApiController@user');

Route::get('revistas', 'Api\RevistasApiController@revistasIndex');

Route::get('revistas/{id}', 'Api\RevistasApiController@show');

Route::get('revista/appview/{id}', 'Api\RevistasApiController@revistaView');*/

Route::prefix('v1')->group(function(){

Route::post('login', 'Api\AuthController@login');

Route::post('fb-login', 'Api\AuthController@loginFacebookApi');

Route::post('register', 'Api\AuthController@register');

Route::get('testuser/{id}', 'Api\AuthController@testUser');

//Route::get('user-revistas/{id}', 'Api\UserApicontroller@getUserRevistas');

//Route::get('revista/mobile/video/{hash_slug}', 'Api\RevistasApiController@revistaMobileVideo');



Route::group(['middleware' => 'auth:api'], function(){

 	Route::post('getUser', 'Api\AuthController@getUser');
 	Route::post('update-user-data', 'Api\UserApicontroller@updateUserData');

 	/*
		REVISTAS
 	*/

 	Route::get('revistas', 'Api\RevistasApiController@revistasIndex');

 	// falta un middleware adicional aca

 	Route::get('revista/files/{id}/{hash_slug}', 'Api\RevistasApiController@revistaView')->middleware('issue');

 	//Route::get('revista/files/{id}', 'Api\RevistasApiController@revistaView')->middleware('issue');

 	// la aplicacion no estaba cargando esta ruta bien hasta q le cambie de nombre y se arreglo no se porq, facil por el cache
	Route::get('image/{id}/{filename}/{extension}', 'Api\RevistasApiController@revistaEachImage')->middleware('mobile.image.auth');

 	//Route::get('revista/each/{filename}/{extension}', 'Api\RevistasApiController@revistaEachImage')->middleware('mobile.image.auth');

 	// revista mobiles

 	Route::get('revista/mobile/video/{hash_slug}', 'Api\RevistasApiController@revistaMobileVideo');

 	

 	// USER REVISTAS
 	Route::get('user-revistas', 'Api\UserApicontroller@getUserRevistas');

 	/*
		PRODUCTOS
 	*/

	Route::get('/productos', 'Api\ProductosApiController@productsIndex');

 	// PLACE ORDERS - REVISTAS

 	Route::post('place-revista-order', 'Api\RevistaOrderApiController@placeRevistaOrderMobile');

 	Route::post('remove-revista-order/{productId}', 'Api\RevistaOrderApiController@removeRevistaOrdeMobile');

 	

 	Route::get('user-orders', 'Api\RevistaOrderApiController@userOrdenesMobile');


 	// PLACER PRODUCTOS ORDERS

 	Route::post('place-producto-order', 'Api\ProductoOrderApiController@placeProductoOrderMobile');
 	Route::post('update-producto-order/{productId}', 'Api\ProductoOrderApiController@updateProductoOrderMobile');
 	Route::post('remove-producto-order/{productId}', 'Api\ProductoOrderApiController@removeProductoOrdeMobile');

 	// PAYMENT REVISTAS

 	Route::post('user-update-revista-orders', 'Api\UserApicontroller@updateUserOrderRevistas');

 	// PAYMENT PRODUCTOS

 	Route::post('user-update-productos-orders', 'Api\UserApicontroller@updateUserOrderProductos');

 });

});
