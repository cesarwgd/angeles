<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'Frontend\General@home', 'as' => 'home']);

//Route::get('/', ['uses' => 'Frontend\General@mantenimiento', 'as' => 'home']);

Route::get('/nosotros', ['uses' => 'Frontend\General@nosotros', 'as' => 'nosotros']);

Route::get('/contacto', ['uses' => 'Frontend\General@contacto', 'as' => 'contacto']);
Route::post('/contacto', ['uses' => 'Frontend\General@postContacto', 'as' => 'post.contacto']);

Route::get('/blog', ['uses' => 'Frontend\Blog@all', 'as' => 'blog']);
Route::get('/blog/{categorySlug}', ['uses' => 'Frontend\Blog@blogCategoryPage', 'as' => 'blog.category']);
Route::get('/blog/{category}/{slug}', ['uses' => 'Frontend\Blog@singlePost', 'as' => 'blog.single']);

Route::get('/terminos-y-condiciones', ['uses' => 'Frontend\General@terminos', 'as' => 'terminos']);

Route::get('/hot-max', ['uses' => 'Frontend\General@hotMax', 'as' => 'hot.max']);
Route::get('/angel/{slug}', ['uses' => 'Frontend\General@hotAngel', 'as' => 'hot.angel']);

Route::get('/revistas', ['uses' => 'Frontend\Revistas@allIssues', 'as' => 'revistas']);
Route::get('/revistas/busqueda', ['uses' => 'Frontend\Revistas@searchIssue', 'as' => 'buscar.revista']);

Route::get('/ultima-revista', ['uses' => 'Frontend\Revistas@latestIssue', 'as' => 'ultima.revista']);
Route::get('/revistas/{slug}', ['uses' => 'Frontend\Revistas@issuePreview', 'as' => 'revistas.preview']);

// PRODUCTOS

Route::get('/productos', ['uses' => 'Frontend\Products@allProducts', 'as' => 'frontend.productos']);
Route::get('/producto/{slug}', ['uses' => 'Frontend\Products@productDetail', 'as' => 'frontend.productos.detail']);
Route::get('/productos/busqueda', ['uses' => 'Frontend\Products@productSearchResult', 'as' => 'frontend.buscar.producto']);

/* LOGIN WITH FACEBOOK */

Route::get('/login/facebook', ['uses' => 'Auth\LoginController@redirectToProvider', 'as' => 'login.facebook']);
Route::get('/login/facebook/callback', ['uses' => 'Auth\LoginController@handleProviderCallback', 'as' => 'callback.login.facebook']);
// si es que no le dio autorizacion para grab el email
Route::get('/login/facebook/rerequest', ['uses' => 'Auth\LoginController@redirectAgainToFb', 'as' => 'login.rerequest.facebook']);

// URL DE LA REVISTA DEL STORAGE

Route::get('revista/public_image/{filename}/{extension}', ['uses' => 'Frontend\Revistas@getRevistaPublicImage', 'as' => 'frontend.revista.publicimage']);




/* USERS - FRONTEND */

Route::group(['middleware' => 'auth'], function(){

	Route::get('/ayuda', ['uses' => 'Frontend\General@ayuda', 'as' => 'ayuda']);

	Route::get('/user/perfil', ['uses' => 'Frontend\Users@perfil', 'as' => 'user.profile']);
	Route::get('/user/revistas', ['uses' => 'Frontend\Users@userRevistas', 'as' => 'user.revistas']);
	Route::get('/user/tickets', ['uses' => 'Frontend\Users@userTickets', 'as' => 'user.tickets']);
	Route::get('/user/tickets/create', ['uses' => 'Frontend\Users@userTicketCreate', 'as' => 'user.ticket.create']);
	Route::post('/user/tickets/create', ['uses' => 'Frontend\Users@postUserTicket', 'as' => 'user.ticket.post']);

	Route::get('/user/ticket/{id}', ['uses' => 'Frontend\Users@userTicketView', 'as' => 'user.ticket.view'])->middleware('user.ticket');
	Route::post('/user/ticket/{id}', ['uses' => 'Frontend\Users@postTicketReply', 'as' => 'user.ticket.reply']);

	/* User Tasks */

	Route::post('/user/update', ['uses' => 'Frontend\Users@updateUserInfo', 'as' => 'update.user']);
	Route::post('/user/delete', ['uses' => 'Frontend\Users@deleteUser', 'as' => 'delete.user']);

	/* USER FRONTEND REVISTAS PROTECTED BY MIDDLEWARE*/

	Route::get('/issue/{hash_slug}/{page}', ['uses' => 'Frontend\Revistas@watchIssue', 'as' => 'watch.issue'])->middleware('issue');
	Route::get('/issue/{hash_slug}/video/show', ['uses' => 'Frontend\Revistas@watchIssueVideo', 'as' => 'watch.issue.video'])->middleware('issue');
	Route::get('revista/private_image/{filename}/{extension}', ['uses' => 'Frontend\Revistas@getRevistaPrivateImage', 'as' => 'frontend.revista.privateimage']);

	// video for mobile
	//Route::get('/mobile-video-app/{userId}/{revistaId}/show', ['uses' => 'Frontend\Revistas@watchMobileIssueVideo', 'as' => 'watch.issue.mobile.video'])->middleware('mobile.video.auth');
	


	/* ORDERS*/

	Route::get('/user/ordenes', ['uses' => 'Frontend\Users@userOrdenes', 'as' => 'user.ordenes']);


	/*	CHECKOUT   */

	Route::post('/placeorder/', ['uses' => 'Frontend\Checkout@addIssueOrder', 'as' => 'user.place.order']);
	Route::get('/checkout/issue', ['uses' => 'Frontend\Checkout@checkout', 'as' => 'user.checkout']);
	// Remover del carrito de compras
	Route::post('/removeorder/', ['uses' => 'Frontend\Checkout@removeFromCart', 'as' => 'user.remove.order']);
	// Pago a deposito de cuenta bancaria
	Route::post('/bankdeposit/', ['uses' => 'Frontend\Checkout@bankDepositCheckout', 'as' => 'user.bankdeposit.checkout']);
	Route::get('/order-placed/bank-deposit', ['uses' => 'Frontend\Checkout@orderPlacedBankdeposit', 'as' => 'user.orderplaced.bank']);
	// PAGO CON CULQI
	Route::post('/culqi/payment', ['uses' => 'Frontend\Checkout@postCulqiPayment', 'as' => 'user.culq.payment']);

	/*PRODUCT CHEKCOUT*/

	Route::post('/product/placeorder/', ['uses' => 'Frontend\ProductCheckout@addIssueOrder', 'as' => 'user.product.place.order']);
	Route::get('/checkout/product', ['uses' => 'Frontend\ProductCheckout@checkout', 'as' => 'user.product.checkout']);
	// Remover del carrito de compras
	Route::post('/product/removeorder/', ['uses' => 'Frontend\ProductCheckout@removeFromCart', 'as' => 'user.product.remove.order']);
	
	// PAGO CON CULQI
	Route::post('/culqi/product/payment', ['uses' => 'Frontend\ProductCheckout@postCulqiPayment', 'as' => 'user.culqi.product.payment']);

});

/*	BACKEND  */

Route::get('administrador/login', ['uses' => 'Backend\Auth@backendLogin', 'as' => 'backend.login']);
Route::post('administrador/login', ['uses' => 'Backend\Auth@login', 'as' => 'backend.post.login']);

Route::group(['prefix' => 'administrador', 'middleware' => 'cms'], function(){

	Route::post('logout', ['uses' => 'Backend\Auth@logout', 'as' => 'backend.logout']);

	Route::get('dashboard', ['uses' => 'Backend\General@dashboard', 'as' => 'backend.dashboard']);



	// ANGELES

	Route::get('modelos', ['uses' => 'Backend\Angeles@all', 'as' => 'backend.modelos']);
	Route::get('modelos/search/result', ['uses' => 'Backend\Angeles@searchResult', 'as' => 'backend.modelos.searchresult']);

	Route::get('modelos/create', ['uses' => 'Backend\Angeles@create', 'as' => 'backend.modelos.create']);
	Route::post('modelos/create', ['uses' => 'Backend\Angeles@postCreate', 'as' => 'backend.modelos.save']);

	Route::get('modelo/view/{id}', ['uses' => 'Backend\Angeles@update', 'as' => 'backend.modelo.view']);
	Route::post('modelo/view/{id}', ['uses' => 'Backend\Angeles@postUpdate', 'as' => 'backend.modelo.update']);
	
	Route::get('modelo/delete/{id}', ['uses' => 'Backend\Angeles@delete', 'as' => 'backend.modelo.delete']);

	Route::get('modelo/{id}/images', ['uses' => 'Backend\Angeles@angelImages', 'as' => 'backend.modelo.images']);
	Route::post('modelo/{id}/images', ['uses' => 'Backend\Angeles@postAngelImages', 'as' => 'backend.modelo.saveimages']);

	Route::post('angeles/dropzone/{id}', ['uses' => 'Backend\Angeles@angelesDropZoneFiles', 'as' => 'angeles.dropzone']);
	Route::post('angeles/filedelete/{id}', ['uses' => 'Backend\Angeles@angelDeleteFile', 'as' => 'angeles.filedelete']);
	Route::post('angeles/fileposition', ['uses' => 'Backend\Angeles@angelUpdateFilePosition', 'as' => 'angeles.fileposition']);

	// BLOG

	Route::get('blog', ['uses' => 'Backend\Blog@all', 'as' => 'backend.blog']);
	Route::get('blog/searchresult', ['uses' => 'Backend\Blog@searchResult', 'as' => 'backend.blog.searchresult']);

	Route::get('blog/create', ['uses' => 'Backend\Blog@createPostView', 'as' => 'backend.blog.new']);
	Route::post('blog/create', ['uses' => 'Backend\Blog@createPostSave', 'as' => 'backend.blog.create']);
	Route::get('blog/update/{id}', ['uses' => 'Backend\Blog@editPostView', 'as' => 'backend.blog.update']);
	Route::post('blog/update/{id}', ['uses' => 'Backend\Blog@editPostSave', 'as' => 'backend.blog.save']);
	Route::get('blog/delete/{id}', ['uses' => 'Backend\Blog@deletePost', 'as' => 'backend.blog.delete']);

	// CATEGORIAS

	Route::get('categorias', ['uses' => 'Backend\Categories@categories', 'as' => 'backend.categorias']);

	Route::get('categorias/modelos', ['uses' => 'Backend\Categories@allModelCategories', 'as' => 'backend.model.categorias']);
	Route::post('categorias/modelos', ['uses' => 'Backend\Categories@postModelCategories', 'as' => 'backend.postmodel.categorias']);
	Route::get('categorias/modelos/{id}', ['uses' => 'Backend\Categories@viewModelCategorie', 'as' => 'backend.viewmodel.categorie']);
	Route::post('categorias/modelos/{id}', ['uses' => 'Backend\Categories@updateModelCategorie', 'as' => 'backend.updatemodel.categorie']);
	Route::post('categorias/modelos/delete/{id}', ['uses' => 'Backend\Categories@deleteModelCategorie', 'as' => 'backend.deletemodel.categorie']);

	Route::get('categorias/blog', ['uses' => 'Backend\Categories@allBlogCategories', 'as' => 'backend.blog.categorias']);
	Route::post('categorias/blog', ['uses' => 'Backend\Categories@postBlogCategories', 'as' => 'backend.postblog.categorias']);
	Route::get('categorias/blog/{id}', ['uses' => 'Backend\Categories@viewBlogCategorie', 'as' => 'backend.viewblog.categorie']);
	Route::post('categorias/blog/{id}', ['uses' => 'Backend\Categories@updateBlogCategorie', 'as' => 'backend.updateblog.categorie']);
	Route::post('categorias/blog/delete/{id}', ['uses' => 'Backend\Categories@deleteBlogCategorie', 'as' => 'backend.deleteblog.categorie']);

	/* ADMIN ONLY ROUTES */

	Route::group(['middleware' => 'cms.admin'], function () {

		// EDITAR EL HOMEPAGE
		Route::get('homepage', ['uses' => 'Backend\Homepage@all', 'as' => 'backend.homepage']);

		//ANUNCIOS PUBLICITARIOS
		Route::get('anuncios', ['uses' => 'Backend\Anuncios@showAds', 'as' => 'backend.anuncios']);
		Route::post('anuncios', ['uses' => 'Backend\Anuncios@postAds', 'as' => 'backend.post.anuncios']);
		Route::post('anuncios/update', ['uses' => 'Backend\Anuncios@updateAds', 'as' => 'backend.update.anuncios']);
		Route::post('anuncios/update/positions', ['uses' => 'Backend\Anuncios@updateAdPosition', 'as' => 'backend.update.positions']);
		Route::post('anuncios/delete', ['uses' => 'Backend\Anuncios@deleteAds', 'as' => 'backend.delete.anuncios']);

		//BANNERS DEL HOME
		Route::get('homepage-banners', ['uses' => 'Backend\Homepage@homeBanners', 'as' => 'backend.homebanners']);
		Route::post('homepage-banners', ['uses' => 'Backend\Homepage@postHomeBanners', 'as' => 'backend.post.homebanners']);
		Route::post('homepage-banners/update/banners', ['uses' => 'Backend\Homepage@updateHomeBanners', 'as' => 'backend.update.homebanners']);
		Route::post('homepage-banners/update/positions', ['uses' => 'Backend\Homepage@updateHomePositions', 'as' => 'backend.update.homepositions']);
		Route::post('homepage-banners/delete/banner', ['uses' => 'Backend\Homepage@deleteHomeBanner', 'as' => 'backend.delete.homebanners']);

		// HOME FEATURED
		Route::get('homepage-featured', ['uses' => 'Backend\Homepage@homeFeatured', 'as' => 'backend.homefeatured']);
		Route::post('homepage-featured', ['uses' => 'Backend\Homepage@postHomeFeatured', 'as' => 'backend.post.homefeatured']);
		Route::post('homepage-featured/update/boxes', ['uses' => 'Backend\Homepage@updateHomeFeatured', 'as' => 'backend.update.homefeatured']);
		Route::post('homepage-featured/update/positions', ['uses' => 'Backend\Homepage@updateHfeaturedPosition', 'as' => 'backend.update.hbxpositions']);
		Route::post('homepage-featured/delete/box', ['uses' => 'Backend\Homepage@deleteHomeFeatured', 'as' => 'backend.delete.homeboxes']);


	    //  REVISTAS

		Route::get('revistas', ['uses' => 'Backend\Revistas@all', 'as' => 'backend.revistas']);
		Route::get('revistas/posiciones', ['uses' => 'Backend\Revistas@revistasPositionShow', 'as' => 'backend.revistas.posiciones']);
		Route::post('revistas/posiciones', ['uses' => 'Backend\Revistas@revistasPositionPost', 'as' => 'backend.revistas.saveposition']);

		Route::get('revistas/search/result', ['uses' => 'Backend\Revistas@searchResult', 'as' => 'backend.revistas.search']);


		Route::get('revistas/new', ['uses' => 'Backend\Revistas@new', 'as' => 'backend.revistas.new']);
		Route::post('revistas/new', ['uses' => 'Backend\Revistas@postNew', 'as' => 'backend.create.revista']);

		Route::get('revista/{id}', ['uses' => 'Backend\Revistas@editView', 'as' => 'backend.revista.view']);
		Route::post('revista/{id}', ['uses' => 'Backend\Revistas@revistaUpdate', 'as' => 'backend.revista.update']);
		Route::get('revista/image/{filename}/{extension}', ['uses' => 'Backend\Revistas@getRevistasImages', 'as' => 'backend.getrevista.image']);


		// BORRAR
		
		Route::post('revista/delete/{id}', ['uses' => 'Backend\Revistas@revistaDelete', 'as' => 'backend.revista.delete']);

		// PRODUCTOS

		Route::get('productos', ['uses' => 'Backend\Productos@viewAllProducts', 'as' => 'backend.products.all']);
		
		Route::get('productos/create', ['uses' => 'Backend\Productos@createNewProduct', 'as' => 'backend.product.create']);
		Route::post('productos/create', ['uses' => 'Backend\Productos@saveNewProduct', 'as' => 'backend.product.post']);

		Route::get('productos/detail/{id}', ['uses' => 'Backend\Productos@viewProductDetail', 'as' => 'backend.product.detail']);
		Route::post('productos/update/{id}', ['uses' => 'Backend\Productos@updateProduct', 'as' => 'backend.product.update']);

		Route::get('productos/imagenes/{id}', ['uses' => 'Backend\Productos@productImages', 'as' => 'backend.product.images']);

		Route::post('productos/imagenes/upload/{id}', ['uses' => 'Backend\Productos@productImageDropZoneFiles', 'as' => 'backend.product.uploadimages']);
		Route::post('productos/imagenes/position/{id}', ['uses' => 'Backend\Productos@productUpdateImagePosition', 'as' => 'backend.productos.imageposition']);


		Route::post('productos/imagenes/delete/{id}', ['uses' => 'Backend\Productos@deleteImage', 'as' => 'backend.product.deleteimage']);

		Route::post('producto/delete/{id}', ['uses' => 'Backend\Productos@deleteProduct', 'as' => 'backend.product.delete']);

		Route::get('productos/search/result', ['uses' => 'Backend\Productos@searchResult', 'as' => 'backend.productos.search']);

		// FILES
		
		Route::get('revista/{id}/files', ['uses' => 'Backend\Revistas@issueFiles', 'as' => 'backend.revistas.files']);

		Route::post('revista/dropzone/{id}', ['uses' => 'Backend\Revistas@revistaDropZoneFiles', 'as' => 'backend.revistas.dropzone']);
		Route::post('revista/filedelete/{id}', ['uses' => 'Backend\Revistas@revistaDeleteFile', 'as' => 'backend.revistas.filedelete']);
		Route::post('revista/fileposition/{id}', ['uses' => 'Backend\Revistas@revistaUpdateFilePosition', 'as' => 'backend.revistas.fileposition']);

			//Video de las revistas

		Route::get('revista/{id}/video', ['uses' => 'Backend\Revistas@showVideo', 'as' => 'backend.revista.video']);
		Route::post('revista/{id}/video', ['uses' => 'Backend\Revistas@postVideo', 'as' => 'backend.revista.postvideo']);
		Route::post('revista/{id}/videoupdate', ['uses' => 'Backend\Revistas@updateVideo', 'as' => 'backend.revista.updatevideo']);

		// ORDENES

		Route::get('ordenes', ['uses' => 'Backend\Ordenes@all', 'as' => 'backend.ventas']);
		Route::get('ordenes/searchresult', ['uses' => 'Backend\Ordenes@searchResult', 'as' => 'backend.ventas.searchresult']);
		Route::get('ordenes/filtersearch', ['uses' => 'Backend\Ordenes@orderFilterResult', 'as' => 'backend.ventas.filtersearch']);
		Route::get('orden/{id}', ['uses' => 'Backend\Ordenes@orderView', 'as' => 'backend.order.view']);
		Route::post('orden/{id}', ['uses' => 'Backend\Ordenes@orderUpdate', 'as' => 'backend.order.update']);

		Route::get('ordenes/exportar/csv', ['uses' => 'Backend\Ordenes@exportOrdersToCsv', 'as' => 'backend.csv.orders']);

		// PRODUCTOS - ORDENES

		Route::get('productos-ordenes', ['uses' => 'Backend\ProductOrders@all', 'as' => 'backend.productos.ventas']);
		Route::get('productos-ordenes/searchresult', ['uses' => 'Backend\ProductOrders@searchResult', 'as' => 'backend.productos.ventas.searchresult']);
		Route::get('productos-ordenes/filtersearch', ['uses' => 'Backend\ProductOrders@orderFilterResult', 'as' => 'backend.productos.ventas.filtersearch']);
		Route::get('producto-orden/{id}', ['uses' => 'Backend\ProductOrders@orderView', 'as' => 'backend.product.order.view']);
		Route::post('producto-orden/{id}', ['uses' => 'Backend\ProductOrders@orderUpdate', 'as' => 'backend.product.order.update']);

		Route::get('productos-ordenes/exportar/csv', ['uses' => 'Backend\ProductOrders@exportOrdersToCsv', 'as' => 'backend.csv.products.orders']);

		// ADMINS

		Route::get('admins', ['uses' => 'Backend\General@adminsList', 'as' => 'backend.show.admins']);
		Route::post('admins/new', ['uses' => 'Backend\General@adminPostNew', 'as' => 'admin.create.new']);

		Route::get('admins/{id}', ['uses' => 'Backend\General@adminsViewDetail', 'as' => 'admin.detail.view']);
		Route::post('admins/{id}', ['uses' => 'Backend\General@adminUpdateInfo', 'as' => 'admin.detail.update']);

		Route::post('admins/delete/{id}', ['uses' => 'Backend\General@deleteAdmin', 'as' => 'admin.backend.delete']);
		

		// USUARIOS

		Route::get('usuarios', ['uses' => 'Backend\General@usersList', 'as' => 'backend.show.usuarios']);
		Route::get('usuarios/{id}', ['uses' => 'Backend\General@userDetail', 'as' => 'backend.user.detail']);
		Route::get('usuarios/search/result', ['uses' => 'Backend\General@userSearch', 'as' => 'backend.user.search']);

		Route::get('usuarios/exportar/csv', ['uses' => 'Backend\General@exportarUsuariosCsv', 'as' => 'backend.csv.users']);

		// AGREGAR Y QUITAR REVITAS A USUARIOS
		Route::post('usuario/addissue/{id}', ['uses' => 'Backend\General@addIssueUser', 'as' => 'backend.user.addissue']);
		Route::post('usuario/deleteissue/{id}', ['uses' => 'Backend\General@deleteIssueUser', 'as' => 'backend.user.deleteissue']);

		// TICKETS DE SOPORTE

		Route::get('support-tickets', ['uses' => 'Backend\Tickets@usersTickets', 'as' => 'backend.show.tickets']);
		Route::get('support-tickets/view/{id}', ['uses' => 'Backend\Tickets@viewTicket', 'as' => 'backend.view.ticket']);
		Route::post('support-tickets/view/{id}', ['uses' => 'Backend\Tickets@updateStatusTicket', 'as' => 'backend.update.ticket']);
		Route::post('support-tickets/reply/{id}', ['uses' => 'Backend\Tickets@replySupportTicket', 'as' => 'backend.reply.ticket']);
		Route::get('support-tickets/search/result', ['uses' => 'Backend\Tickets@searchTicketResult', 'as' => 'backend.search.ticket']);

		

	});


	


});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home.laravel');
