<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomephotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homephotos', function (Blueprint $table) {
            $table->increments('id');            
            $table->string('image_name')->nullable();
            $table->text('video_name')->nullable();
            $table->text('video_cover')->nullable();
            $table->string('type')->nullable();
            $table->integer('position')->default(0);
            $table->text('description')->nullable();
            $table->text('url')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homephotos');
    }
}
