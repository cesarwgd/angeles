<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_user = new Role();
        $role_user->nombre = "usuario";
        $role_user->descripcion = "Usuario Normal sin ningún privilegio";
        $role_user->save();

        $role_premium = new Role();
        $role_premium->nombre = "premium";
        $role_premium->descripcion = "Usuario Premium con acceso a las revistas y todo el contenido premium";
        $role_premium->save();

        $role_angel = new Role();
        $role_angel->nombre = "angel";
        $role_angel->descripcion = "Usuario Angel, privilegios aún por definirse";
        $role_angel->save();

        $role_editor = new Role();
        $role_editor->nombre = "editor";
        $role_editor->descripcion = "Usuario Editor con acceso al Panel de Administración de Contenidos solo a la parte del Blog";
        $role_editor->save();

        $role_admin = new Role();
        $role_admin->nombre = "admin";
        $role_admin->descripcion = "Usuario Admin con acceso a todo";
        $role_admin->save();

    }
}
