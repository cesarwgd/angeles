<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // retrieve roles
        $role_usuario = Role::where('nombre', 'usuario')->first();
        $role_premium = Role::where('nombre', 'premium')->first();
        $role_angel = Role::where('nombre', 'angel')->first();
        $role_editor = Role::where('nombre', 'editor')->first();
        $role_admin = Role::where('nombre', 'admin')->first();



        $user_normal = new User();
        $user_normal->name = "Juan";
        $user_normal->last_name = "Perez";
        $user_normal->email = "juan.perez@angelesrevista.com";
        $user_normal->password = bcrypt('normaluser');
        $user_normal->save();
        $user_normal->roles()->attach($role_usuario);

        $user_premium = new User();
        $user_premium->name = "Mario";
        $user_premium->last_name = "Gonzalez";
        $user_premium->email = "mario.gonzalez@angelesrevista.com";
        $user_premium->password = bcrypt('premiumuser');
        $user_premium->save();
        $user_premium->roles()->attach($role_premium);

        $user_angel = new User();
        $user_angel->name = "Bonita";
        $user_angel->last_name = "Tetona";
        $user_angel->email = "bonita.tetona@angelesrevista.com";
        $user_angel->password = bcrypt('angeluser');
        $user_angel->save();
        $user_angel->roles()->attach($role_angel);

        $user_editor = new User();
        $user_editor->name = "Miguel";
        $user_editor->last_name = "Editor";
        $user_editor->email = "miguel.editor@angelesrevista.com";
        $user_editor->password = bcrypt('editoruser');
        $user_editor->save();
        $user_editor->roles()->attach($role_editor);

        $user_admin = new User();
        $user_admin->name = "Cesar";
        $user_admin->last_name = "Gutierrez";
        $user_admin->email = "cesar.gutierrez@angelesrevista.com";
        $user_admin->password = bcrypt('adminuser');
        $user_admin->save();
        $user_admin->roles()->attach($role_admin);

    }
}
