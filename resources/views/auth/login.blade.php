@extends('frontend.layouts.main')

@section('content')
<div class="container padding-top-80 padding-bottom-80">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ingresa</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo electrónico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Ingresa
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Olvidaste tu contraseña?
                                </a>
                                <br>
                                No tienes cuenta?. Regístrate <a href="{{route('register')}}">AQUÍ</a>
                            </div>
                        </div>
                    </form>

                    <!--FACEBOOK LOGIN-->
                    <div class="form-group">

                        <div class="col-md-6 col-md-offset-4">
                            
                            @if(session()->has('autoriza'))
                            <button class="btn btn-primary">
                                <a href="{{route('login.rerequest.facebook')}}">Accede con tu cuenta de Facebook</a>
                            </button>
                            <br><br>
                            <div class="alert alert-warning">
                                <strong>{{session()->get('autoriza')}}</strong>
                            </div>
                            @else
                            <!--<button class="btn btn-primary">-->
                                <a href="{{route('login.facebook')}}"><img src="{{asset('images/login-fb.png')}}" ></a>
                            <!--</button>-->
                            @endif
                        </div>
                    </div>
                    <!--/FACEBOOK LOGIN-->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_page_scripts')
<script src="{{ asset('js/app.js') }}"></script>
@endsection
