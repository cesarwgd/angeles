@extends('frontend.layouts.issues_main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revistas - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	<nav class="issue-menu-box"><!--issue menu box-->
		<a href="#" class="open-mob-issuem"></a>
		<div><!--div-->

			<ul><!--ul-->
				<li>
					<a href="{{route('home')}}">
						<span aria-hidden="true" class="glyphicon glyphicon-home"></span> Inicio
					</a>
				</li>
				<li>
					<a href="{{route('user.profile')}}">
						<span aria-hidden="true" class="glyphicon glyphicon-user"></span> Mi Perfil
					</a>
				</li>
				
				<li class="mis-revistas-show">
					<a href="#">
						<span aria-hidden="true" class="glyphicon glyphicon-file"></span> Mis Revistas
					</a>
				</li>
				<li class="issue-title">
					<strong>{{$revista->name}}</strong>
				</li>
				@if($revista->type == "revista")
				<li class="show-revista-paginas">
					<a href="#">
						<span aria-hidden="true" class="glyphicon glyphicon-th"></span> Paginas
					</a>
				</li>
				@endif
				<li>
					<a href="{{route('watch.issue.video', ['hash_slug' => $revista->hash_slug])}}">
						<span aria-hidden="true" class="glyphicon glyphicon-facetime-video"></span> Video
					</a>
				</li>
			</ul><!--/ul-->

			<?php		

				// obtenemos la cantidad de total de imagenes que tiene la revista para sacar la paginacion
				$files =  $revista->files()->where('type', 'image')->where('class', 'content')->orderBy('position', 'asc')->get();			
				
				$p = 1;
				
			?>

			@if($revista->type == "revista")
			<div class="choose-page"><!--choose page-->
			
				<ul>
					@foreach($files as $foto)
					<li>
						<span>						
							<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $p])}}">
							{{$p}}
							</a>						
						</span>
					</li>
					<?php $p++; ?>
					@endforeach
				</ul>
			</div><!--/choose page-->
			@endif

			<div class="choose-issue"><!--choose issue-->

				<?php 
					$roles = ['admin', 'premium'];
					// premium user or admin
				?>
				@if($user->hasAnyRole($roles))
					@foreach($revistas as $mag)
					<div>
						<figure>
						@if($mag->type == "revista")
						<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}">
						@else
						<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}">
						@endif
							@if(!empty($mag->main_pic) && Storage::disk('rev_thumbs')->exists($mag->main_pic . "-thumb." . $mag->extension))		
							<img src="{{Storage::disk('rev_thumbs')->url($mag->main_pic . '-thumb.' . $mag->extension)}}">			    				
							@else
							{{$mag->name}}
							@endif
						</a>					
						</figure>
						<h5>
							@if($mag->type == "revista")
							<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}"<?php if($revista->id == $mag->id) echo " class='active'"; ?>>
							@else
							<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}"<?php if($revista->id == $mag->id) echo " class='active'"; ?>>
							@endif
								{{$mag->name}}
							</a>
						</h5>
					</div>
					@endforeach
				@else

				<?php // NORMAL USER ?>
				
					@foreach($user->revistas as $issue)
				
					<div>
						<figure>
						@if($issue->type == "revista")
						<a href="{{route('watch.issue', ['hash_slug' => $issue->hash_slug, 'page' => 1])}}">
						@else
						<a href="{{route('watch.issue.video', ['hash_slug' => $issue->hash_slug])}}">
						@endif
							@if(!empty($issue->main_pic) && Storage::disk('revista')->exists($issue->main_pic . "." . $issue->extension))							
														
							<img src="{{route('frontend.revista.privateimage', ['filename' => $issue->main_pic, 'extension' => $issue->extension])}}">				    			    				
							@else
							{{$issue->name}}
							@endif
						</a>					
						</figure>
						<h5>
							@if($issue->type == "revista")
							<a href="{{route('watch.issue', ['hash_slug' => $issue->hash_slug, 'page' => 1])}}"<?php if($revista->id == $issue->id) echo " class='active'"; ?>>
							@else
							<a href="{{route('watch.issue.video', ['hash_slug' => $issue->hash_slug])}}"<?php if($revista->id == $issue->id) echo " class='active'"; ?>>
							@endif
								{{$issue->name}}
							</a>
						</h5>
					</div>
					@endforeach
				@endif			
				
			</div><!--/choose issue-->
		</div><!--/div-->

	</nav><!--/issue menu box-->

	<!--=======================
		CONTENIDO DE LA REVISTA
	==========================-->

	<div class="issue-main"><!--issue main-->
		<?php 
			$video = $revista->files()->where('type', 'video')->first(); 
			// arrojar una excepcion 404 sino encuentra el arhivo
		?>
		<!--password-->	
		<div class="issue-video-password">
			<div>
				<a href="#" class="close-password">X</a>
				<p>Copie e ingrese el siguiente codigo:</p>
				<p><strong>@if($video) {{$video->extra}} @endif</strong></p>
			</div>
		</div>
		<!--boxes-->
		<div class="issue-content"><!--issue content-->
			
			<div class="loading-issue"></div>

			<div class="view-page"><!--view page-->
				<div class="tabla">
					<div class="tabla-celda">

					
					@if($video)
						<div id="issue-main-video">
							{!!$video->name!!}
						</div>
					@else
						<h2>Video no disponible</h2>
					@endif
						
					</div>					
				</div>
			</div><!--/view page-->
			
		</div><!--/issue content-->
		
	</div><!--/issue main-->

	<!--MOBILE PAGINATION-->
	<div class="mob-issue-pag">
		<div><!--div-->			
		</div><!--/div-->
	</div>



@endsection

@section('footer_page_scripts')
<script type="text/javascript" src="{{asset('js/issue.js')}}"></script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    player.on('play', function() {
        $(".issue-video-password").fadeOut();
    });
    player.on('ended', function() {
        $(".issue-video-password").fadeIn();
    });

    
</script>
@endsection