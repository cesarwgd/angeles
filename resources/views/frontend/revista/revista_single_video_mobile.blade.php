@extends('frontend.layouts.issues_main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revistas - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	

	<!--=======================
		CONTENIDO DE LA REVISTA
	==========================-->

	<div class="issue-main"><!--issue main-->
		<?php 
			$video = $revista->files()->where('type', 'video')->first(); 
			// arrojar una excepcion 404 sino encuentra el arhivo
		?>
		<!--password-->	
		<div class="issue-video-password">
			<div>
				<a href="#" class="close-password">X</a>
				<p>Copie e ingrese el siguiente codigo:</p>
				<p><strong>@if($video) {{$video->extra}} @endif</strong></p>
			</div>
		</div>
		<!--boxes-->
		<div class="issue-content"><!--issue content-->
			
			<div class="loading-issue"></div>

			<div class="view-page"><!--view page-->
				<div class="tabla">
					<div class="tabla-celda">

					
					@if($video)
						<div id="issue-main-video">
							{!!$video->name!!}
						</div>
					@else
						<h2>Video no disponible</h2>
					@endif
						
					</div>					
				</div>
			</div><!--/view page-->
			
		</div><!--/issue content-->
		
	</div><!--/issue main-->


@endsection

@section('footer_page_scripts')
<script type="text/javascript" src="{{asset('js/issue.js')}}"></script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
    var iframe = document.querySelector('iframe');
    var player = new Vimeo.Player(iframe);

    player.on('play', function() {
        $(".issue-video-password").fadeOut();
    });
    player.on('ended', function() {
        $(".issue-video-password").fadeIn();
    });

    
</script>
@endsection