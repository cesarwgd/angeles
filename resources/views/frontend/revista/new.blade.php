@extends('frontend.layouts.main')

@section('meta-description')
	{{$new->meta_description}}
@endsection

@section('meta-keywords')
	{{$new->meta_keywords}}
@endsection

@section('meta-title')
	@if(!empty($new->meta_title)) {{$new->meta_title}} @else {{$new->name}} @endif - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')

<div class="porfavor-acceda">
	<div>		
		<h5>
			Por favor <a href="{{url('login')}}">INGRESE</a> a su cuenta o <a href="{{url('register')}}">REGÍSTRESE</a> para proceder con la compra de la revista.
		</h5>
		<a href="#" class="cerrar-porfavor">X</a>
	</div>
</div>	
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor revista-detail-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="new-issue-box"><!--pics-->

					<h1>{{$new->name}}</h1>
					@if(session()->has('exito'))
					<div class="alert alert-success">
						Revista agregada con éxito a su carrito de compras. Haga clic <a href="{{route('user.checkout')}}">ACÁ</a> para proceder a pagar su compra o <a href="{{route('revistas')}}">continue comprando más revistas</a>
					</div>
					@endif
					@if(session()->has('already'))
					<div class="alert alert-warning">
						{{session()->get('already')}}
					</div>
					@endif
					<div class="video-intriga"><!--video intriga-->
						{!!$new->video_intriga!!}
					</div><!--/video intriga-->

					<article>
						<p>{{$new->short_description}}</p>
					</article>
					<h3 class="detail-price">S/. {{$new->price}}</h3>
					<div class="clearfix"></div>
					<form action="{{route('user.place.order')}}" method="POST" id="agregar-al-carrito">
						<input type="hidden" name="revista_id" value="{{$new->id}}">
						<input type="hidden" name="type" value="revista">
						{{csrf_field()}}
						@if(!Auth::user())
						<a href="#" class="detail-comprar" id="buy-not-logeddin">Comprar</a>
						<script type="text/javascript">
							$("#buy-not-logeddin").click(function(){
								$('.porfavor-acceda').fadeIn();
								return false;
							});
						</script>
						@else
						<button type="submit" class="detail-comprar" id="revista-place-order">
							Comprar
						</button>
						@endif
					</form>

					<ul class="share-detail">
						<li class="fb-share-detail">
							<a href="#"></a>
						</li>
						<li class="twitter-share-detail">
							<a href="#"></a>
						</li>
						<li class="gplus-share-detail">
							<a href="#"></a>
						</li>
					</ul>
										
				</div><!--/pics-->

				

			</div><!--/row-->

			<div class="ads ad-revista-preview"><!--ads-->
				@if($ad != null)
					<img src="{{Storage::disk('home')->url($ad->media_name)}}">
				@endif					
			</div><!--/ads-->

			<script type="text/javascript">
					$("#agregar-al-carrito").submit(function(){
						$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Agregando a su carrito de compras..</div></div></div>");
					});
				</script>
			

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection