@extends('frontend.layouts.main')

@section('meta-description')
	{{$revista->meta_description}}
@endsection

@section('meta-keywords')
	{{$revista->meta_keywords}}
@endsection

@section('meta-title')
	@if(!empty($revista->meta_title)) {{$revista->meta_title}} @else {{$revista->name}} @endif - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
<div class="porfavor-acceda">
	<div>		
		<h5>
			Por favor <a href="{{url('login')}}">INGRESE</a> a su cuenta o <a href="{{url('register')}}">REGÍSTRESE</a> para proceder con la compra de la revista.
		</h5>
		<a href="#" class="cerrar-porfavor">X</a>
	</div>
</div>	


	<div class="main"><!--main-->

		
		
		<div class="contenedor revista-detail-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-6 revista-detail-mainpic"><!--pics-->

					<figure class="show-main-detailpic">
					<?php $img = 1; ?>
						@foreach($revista->files()->where('class', 'preview')->get() as $file)
							
						<img src="{{route('frontend.revista.publicimage', ['filename' => $file->name, 'extension' => $file->extension])}}" alt="{{$revista->name}}" id="thumb<?php echo $img; ?>">
							<?php $img++; ?>	
						@endforeach	
						
																	
					</figure>

					<div class="thumbs-container">
						<div class="show-thumbs-box">
						<?php $thumb = 1; ?>
						@foreach($revista->files()->where('class', 'preview')->get() as $file)
							<div class="thumbs-detail-preview">
								<a href="#thumb<?php echo $thumb; ?>" data-imgurl="{{route('frontend.revista.publicimage', ['filename' => $file->name, 'extension' => $file->extension])}}" title="{{$revista->name}}">
									<img src="{{route('frontend.revista.publicimage', ['filename' => $file->name, 'extension' => $file->extension])}}" alt="{{$revista->name}}">
								</a>
							</div>
							<?php $thumb++; ?>
						@endforeach						
							
						</div>
					</div>
					
				</div><!--/pics-->
				<script type="text/javascript">
					$(".thumbs-detail-preview a").click(function(){
						$('.show-main-detailpic img').hide();
						var image = $(this).attr('href');						
						$('.show-main-detailpic').find(image).fadeIn();
						return false;
					});
				</script>

				<div class="col-xs-12 col-sm-6 revista-detail-texts"><!--texts-->

					<h1>{{$revista->name}}</h1>
					@if(session()->has('exito'))
					<div class="alert alert-success">
						Revista agregada con éxito a su carrito de compras. Haga clic <a href="{{route('user.checkout')}}">ACÁ</a> para proceder a pagar su compra o <a href="{{route('revistas')}}">continue comprando más revistas</a>
					</div>
					@endif
					@if(session()->has('already'))
					<div class="alert alert-warning">
						{{session()->get('already')}}
					</div>
					@endif
					<h3 class="detail-price">S/. {{$revista->price}}</h3>
					<h4>Precio aproximado en dólares: <strong>${{ round($revista->price/3.26, 2) }}</strong></h4>
					<div class="clearfix"></div>
					<form action="{{route('user.place.order')}}" method="POST" id="agregar-al-carrito">
						<input type="hidden" name="revista_id" value="{{$revista->id}}">
						<input type="hidden" name="type" value="{{$revista->type}}">
						{{csrf_field()}}
						@if(!Auth::user())
						<a href="#" class="detail-comprar" id="buy-not-logeddin">Comprar</a>
						<script type="text/javascript">
							$("#buy-not-logeddin").click(function(){
								$('.porfavor-acceda').fadeIn();
								return false;
							});
						</script>
						@else
							<?php 
								$roles = ['admin', 'premium'];
							?>
							@if(Auth::user()->hasAnyRole($roles))
							<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => 1])}}" class="detail-comprar">Ver Revista</a>

							@else
							<button type="submit" class="detail-comprar" id="revista-place-order">
								Comprar
							</button>
							@endif
						@endif
					</form>
					<div class="clearfix"></div>
					<section>
						{!! $revista->description !!}
					</section>
					<div class="video-preview-revista">
						{!!$revista->video_intriga!!}
					</div>
					<ul class="share-detail">
						<li class="fb-share-detail">
							<a href="#"></a>
						</li>
						<li class="twitter-share-detail">
							<a href="#"></a>
						</li>
						<li class="gplus-share-detail">
							<a href="#"></a>
						</li>
					</ul>
				
				</div><!--/texts-->
				
			</div><!--/row-->

			<div class="ads ad-revista-preview"><!--ads-->
				@if($ad != null)
					<img src="{{Storage::disk('home')->url($ad->media_name)}}">
				@endif				
			</div><!--/ads-->

			<div class="similar-products"><!--similar products-->

				<h2>También te recomendamos</h2>

				<div class="row"><!--row-->
				@foreach($otros as $otro)
				<div class="col-xs-12 col-sm-4"><!--col-->
						<figure class="recomendamos-imgs">
							<img src="{{route('frontend.revista.publicimage', ['filename' => $otro->main_pic, 'extension' => $otro->extension])}}" alt="{{$otro->name}}">
							<a href="{{route('revistas.preview', ['slug' => $otro->slug])}}" title="{{$otro->name}}"></a>							
						</figure>
						<h4>
							<a href="{{route('revistas.preview', ['slug' => $otro->slug])}}" title="{{$otro->name}}">{{$otro->name}}</a>
						</h4>
					</div><!--/col-->
				@endforeach
					

					


				</div><!--/row-->

			</div><!--/similar products-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection
@section('footer_page_scripts')
<script type="text/javascript">
	$("#agregar-al-carrito").submit(function(){
		$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Agregando a su carrito de compras..</div></div></div>");
	});
</script>
@endsection