@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revistas - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title"><!--main title-->
			<h1>Revistas</h1>
		</header><!--/main title-->
		
		<div class="contenedor all-issues"><!--/contenedor-->
			
			<div class="row"><!--row-->


				<div class="col-xs-12 col-sm-6 pull-right">

					@if(session()->has('error'))
						<div class="alert alert-danger">
							{{session()->get('error')}}
						</div>
					@endif

					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					
					<div class="pull-right">
						<form action="{{route('buscar.revista')}}" method="GET">
				  
						  <div class="form-group" style="display: inline-block;">						    
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por nombre" name="nombre" required>
						  </div>						  
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
					<br><br><br><br>
				</div>
				<div class="clearfix"></div>

				@foreach($revistas as $revista)
				<div class="col-xs-12 col-sm-3 col-lg-2 issues-box"><!--issues box-->
					<div class="issue-portrait">
						<a href="{{route('revistas.preview', ['slug' => $revista->slug])}}" title="{{$revista->name}}">
							<img src="{{route('frontend.revista.publicimage', ['filename' => $revista->main_pic, 'extension' => $revista->extension])}}" class="angel-img">
						</a>
					</div>
					<h2>
						<a href="{{route('revistas.preview', ['slug' => $revista->slug])}}" title="{{$revista->name}}">
						{{$revista->name}}
						</a>
					</h2>
				</div><!--/issues box-->
					

				@endforeach
				

				<div class="clearfix"></div>

			</div><!--/row-->

			{{$revistas->links()}}

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection