@extends('frontend.layouts.issues_main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revistas - Revista Angeles - Te traemos el paraíso 
@endsection

@section('page-speficic-scripts')
<link href="{{asset('css/imageviewer.css')}}"  rel="stylesheet" type="text/css" />
@endsection

@section('content')
	
	<nav class="issue-menu-box"><!--issue menu box-->
		<a href="#" class="open-mob-issuem"></a>
		<div><!--DIV-->
			<ul><!--ul-->
				<li>
					<a href="{{route('home')}}">
						<span aria-hidden="true" class="glyphicon glyphicon-home"></span> Inicio
					</a>
				</li>
				<li>
					<a href="{{route('user.profile')}}">
						<span aria-hidden="true" class="glyphicon glyphicon-user"></span> Mi Perfil
					</a>
				</li>			
				<li class="mis-revistas-show">
					<a href="#">
						<span aria-hidden="true" class="glyphicon glyphicon-file"></span> Mis Revistas
					</a>
				</li>
				<li class="issue-title">
					<strong>{{$revista->name}}</strong>
				</li>
				@if($revista->type == "revista")
				<li class="show-revista-paginas">
					<a href="#">
						<span aria-hidden="true" class="glyphicon glyphicon-th"></span> Paginas
					</a>
				</li>
				@endif
				<li>
					<a href="{{route('watch.issue.video', ['hash_slug' => $revista->hash_slug])}}">
						<span aria-hidden="true" class="glyphicon glyphicon-facetime-video"></span> Video
					</a>
				</li>
			</ul><!--/ul-->

			<?php
				// segmento de la url que indica en que pagina se encutrna
				$segment = Request::segment(2);
				$currentPage = Request::segment(3);

				// obtenemos la cantidad de total de imagenes que tiene la revista para sacar la paginacion
				$files =  $revista->files()->where('type', 'image')->where('class', 'content')->orderBy('position', 'asc')->get();
				$totalFiles = count($files);
				$prev = $currentPage - 1;
				$next = $currentPage + 1;
				$p = 1;
				
			?>

			@if($revista->type == "revista")
			<div class="choose-page"><!--choose page-->
			
				<ul>
					@foreach($files as $foto)
					<li @if($currentPage == $p) class="current" @endif>
						<span>
							@if($currentPage != $p)
							<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $p])}}">
							{{$p}}
							</a>
							@else						
							{{$p}}						
							@endif
						</span>
					</li>
					<?php $p++; ?>
					@endforeach
				</ul>
			</div><!--/choose page-->
			@endif

			<div class="choose-issue"><!--choose issue-->
				<?php 
					$roles = ['admin', 'premium'];
					// PREMIUM
				?>
				@if($user->hasAnyRole($roles))
					@foreach($revistas as $mag)
					<div>
						<figure>
						@if($mag->type == "revista")
						<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}">
						@else
						<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}">
						@endif
							@if(!empty($mag->main_pic) && Storage::disk('rev_thumbs')->exists($mag->main_pic . "-thumb." . $mag->extension))		
							<img src="{{Storage::disk('rev_thumbs')->url($mag->main_pic . '-thumb.' . $mag->extension)}}">			    				
							@else
							{{$mag->name}}
							@endif
						</a>					
						</figure>
						<h5>
							@if($mag->type == "revista")
							<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}"<?php if($revista->id == $mag->id) echo " class='active'"; ?>>
							@else
							<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}"<?php if($revista->id == $mag->id) echo " class='active'"; ?>>
							@endif
								{{$mag->name}}
							</a>
						</h5>
					</div>
					@endforeach
				@else

				<?php // NORMAL USERS ?>
				
					@foreach($user->revistas as $issue)
				
					<div>
						<figure>
						@if($issue->type == "revista")
						<a href="{{route('watch.issue', ['hash_slug' => $issue->hash_slug, 'page' => 1])}}">
						@else
						<a href="{{route('watch.issue.video', ['hash_slug' => $issue->hash_slug])}}">
						@endif
							@if(!empty($issue->main_pic) && Storage::disk('revista')->exists($issue->main_pic . "." . $issue->extension))							
														
							<img src="{{route('frontend.revista.privateimage', ['filename' => $issue->main_pic, 'extension' => $issue->extension])}}">				    			    				
							@else
							{{$issue->name}}
							@endif
						</a>					
						</figure>
						<h5>
							@if($issue->type == "revista")
							<a href="{{route('watch.issue', ['hash_slug' => $issue->hash_slug, 'page' => 1])}}"<?php if($revista->id == $issue->id) echo " class='active'"; ?>>
							@else
							<a href="{{route('watch.issue.video', ['hash_slug' => $issue->hash_slug])}}"<?php if($revista->id == $issue->id) echo " class='active'"; ?>>
							@endif
								{{$issue->name}}
							</a>
						</h5>
					</div>
					@endforeach
				@endif		
				
				
			</div><!--/choose issue-->
		</div><!--/DIV-->

	</nav><!--/issue menu box-->

	<!--=======================
		CONTENIDO DE LA REVISTA
	==========================-->

	<div class="issue-main"><!--issue main-->
	
		@if($currentPage != 1)
		<div class="issue-prev button-nav-box desk-issue-prev">
			<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $prev])}}">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 30">
					<title>Left Arrows</title>
					<g id="Layer_2" data-name="Layer 2">
						<g>
							<polygon fill="#000000" id="Left_Arrows" data-name="Left Arrows" class="cls-1" points="15 0 16 1 2 15 16 29 15 30 0 15 15 0"></polygon>
						</g>
					</g>
				</svg>

			</a>
		</div>
		@endif

		@if($currentPage != $totalFiles)
		<div class="issue-next button-nav-box desk-issue-next">
			<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $next])}}">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 30">
					<title>Right Arrow</title>
					<g id="Layer_2" data-name="Layer 2">
						<g>
							<polygon fill="#000000" id="Right_Arrow" data-name="Right Arrow" class="cls-1" points="1 0 0 1 14 15 0 29 1 30 16 15 1 0"></polygon>
						</g>
					</g>
				</svg>
			</a>
		</div>
		@endif

		<!--boxes-->
		<div class="issue-content"><!--issue content-->
			<a class="open-fullimage-mobile" id="open-fullimage-mobile">+</a>
			<div class="overlay-block"></div>
			<!-- class loading-issue-->

			<div class="view-page" id="mobileFullScreen"><!--view page-->
				<div class="tabla">
					<div class="tabla-celda">

					<?php 
						$imagen = $revista->files()->where('position', $page)->where('class', 'content')->first(); 
						// arrojar una excepcion 404 sino encuentra el arhivo
					?>
					
					@if(Storage::disk('revista')->exists($imagen->name . "." . $imagen->extension))							
				      <img src="{{route('frontend.revista.privateimage', ['filename' => $imagen->name, 'extension' => $imagen->extension])}}" class="pannable-image">		        	
			    	@endif		
						
					</div>					
				</div>
			</div><!--/view page-->
			
		</div><!--/issue content-->
		
	</div><!--/issue main-->


	<!--MOBILE PAGINATION-->
	<div class="mob-issue-pag">
		<div><!--div-->
			@if($currentPage != 1)
			<div class="issue-prev button-nav-box mob-issue-prev">
				<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $prev])}}">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 30">
						<title>Left Arrows</title>
						<g id="Layer_2" data-name="Layer 2">
							<g>
								<polygon fill="#FFFFFF" id="Left_Arrows" data-name="Left Arrows" class="cls-1" points="15 0 16 1 2 15 16 29 15 30 0 15 15 0"></polygon>
							</g>
						</g>
					</svg>

				</a>
			</div>
			@endif

			@if($currentPage != $totalFiles)
			<div class="issue-next button-nav-box mob-issue-next">
				<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => $next])}}">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 30">
						<title>Right Arrow</title>
						<g id="Layer_2" data-name="Layer 2">
							<g>
								<polygon fill="#FFFFFF" id="Right_Arrow" data-name="Right Arrow" class="cls-1" points="1 0 0 1 14 15 0 29 1 30 16 15 1 0"></polygon>
							</g>
						</g>
					</svg>
				</a>
			</div>
			@endif
		</div><!--/div-->
	</div>


@endsection

@section('footer_page_scripts')
<script src="{{asset('js/imageviewer.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/issue.js')}}"></script>
@endsection