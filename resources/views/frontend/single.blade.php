@extends('frontend.layouts.main')

@section('meta-description')
	@if(!empty($post->meta_description)) {{$post->meta_description}} @else {{$post->titulo}} @endif
@endsection

@section('meta-keywords')
	@if(!empty($post->meta_keywords)) {{$post->meta_keywords}} @else {{$post->titulo}} @endif
@endsection

@section('meta-title')
	@if(!empty($post->meta_title)) {{$post->meta_title}} @else {{$post->titulo}} @endif - Revista Ángeles
@endsection

@section('page-specific-scripts')
<meta property="og:title" content="{{$post->titulo}}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content='<?php if(!empty($post->main_image) && Storage::disk('blog')->exists($post->main_image)) { ?>{{Storage::disk('blog')->url($post->main_image)}} <?php } ?>'/>
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:url" content="https://www.angelesrevista.com/blog/{{$post->slug}}" />
    <meta property="og:description" content="{{$post->meta_description}}" />
    <meta property="fb:app_id" content="355331158315172" />
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title blog-title"><!--main title-->
			<h1>{{$post->titulo}}</h1>
			<div class="published-on">
				Pubicado el {{$post->created_at->format('d M, Y')}}
			</div>
		</header><!--/main title-->
		
		<div class="contenedor blog-main-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-9 single-post"><!--all posts-->
					
					<article>
						<figure class="single-main-pic">
							@if(!empty($post->main_image))
								@if(Storage::disk('blog')->exists($post->main_image))
								<img src="{{Storage::disk('blog')->url($post->main_image)}}" alt="{{$post->titulo}}">
								@endif
							@endif
						</figure>
						<div class="single-post-content"><!--content-->
							{!! $post->contenido !!}
							<div class="clearfix"></div>
						</div><!--/content-->

						<ul class="single-post-share">
							<li>
								Compártelo en las redes:
							</li>
							<?php $categoryShareSlug = $post->getCategorySlug(); ?>
							<li class="fb-blog-single">
								<div class="fb-share-button" data-href="{{route('blog.single', ['category' => $categoryShareSlug, 'slug' => $post->slug])}}" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartir</a></div>
							</li>
							<li class="twitter-blog-single">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-size="large"
								  data-text="custom share text"
								  data-url="{{route('blog.single', ['category' => $categoryShareSlug, 'slug' => $post->slug])}}"
								  data-hashtags="example,demo"
								  data-via="twitterdev"
								  data-related="twitterapi,twitter">
								<img src="{{asset('images/tweet.png')}}">
								</a>
							</li>
							<li class="youtube-blog-single">
								<a href="#"></a>
								<!-- Place this tag where you want the share button to render. -->
								<div class="g-plus" data-action="share" data-height="24" data-href="{{route('blog.single', ['category' => $categoryShareSlug, 'slug' => $post->slug])}}"></div>
							</li>
						</ul>
						<div class="clearfix"></div>
					</article>


					<div class="comentarios-main"><!--comentarios main-->
						<header class="otros-posts-title">
							<h3>Comentarios</h3>
						</header>
						<div class="fb-comentarios">
							<div class="fb-comments" data-href="{{route('blog.single', ['category' => $categoryShareSlug, 'slug' => $post->slug])}}" data-numposts="5"></div>							
						</div>
					</div><!--/comentarios main-->

					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-12 otros-posts"><!--otros posts-->

						<header class="otros-posts-title">
							<h3>Otros Artículos que podría interesarte</h3>
						</header>

						<div class="row"><!--row-->
							
							@foreach($otros as $otro)
							<div class="col-xs-12 col-sm-4 all-posts-preview"><!--preview-->
								<div>
									<?php 
										$categorySlug = $post->getCategorySlug();
										$imagen = "";
										if(Storage::disk('blog')->exists($otro->main_image)){
											$imagen = Storage::disk('blog')->url($otro->main_image);
										}
									?>
									<figure style="background-image:url({{$imagen}});">
										<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$otro->titulo}}"></a>
									</figure>
									<section>
										<h2><a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$otro->titulo}}">{{$otro->titulo}}</a></h2>
										<p>
											@if(!empty($otro->extracto))
											{{$otro->extracto}}
											@else
											<?php echo strip_tags(substr($otro->contenido, 0, 180)); ?>
											@endif
											...
										</p>
										<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$otro->titulo}}" class="read-more">Continuar leyendo</a>
									</section>
								</div>
							</div><!--/preview-->

							@endforeach
							

							

						</div><!--/row-->
					</div><!--/otros posts-->
					

				</div><!--/all posts-->

				@include('frontend.layouts.includes.blog_sidebar')

				

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection
