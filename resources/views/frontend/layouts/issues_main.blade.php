<!DOCTYPE html>
<html lang="es" class="full-body-height">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="@yield('meta-description')" />
<meta name="keywords" content="@yield('meta-keywords')" />
<meta name="author" content="Basecamp" />
<title>@yield('meta-title')</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/main.min.css')}}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{asset('js/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/functions.js')}}"></script>
@yield('user-scripts')
@yield('page-speficic-scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88370713-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-88370713-1');
</script>
</head>

<body class="full-body-height">

	
@yield('content')

	
@yield('footer_page_scripts')
</body>
</html>	