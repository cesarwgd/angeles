				<div class="col-xs-12 col-sm-3 blog-sidebar"><!--blog sidebar-->

					<div class="ads side-ad side-boxes"><!--ad-->
						@if($ad != null)
							<img src="{{Storage::disk('home')->url($ad->media_name)}}">
						@endif						
					</div><!--/ad-->

					<div class="side-boxes"><!--side boxes-->
						<h3>Revistas</h3>
						@foreach($revistas as $revista)
						<div class="side-revistas"><!--side revistas-->	
							<div>
								<h4><a href="{{route('revistas.preview', ['slug' => $revista->slug])}}">{{$revista->name}}</a></h4>
								<h5>S/. {{$revista->price}}</h5>
							</div>							
							<figure style="background-image:url({{route('frontend.revista.publicimage', ['filename' => $revista->main_pic, 'extension' => $revista->extension])}});">
								<a href="{{route('revistas.preview', ['slug' => $revista->slug])}}" title="{{$revista->name}}"></a>
							</figure>
						</div><!--/side revistas-->
						@endforeach
						
					</div><!--side boxes-->

					<div class="side-boxes">
						<h3>Síguenos en Facebook</h3>
						<div class="fb-side-block">
							<div class="fb-page" data-href="https://www.facebook.com/angelesrevista/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/angelesrevista/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/angelesrevista/">Ángeles Revista</a></blockquote></div>							
						</div>
					</div>
				
				</div><!--/blog sidebar-->