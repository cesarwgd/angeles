							
							<div class="panel panel-default"><!--panel-->
							  <div class="panel-heading">Eliminar cuenta</div>
							  <div class="panel-body">
							    
							    <a class="btn btn-danger" href="#" id="borrar-cuenta">
							    	Eliminar cuenta
							    </a>
							    <form id="delete-form" action="{{route('delete.user')}}" method="POST">{{csrf_field()}}</form>
							  </div>
							</div><!--/panel-->
							<div class="are-you-sure">
								<div class="box">
									<h4>Esta seguro?</h4>
									<p>Si borra su cuenta ya no volverá a tener acceso a todo el contenido que su cuenta tiene.</p>
									<a class="btn btn-warning" href="#" id="cancelar-borrar">Cancelar</a> 
									 <a class="btn btn-danger" href="#" onclick="confirmarBorrar()">
								    	Eliminar cuenta
								    </a>
								</div>
							</div>
							<script type="text/javascript">

								$("#borrar-cuenta").click(function(){
									$(".are-you-sure").fadeIn();
									return false;
								});

								$("#cancelar-borrar").click(function(){
									$(".are-you-sure").fadeOut();
									return false;
								});

								function confirmarBorrar()
								{									
									document.getElementById("delete-form").submit();															
									
								}
							</script>