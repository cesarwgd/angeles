					<div class="list-group"><!--list group-->
					  <a href="{{route('user.profile')}}" class="list-group-item @if(request()->segment(2) == 'perfil') active @endif">
					    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Perfil
					  </a>
					  <a href="{{route('user.revistas')}}" class="list-group-item @if(request()->segment(2) == 'revistas') active @endif">
					  	<span class="glyphicon glyphicon-file" aria-hidden="true"></span> Mis Revistas
					  </a>
					  <a href="{{route('user.ordenes')}}" class="list-group-item @if(request()->segment(2) == 'ordenes') active @endif">
					  	<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mis Ordenes
					  </a>
					  <a href="{{route('user.tickets')}}" class="list-group-item @if(request()->segment(2) == 'tickets') active @endif">
					  	<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Tickets de Soporte
					  </a>
					  <a href="{{route('ayuda')}}" class="list-group-item">
					  	<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Ayuda
					  </a>					  		  
					</div><!--/list group-->