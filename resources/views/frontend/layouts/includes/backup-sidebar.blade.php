<!--ESTO IBA PRIMERO EN EL RIGHT SIDEBAR / ACTIVARLO CUANDO SE SAQUE PREMIUM -->
<div class="panel panel-default"><!--panel-->
							  <div class="panel-heading">Tipo de usuario</div>
							  <div class="panel-body">
							    <h5>
							    	@foreach(Auth::user()->roles as $role)
							    		@if($role->nombre == "usuario")
							    			Normal
							    		@endif
							    	@endforeach
							    </h5>
							    <hr>
							    <h6><strong>Actualizar a Premium:</strong></h6>
							    <a class="btn btn-primary">Comprar Premium</a>
							    <br><br>
							    <p>Entérate <a href="#">aquí</a> de los beneficios de actualiar tu cuenta a <strong>premium</strong> </p>
							  </div>
							</div><!--/panel-->