	<header class="cabecera"><!--cabecera-->
		<div><!--div-->
			<div class="first-head"><!--div-->
				<a href="{{route('home')}}"><img src="{{asset('images/logo.png')}}" alt="Revista Angeles" class="logo logo-interno"></a>
				<div class="first-menu"><!-- first menu -->
					<ul>
						<li class="nosotros-menu">
							<a href="{{route('nosotros')}}">Nosotros</a>						
						</li>
						<li class="blog-menu">
							<a href="{{route('blog')}}">Blog</a>
							<!--BLOG SUBMENU-->
							@if(isset($cats))
							<ul class="blog-submenu">							
								@foreach($cats as $cat)
								<li>
									<a href="{{route('blog.category', [ 'categorySlug' => $cat->slug ])}}">{{ $cat->nombre }}</a>
								</li>
								@endforeach							
							</ul>
							@endif						
						</li>
						<li class="contacto-menu">
							<a href="{{route('contacto')}}">Contacto</a>						
						</li>
						<li class="fb-head">
							<a href="https://www.facebook.com/angelesrevista/" target="_blank" title="Facebook Angeles">
								<img src="{{asset('images/fb-head.png')}}">
							</a>
						</li>
						<li class="twitter-head">
							<a href="#" target="_blank" title="Twitter Angeles">
								<img src="{{asset('images/twitter-head.png')}}">
							</a>
						</li>
						<li class="instagram-head">
							<a href="https://www.instagram.com/angelesrevistaoficial/" target="_blank" title="Instagram Angeles">
								<img src="{{asset('images/instagram-header.png')}}">
							</a>
						</li>
					</ul>
				</div><!--/ first menu-->
			</div><!--/div-->
			<div class="second-head"><!--second head-->
				<nav class="container-menu">
					<ul class="main-menu"><!--main menu-->
						<li class="logo-movil">
							<a href="{{route('home')}}"><img src="{{asset('images/logo-movil.png')}}" alt="Angeles Revista"></a>
						</li>
						<li>
							<a href="{{route('revistas')}}">Revistas</a>
						</li>
						<li>
							<a href="{{route('frontend.productos')}}">ANG Store</a>
						</li>
						<li>
							<a href="{{route('hot.max')}}">Paradise</a>
						</li>						
					</ul><!--/main menu-->
				</nav>
				<nav class="user-menu"><!--user menu-->
					<a href="#" class="open-mob-menu"></a>
					<div>
						<ul>					
							@if(Auth::check())
							<li>
								<a href="{{route('user.revistas')}}">
									<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Mi Perfil
								</a>
							</li>
							@if(Auth::user()->hasIncompleteOrders() || Auth::user()->hasIncompleteProductOrders())
							<li>
								<a href="#">
									<span aria-hidden="true" class="glyphicon glyphicon-shopping-cart">
									</span> Shopping Cart <strong><?php echo Auth::user()->newOrdersTotal(); ?></strong>
								</a>
								<ul class="shopping-user-menu">
									<li><a href="{{route('user.checkout')}}">Revistas <?php echo Auth::user()->newRevistasOrdersTotal(); ?></a></li>
									<li><a href="{{route('user.product.checkout')}}">Productos <?php echo Auth::user()->newProductsOrdersTotal(); ?></a></li>
								</ul>
							</li>
							@endif	
							<li>
		                        <a href="{{ route('logout') }}"
		                            onclick="event.preventDefault();
		                                     document.getElementById('logout-form').submit();">
		                            <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Cerrar Sesión
		                        </a>
		                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
		                            {{ csrf_field() }}
		                        </form>
		                    </li>
							@else
							<li>
								<a href="{{ route('login') }}"><span aria-hidden="true" class="glyphicon glyphicon-log-in"></span> Ingresa</a>
							</li>
							<li>
								<a href="{{ route('register') }}"><span aria-hidden="true" class="glyphicon glyphicon-edit"></span> Regístrate</a>
							</li>
							@endif					
						</ul>
						@if(Auth::check())
						<ul class="mob-menu-2">						
							<li>
								<a href="{{route('user.profile')}}" @if(request()->segment(2) == 'revistas') class='active' @endif>
								  	<span class="glyphicon glyphicon-file" aria-hidden="true"></span> Mi Cuenta
								  </a>
							</li>
							<li>
								<a href="{{route('user.ordenes')}}" @if(request()->segment(2) == 'ordenes') class='active' @endif>
								  	<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Mis Ordenes
								  </a>
							</li>
							<li>
								<a href="{{route('user.tickets')}}" @if(request()->segment(2) == 'tickets') class='active' @endif>
								  	<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> Tickets de Ayuda
								  </a>	
							</li>
							
						</ul>
						@endif
						<ul class="mob-menu-3">						
							<li>
								<a href="{{route('blog')}}" class="open-mob-blog">Blog</a>
								<!--BLOG SUBMENU-->
								@if(isset($cats))
								<ul class="blog-submenu blog-sbmenu-mob">							
									@foreach($cats as $cat)
									<li>
										<a href="{{route('blog.category', [ 'categorySlug' => $cat->slug ])}}">{{ $cat->nombre }}</a>
									</li>
									@endforeach							
								</ul>
								@endif
							</li>
							<li>
								<a href="{{route('nosotros')}}">Nosotros</a>
							</li>
							<li>
								<a href="{{route('contacto')}}">Contacto</a>
							</li>
							<li>
								<a href="https://www.facebook.com/angelesrevista/" target="_blank" title="Facebook Angeles">
									<img src="{{asset('images/fb-head.png')}}">
								</a>
								<a href="#" target="_blank" title="Twitter Angeles">
									<img src="{{asset('images/twitter-head.png')}}">
								</a>
								<a href="https://www.instagram.com/angelesrevistaoficial/" target="_blank" title="Instagram Angeles">
									<img src="{{asset('images/instagram-header.png')}}">
								</a>
							</li>
						</ul>
					</div>
				</nav><!--/user menu-->
			</div><!--/second head-->
		</div><!--/div-->
	</header><!--/cabecera-->