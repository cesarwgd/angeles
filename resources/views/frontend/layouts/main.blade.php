<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="@yield('meta-description')" />
<meta name="keywords" content="@yield('meta-keywords')" />
<meta name="author" content="Angeles Revista" />
<title>@yield('meta-title')</title>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/main.css')}}?v=1" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{asset('js/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/functions.js')}}"></script>
@yield('user-scripts')
@yield('page-specific-scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-88370713-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-88370713-1');
</script>
<!--ADSENSE-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4883441401822180",
    enable_page_level_ads: true
  });
</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2012007368908371');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=2012007368908371&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.12&appId=355331158315172&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@if(isset($page_id) && $page_id == "home")
	@include('frontend.layouts.includes.header_home')
@else
	@include('frontend.layouts.includes.header_rest')
@endif

@yield('content')

	<footer class="pie"><!--pie-->
		<div class="copyright"><!--copyright-->
			© <?php echo date("Y"); ?> Ángeles Revista - Todos los derechos reservados
		</div><!--/copyright-->
		<div class="footer-redes-otros"><!--footer redes otros-->
			<div class="pull-right">
				<ul>
					<li><a href="{{route('terminos')}}">Terminos y Condiciones</a></li>
					<li><a href="{{route('nosotros')}}">Nosotros</a></li>
					<li><a href="{{route('contacto')}}">Contacto</a></li>
				</ul>
				<ul>
					<li class="fb-footer">
						<a href="https://www.facebook.com/angelesrevista/" target="_blank"><img src="{{asset('images/fb-footer.png')}}"></a>
					</li>
					<li class="twitter-footer">
						<a href="#" target="_blank"><img src="{{asset('images/twitter-footer.png')}}"></a>
					</li>
					<li class="instagram-footer">
						<a href="https://www.instagram.com/angelesrevistaoficial/" target="_blank"><img src="{{asset('images/instagram-footer.png')}}"></a>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div><!--/footer redes otros-->
	</footer><!--/pie-->
@yield('footer_page_scripts')
<script type="text/javascript" src="{{asset('js/noimages.js')}}"></script>
</body>
</html>	