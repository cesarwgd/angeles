@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Nosotros- Revista Angeles - Te traemos el paraíso 
@endsection


@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title nosotros-maintitle"><!--main title-->
			<h1>Términos y condiciones de uso del sitio Web</h1>			
					
		</header><!--/main title-->
		
		<div class="contenedor contacto-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-12 nosotros-text align-left caja-full-texto"><!--form-->
				<hr>
				<br><br>	

				<h3>Condiciones de uso</h3>
				<p>
					El usuario del sitio Web (www.angelesrevista.com) se compromete a leer detenidamente los términos y condiciones generales, antes de utilizar los portales y servicios Web que ofrece GRUPO NUTANGEL S.A.C. (en adelante, “NUTANGEL”). Ello implica que usted acepta expresamente los términos y condiciones generales. En caso de no aceptarlos, se le solicita que no haga uso, ni acceda, ni manipule la información de los servicios ofrecidos por el sitio Web, ya que usted (usuario) está haciendo un uso inadecuado de éste.
				</p>
				<p>
					Este sitio web tiene por objeto la promoción y divulgación de las actividades, material audiovisual, y demás relacionados a las actividades de GRUPO NUTANGEL S.A.C., y la Revista “Ángeles”.
				</p>

				<h3>Derechos de Propiedad Intelectual</h3>

				<p>
					Todos los derechos de propiedad intelectual de la página web son de propiedad de NUTANGEL. Se prohíbe el uso de cualquier derecho de propiedad intelectual sin contar con el consentimiento previo, expreso y por escrito de aquella.
				</p>

				<h3>Derechos de Autor</h3>
				<p>
					Este sitio se encuentra protegido por la normativa vigente sobre derechos de autor. Todos los derechos involucrados, como por ejemplo su contenido y su diseño visual, son de titularidad de <strong>NUTANGEL</strong>, por lo que se encuentra estrictamente prohibido su empleo, modificación, reproducción, distribución, transmisión o comercialización de los derechos involucrados sin el permiso previo, expreso y por escrito de <strong>NUTANGEL.</strong>
				</p>

				<h3>Signos distintivos y patentes</h3>
				<p>
					Todos los signos distintivos (marcas de productos y de servicio, lemas comerciales y nombres comerciales) y patentes registrados de acuerdo a la legislación nacional y extranjera, son de única y exclusiva propiedad de NUTANGEL.
				</p>
				<p>Se prohíbe el uso de cualquier elemento de propiedad intelectual de NUTANGEL sin el consentimiento por escrito previo pertinente.</p>

				<h3>Aceptación de términos</h3>
				<p>
					El sitio web y referencias están disponibles para que el usuario bajo su responsabilidad los utilice adecuadamente sin aprovecharse de alguna falla que ocurra y saque provecho de la misma. Si encuentra alguna falla en nuestro sitio web reportarla a <strong>soporte@angelesrevista.com</strong> o a <strong>angelesrevista@gmail.com</strong>.
				</p>
				<p>
					Los cambios y actualizaciones en los términos y condiciones de uso serán comunicados en nuestro sitio web oportunamente para que todos los usuarios se informen de los términos y condiciones de uso vigente. Está bajo la responsabilidad del usuario enterarse de los nuevos cambios y restringirse a cumplir como normativa del sitio web.
				</p>
				<p>
					Se prohíbe incluir en sitios y portales no controladas por <strong>NUTANGEL</strong>, enlaces o páginas interiores, evitando así no visitar la página principal. Se prohíbe igualmente el despliegue, exhibición o reproducción del sitio.
				</p>
				<p>
					El usuario se compromete a usar de forma diligente y lícita los contenidos de la web, obligándose a no reproducir, copiar o distribuir información que no sea pública de otros enlaces que no incluya las referencias al portal de <strong>NUTANGEL</strong>.
				</p>

				<h3>Obligaciones de los usuarios registrados</h3>
				<ul>
					<li>El acceso a los servicios a través del Código de Usuario y Contraseña entregados es de su plena y exclusiva responsabilidad, así como por el uso de los mismos.</li>
					<li>El uso diligente del Código de Usuario y Contraseña entregados, mantener y preservar la confidencialidad de los mismos.</li>
					<li>
						Notificar inmediatamente de cualquier uso no autorizado del Código de Usuario y Contraseña o cualquier otra infracción a la seguridad de la misma no siendo el área de sistemas en ningún caso responsable de las acciones realizados por la cuenta del usuario.
					</li>
					<li>
						Toda transacción realizada con su código de Usuario y contraseña será vigilada para que no intente hacer de un mal uso los servicios que se les brinda. En caso contrario, no lo cumpla será acreedor de una sanción.
					</li>
					<li>
						No usar el Código de Usuario y Contraseña para ningún propósito ilegal.
					</li>
				</ul>

				<h3>Obligaciones de los usuarios en general</h3>
				<ul>
					<li>El acceso de los servicios web son carácter público en zonas públicas de la Web donde la información entregada es de la referida Revista “Ángeles”. El usuario se obliga a utilizar los servicios y contenidos que le proporciona el portal conforme a la legislación vigente y a los principios de buena fe y usos generalmente aceptados y a no contravenir con su actuación a través de la web.</li>
					<li>
						El usuario se compromete a suministrar información verdadera y exacta acerca de él mismo en los formularios de registro del portal de la Revista “Ángeles”.
					</li>
				</ul>

				<h3>Modificaciones a los Términos y Condiciones</h3>
				<p>
					<strong>NUTANGEL</strong> se reserva el derecho de modificar el contenido y alcance de los presentes Términos y Condiciones en cualquier momento y según lo considere necesario. El Usuario estará obligado a sujetarse a dichas modificaciones, una vez que éstas entren en vigor.
				</p>

				<h3>Delimitación de responsabilidad</h3>

				<p>
					<strong>NUTANGEL</strong> realiza los mayores esfuerzos para brindar la más actualizada y exacta información en este sitio. Sin perjuicio de lo anterior, en algunas oportunidades puede incurrir en errores o en omisiones. En tales hipótesis NUTANGEL no será responsable del daño, perjuicio directo, indirecto, incidental, consecuente o especial que se derive del uso de la información contenida en este sitio o de las páginas web aquí recomendados.
				</p>
				<p>
					Toda la información que es publicada en esta página web, la cual incluye, pero no se limita, a los gráficos, textos y referencias a otras páginas, se proporciona "como es" y está sujeta a posibles modificaciones sin aviso previo. La información a la que se hace referencia es proporcionada sin garantía de cualquier clase, expresa o implícita, incluyendo pero no limitándose a una garantía de negocios y similares o a encontrarse libre de todo virus de computación.
				</p>
				<p>
					<strong>NUTANGEL</strong> no garantiza, en ningún caso, la suficiencia, exactitud e integridad de la información contenida en este sitio o en las páginas web recomendadas por lo que no asumirá ninguna clase de responsabilidad por los errores o las omisiones en las que pudiera incurrir. Los visitantes son enteramente responsables de verificar y evaluar la exactitud, integridad y la utilidad de cualquier información que se encuentre disponible desde esta página web.
				</p>
				<p>
					<strong>NUTANGEL</strong> no garantiza un servicio libre e interrumpido de esta página pero sí declara su voluntad de efectuar los esfuerzos que, dentro de lo razonables, permitan alcanzar su disponibilidad por el mayor tiempo posible.
				</p>

				<h3>Política sobre protección de datos personales</h3>
				<p>
					<strong>NUTANGEL</strong> garantiza la seguridad y confidencialidad en el tratamiento de los datos de carácter personal facilitados por los usuarios, de conformidad con lo dispuesto en la Ley N° 29733, Ley de Protección de Datos Personales, su Reglamento, aprobado por Decreto Supremo N° 003-2013- JUS y/o sus normas reglamentarias, complementarias, modificatorias, sustitutorias y demás disposiciones aplicables (en adelante, la Ley).
				</p>
				<p>
					Toda información proporcionada a <strong>NUTANGEL</strong> a través de su sitios web, <strong>[X]</strong> , será objeto de tratamiento automatizado e incorporada en una o más bases de datos de las que NUTANGEL será titular y responsable, conforme a los términos previstos por la Ley. Al confirmar de acuerdo a los formularios, ficha en formato digital o fisicofísico , el usuario otorga autorización libre, expresa e inequívoca a NUTANGEL para realizar tratamiento y hacer uso de la información personal que éste proporcione a NUTANGEL cuando se registre como usuario en la web de Revista “Ángeles”, haga una consulta en línea, participe en promociones comerciales, envíe consultas o comunique incidencias, y en general cualquier interacción con la referida web, además de la información que se derive del uso de productos y/o servicios que pudiera tener contratados con NUTANGEL y de cualquier información pública o que pudiera recoger a través de fuentes de acceso público, incluyendo aquellos a los que NUTANGEL tenga acceso como consecuencia de su navegación por esta página web (en adelante, la “Información”) para las finalidades de envío de comunicaciones comerciales, comercialización de productos y servicios, y del mantenimiento de su relación contractual con NUTANGEL; todo ello de acuerdo a los términos que aquí se establecen. El usuario reconoce y acepta que NUTANGEL podrá ceder sus datos personales a cualquier tercero, para lo cual el usuario manifiesta su conformidad anticipada.
				</p>
				<p>
					<strong>NUTANGEL</strong> podrá ceder, a su vez, la Información a sus empresas subsidiarias, filiales, asociadas, afiliadas o miembros del grupo económico al cual pertenece y/o terceros con los que éstas mantengan una relación contractual, supuesto en el cual sus datos serán almacenados en los sistemas informáticos de cualquiera de ellos.
				</p>
				<p>
					En todo caso, <strong>NUTANGEL</strong> garantiza el mantenimiento de la confidencialidad y el tratamiento seguro de la Información en estos casos. El uso de la Información por las empresas antes indicadas se circunscribirá a los fines contenidos en este documento.
				</p>
				<p>
					La política de privacidad de <strong>NUTANGEL</strong> le asegura al usuario el ejercicio de los derechos de información, acceso, actualización, inclusión, rectificación, supresión o cancelación, oposición y revocación del consentimiento, en los términos establecidos en la Ley. En cualquier momento, el usuario tendrá el derecho a solicitar a NUTANGEL el ejercicio de los derechos que le confiere la Ley, así como la revocación de su consentimiento según lo previsto en la Ley.
				</p>
				<p>
					<strong>NUTANGEL</strong> garantiza la confidencialidad en el tratamiento de los datos de carácter personal, así como haber adoptado los niveles de seguridad de protección de los datos personales, instalado todos los medios y adoptado todas las medidas técnicas, organizativas y legales a su alcance que garanticen la seguridad y eviten la alteración, pérdida, tratamiento o acceso no autorizado a los datos personales.
				</p>
				<p>
					Para cualquier consulta sobre los alcances de la política sobre protección de datos personales, o en caso los usuarios deseen ejercitar los derechos de información, de acceso, actualización, inclusión, rectificación, supresión o cancelación, oposición, a impedir el suministro, al tratamiento objetivo, u otros contemplados en la Ley sobre sus datos personales, podrán enviar un correo electrónico a: <strong>ventas@angelesrevista.com</strong> / <strong>angelesrevista@gmail.com</strong> / <strong>soporte@angelesrevista.com</strong>
				</p>


				</div><!--/form-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection