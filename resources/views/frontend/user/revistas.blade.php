@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->

								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}} / Mis Revistas
									</h1>
									<br><br>

									@if(session()->has('exito'))
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif

									<?php 
										$roles = ['admin', 'premium'];
										// PREMIUM USERS OR ADMINS
									?>


									@if($user->hasAnyRole($roles))
										@foreach($revistas as $mag)
										<div class="user-revista-block"><!--user revista block-->
											<div class="thumb-picture">
												@if(!empty($mag->main_pic))
												<figure>
													@if($mag->type == "revista")
													<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}" class="thumbnail">
													@else
													<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}" class="thumbnail">
													@endif
													@if(Storage::disk('rev_thumbs')->exists($mag->main_pic . "-thumb." . $mag->extension))
												      <img src="{{Storage::disk('rev_thumbs')->url($mag->main_pic . '-thumb.' . $mag->extension)}}">
												      @endif
												    </a>
												</figure>
												@endif											
											</div>
											<div class="user-revista-title">
												<h2>{{$mag->name}}</h2>
											</div>
											<div class="user-revista-view">
												@if($mag->type == "revista")
												<a href="{{route('watch.issue', ['hash_slug' => $mag->hash_slug, 'page' => 1])}}" class="btn btn-primary">
												@else
												<a href="{{route('watch.issue.video', ['hash_slug' => $mag->hash_slug])}}" class="btn btn-primary">
												@endif
												Ver revista
												</a>
											</div>
										</div><!--/user revista block-->
										@endforeach
									@else

									<?php 
										// REGULAR USERS
									?>
										
										@if(count($user->revistas) < 1)
											<h3>Aún no tienes ninguna revista en tu cuenta</h3>
											<p>Adquiere nuestras revistas <a href="{{route('revistas')}}">AQUÍ</a></p>
										@else
											@foreach($user->revistas()->orderBy('id', 'desc')->get() as $revista)
											<div class="user-revista-block"><!--user revista block-->
												<div class="thumb-picture">
													@if(!empty($revista->main_pic))
													<figure>
														@if($revista->type == "revista")
														<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => 1])}}" class="thumbnail">
														@else
														<a href="{{route('watch.issue.video', ['hash_slug' => $revista->hash_slug])}}" class="thumbnail">
														@endif
														@if(Storage::disk('revista')->exists($revista->main_pic . "." . $revista->extension))
													      <img src="{{route('frontend.revista.privateimage', ['filename' => $revista->main_pic, 'extension' => $revista->extension])}}">
													      @endif
													    </a>
													</figure>
													@endif											
												</div>
												<div class="user-revista-title">
													<h2>{{$revista->name}}</h2>
												</div>
												<div class="user-revista-view">
													@if($revista->type == "revista")
													<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => 1])}}" class="btn btn-primary">
													@else
													<a href="{{route('watch.issue.video', ['hash_slug' => $revista->hash_slug])}}" class="btn btn-primary">
													@endif
													Ver revista
													</a>
												</div>
											</div><!--/user revista block-->
											@endforeach
										@endif

									@endif		


									


									<!--<div class="alert alert-info">
										<p>Quieres tener acceso inmediato a todas las revistas?. Actualiza tu cuenta a <span class="label label-success"><a href="#" style="color:#FFF;">PREMIUM</a></span></p>
									</div>-->


								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection