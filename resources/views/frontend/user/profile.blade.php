@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->
								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}}
									</h1>

									@if(session()->has('exito'))
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif

									@if ($errors->any())
									    <div class="alert alert-danger">
									        <ul>
									            @foreach ($errors->all() as $error)
									                <li>{{ $error }}</li>
									            @endforeach
									        </ul>
									    </div>
									@endif
								
									<form action="{{route('update.user')}}" method="post" id="update-my-profile">
										<div class="form-group">
											<input type="text" name="name" placeholder="Nombres" class="form-control" value="{{$user->name}}">
										</div>
										<div class="form-group">
											<input type="text" name="last_name" placeholder="Apellidos" class="form-control" value="{{$user->last_name}}">
										</div>
										<div class="form-group">
											<input type="email" name="email" placeholder="Correo" class="form-control" value="{{$user->email}}">
										</div>
										<hr>
										<div class="form-group">
											<label for="password">Actualizar contraseña (mínimo 6 caracteres):</label>									

											<input type="password" name="password" placeholder="******" id="password" class="form-control">											
										</div>
										<div class="form-group">
											<label for="passwordConfirm">Confirmar contraseña:</label>
											<input type="password" name="confirm-password" placeholder="" id="passwordConfirm" class="form-control">
										</div>
										@if(!empty($user->fb_id))
											<div class="alert alert-info">
												Usted ingreso por primera vez a nuestro sitio a través de su cuenta de Facebook.<br>
												<strong>Actualice su contraseña sólo si en un futuro planea ya no acceder con su cuenta de Facebook.</strong>
											</div>
											@endif
										<button type="submit" class="btn btn-primary" id="update-profile">
											Actualizar
										</button>
										{{csrf_field()}}
									</form>
								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
	<script type="text/javascript">
		$(function(){
			$("#update-profile").click(function(e){
				e.preventDefault();
				$(this).prop('disabled', true);
				$("#update-my-profile").submit();
			});
		});
	</script>
@endsection