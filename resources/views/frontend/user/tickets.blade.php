@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->

								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}} / Mis Tickets
									</h1>
									<button class="btn btn-primary">
										<a href="{{route('user.ticket.create')}}"><span class="glyphicon glyphicon-plus" aria="hidden"></span> Crear ticket de ayuda</a>
									</button>
									<br><br>

									@if(session()->has('exito'))
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif
									
									@foreach($tickets as $ticket)
									<div class="user-revista-block"><!--user revista block-->
										<h3><a href="{{route('user.ticket.view', ['id' => $ticket->id])}}">{{$ticket->asunto}}</a></h3>
										<h5>Creado el: {{$ticket->created_at}}</h5>
										<div>
											<p>{{substr($ticket->mensaje, 0, 150)}}...</p>
										</div>
										<p>
											<strong>Estado: </strong>
											@if($ticket->estado == "abierto")
												<span class="label label-primary">En Proceso</span>
											@else
												<span class="label label-success">Cerrado</span>
											@endif
										</p>
									</div><!--/user revista block-->
									@endforeach

									

									

									


								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection