@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->
								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}}
									</h1>

									@if($errors->any())
									    <div class="alert alert-danger">
									        <ul>
									            @foreach ($errors->all() as $error)
									                <li>{{ $error }}</li>
									            @endforeach
									        </ul>
									    </div>
									@endif

									@if(session()->has('exito'))
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif

									<ul class="nav nav-tabs select-order-tab">
									  <li role="presentation" class="active"><a href="#order-revista-tab">Revistas</a></li>
									  <li role="presentation"><a href="#order-productos-tab">Productos</a></li>
									</ul>

									<div class="order-tabs" id="order-revista-tab">
										<!--ORDENES REVISTAS-->
										<table class="table table-hover user-mob-front-orders">
											<thead> 
												<tr> 
													<th># Código</th> 
													<th>Descripción</th> 
													<th>Cantidad</th> 
													<th>Estado</th> 
													<th></th> 
												</tr> 
											</thead>
											<tbody>   
											@foreach($ordenes as $orden)
											<tr> 
												<th scope="row">{{$orden->code}}</th> 
												<td>{{$orden->description}}</td> 
												<td>S/ {{$orden->amount}}</td> 
												<td>
													@if($orden->status == "iniciada")
													<span class="label label-default">Incompleta</span>
													@elseif($orden->status == "pendiente")
													<span class="label label-info">En Proceso</span>
													@elseif($orden->status == "cancelada")
													<span class="label label-danger">Cancelada</span>
													@else
													<span class="label label-success">Completada</span>
													@endif
												</td>
												<td>
													@if($orden->status == "iniciada")
													<button type="button" class="btn btn-danger remove-order" data-id="{{$orden->id}}">X</button>
													@endif
												</td> 
											</tr> 
											@endforeach
											</tbody> 
										</table>

										@if($user->hasIncompleteOrders())
										<button class="btn btn-primary">
											<a href="{{route('user.checkout')}}">Completar proceso de compra</a>
										</button>
										@endif
									</div>

									<!--ORDER PRODUCTOS-->
									<div class="order-tabs" id="order-productos-tab">
										<!--ORDENES REVISTAS-->
										<table class="table table-hover user-mob-front-orders">
											<thead> 
												<tr> 
													<th># Código</th> 
													<th>Descripción</th> 
													<th>Cantidad</th> 
													<th>Estado</th> 
													<th></th> 
												</tr> 
											</thead>
											<tbody>   
											@foreach($productos_ordenes as $porden)
											<tr> 
												<th scope="row">{{$porden->code}}</th> 
												<td>{{$porden->description}}</td> 
												<td>{{$porden->quantity}} | S/ {{$porden->amount}}</td> 
												<td>
													@if($porden->status == "iniciada")
													<span class="label label-default">Incompleta</span>
													@elseif($porden->status == "pendiente")
													<span class="label label-info">En Proceso</span>
													@elseif($porden->status == "cancelada")
													<span class="label label-danger">Cancelada</span>
													@else
													<span class="label label-success">Completada</span>
													@endif
												</td>
												<td>
													@if($porden->status == "iniciada")
													<button type="button" class="btn btn-danger remove-product-order" data-id="{{$porden->id}}">X</button>
													@endif
												</td> 
											</tr> 
											@endforeach
											</tbody> 
										</table>

										@if($user->hasIncompleteProductOrders())
										<button class="btn btn-primary">
											<a href="{{route('user.product.checkout')}}">Completar proceso de compra</a>
										</button>
										@endif
									</div>

								</div><!--/col-->
							</div><!--/row-->
							<form action="{{route('user.remove.order')}}" method="POST" id="remover-orden">
						    	<input type="hidden" name="orden-id" id="orden-id-remover" value="">
						    	{{csrf_field()}}
						    </form>
						    <!--remove product order-->
						    <form action="{{route('user.product.remove.order')}}" method="POST" id="remover-producto-orden">
						    	<input type="hidden" name="orden-id" id="product-order-id-remover" value="">
						    	{{csrf_field()}}
						    </form>
						    <script type="text/javascript">
						    	$(".remove-order").click(function(){
						    		$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Removiendo de su carrito de compras..</div></div></div>");
						    		var $orden = $(this).data('id');
						    		$("#orden-id-remover").val($orden);
						    		$("#remover-orden").submit();
						    	});
						    	// remover producto orden

						    	$(".remove-product-order").click(function(){
						    		$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Removiendo de su carrito de compras..</div></div></div>");
						    		var $orden = $(this).data('id');
						    		$("#product-order-id-remover").val($orden);
						    		$("#remover-producto-orden").submit();
						    	});

						    	// tabs

						    	$(".select-order-tab li a").click(function(){
						    		var tabId = $(this).attr('href');
						    		$(".select-order-tab li").removeClass('active');
						    		$(this).parent().addClass('active');
						    		$(".order-tabs").hide();
						    		$(tabId).show();
						    		return false;
						    	});
						    </script>
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection

