@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->
								<div class="col-xs-12 col-sm-10"><!--col-->
									
									@if(session()->has('exito') && session()->get('exito') == true)
									<h1>
										{{Auth::user()->name}} {{Auth::user()->last_name}}
									</h1>

									<div class="panel panel-default"><!--panel-->
									  <div class="panel-heading">ORDEN CREADA</div>
									  <div class="panel-body">
									    <p>
									    	Su orden ha sido creada!. <strong>Revise su correo electrónico</strong> donde le hemos enviado las instrucciones que tiene que seguir para finalizar su proceso de compra.							    	
									    </p>
									    <p>Si no encuentra el correo en su bandeja de entrada por favor revise su bandeja de <strong>spam</strong> o <strong>correo no deseado</strong>.</p>
									    <p>Cualquier duda o consulta puede escribirnos a: <strong>ventas@angelesrevista.com</strong> indicándonos el número de orden de sus compras, las cuales puede revisar <a href="{{route('user.ordenes')}}">ACÁ</a>.</p>
									  </div>
									</div><!--/panel-->	
									@else
									<p>NO HA REALIZADO NINGUNA COMPRA.</p>
									@endif							
								
									
								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection

