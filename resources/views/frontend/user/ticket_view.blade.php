@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->

								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}} / Ticket ID: #{{$ticket->id}}
									</h1>

									@if(session()->has('exito'))
									<br><br>
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif

									@if ($errors->any())
									    <div class="alert alert-danger">
									        <ul>
									            @foreach ($errors->all() as $error)
									                <li>{{ $error }}</li>
									            @endforeach
									        </ul>
									    </div>
									@endif

									<div class="ticket-log-box"><!--ticket log box-->

										<div class="panel panel-default"><!--panel-->
										  <div class="panel-heading">El {{$ticket->created_at}} <strong>Usted</strong> escribió:</div>
										  <div class="panel-body">
										    <p>
										    	{{$ticket->mensaje}}
										    </p>
										  </div>
										</div><!--/panel-->

										@foreach($mensajes as $mensaje)
										<div class="panel panel-<?php if($mensaje->user_id != $ticket->user_id){ echo "primary"; }else{ echo "default"; }?>"><!--panel-->
										  <div class="panel-heading">El {{$mensaje->created_at}} <strong>@if($mensaje->user_id != $user->id) Soporte @else Usted @endif</strong> escribió:</div>
										  <div class="panel-body">
										    <p>
										    	{{$mensaje->mensaje}}
										    </p>
										  </div>
										</div><!--/panel-->
										@endforeach

									</div><!--/ticket log box-->

									
									
									@if($ticket->estado == "abierto")
									<button class="btn btn-primary" id="show-reply-ticket">
										<a href="#"><span class="glyphicon glyphicon-plus" aria="hidden"></span> Responder</a>
									</button>
									<br><br>
									<div class="reply-ticket"><!--div-->

										<form action="{{route('user.ticket.reply', ['id' => $ticket->id])}}" method="post" id="ticket-form-reply"><!--form-->											

											<div class="form-group">
												<label for="ticketDetalle">Detalle:</label>
												<textarea name="detalle" id="ticketDetalle" class="form-control" rows="6" required maxlength="500" onkeyup="countChar(this)"></textarea>
												<div>
													<span class="contador-caracteres"></span> / 500 max.
												</div>
												<input type="hidden" name="user_id" value="{{$user->id}}">
												<input type="hidden" name="name" value="{{$user->name}}">
												<input type="hidden" name="last_name" value="{{$user->last_name}}">
												<input type="hidden" name="email" value="{{$user->email}}">
												<input type="hidden" name="ticket_id" value="{{$ticket->id}}">
												<input type="hidden" name="tipo" value="{{$ticket->tipo}}">
												<input type="hidden" name="asunto" value="{{$ticket->asunto}}">
											</div>

											{{csrf_field()}}

											<button type="submit" class="btn btn-primary">
												Enviar
											</button>
											<div class="enviando">
												...Enviando Mensaje...
											</div>

										</form><!--/form-->

									</div><!--/div-->
									<script type="text/javascript">
									// count characters left
										function countChar(val) {
									        var len = val.value.length;
									        if (len >= 500) {
									          val.value = val.value.substring(0, 500);
									        } else {
									          $('#ticketDetalle').text(500 - len);
									        }
									        $('.contador-caracteres').html(len);
									      };
										// send form
										$("#ticket-form-reply").submit(function(){
											$(".enviando").show();
										});
									</script>
									@else
									<div class="alert alert-info">
										Este ticket ya ha sido solucionado. Si desea reportar un problema nuevo por favor genere un nuevo ticket de soporte.
									</div>
									@endif

								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection