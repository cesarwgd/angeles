@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-9"><!--col-->
							
							<div class="row"><!--row-->

								<div class="col-xs-12 col-sm-10"><!--col-->
									<h1>
										{{$user->name}} {{$user->last_name}} / Crear Ticket de Ayuda
									</h1>								
									
									@if ($errors->any())
									    <div class="alert alert-danger">
									        <ul>
									            @foreach ($errors->all() as $error)
									                <li>{{ $error }}</li>
									            @endforeach
									        </ul>
									    </div>
									@endif
									
									<div><!--div-->

										<form action="{{route('user.ticket.post')}}" method="post" id="ticket-form-create"><!--form-->

											<div class="form-group">
												<div class="row">
													<div class="col-xs-12 col-sm-4">
														<label for="ticketTipo">Seleccione el tipo de ticket *:</label>
														<select class="form-control" name="tipo" id="ticketTipo" required>
															<option disabled selected>----</option>
															<option value="revista">Revista</option>
															<option value="pagos">Pagos</option>
															<option value="cuenta">Cuenta</option>
															<option value="otro">Otro</option>
														</select>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label for="ticketAsunto">Asunto *:</label>
												<input type="text" name="asunto" id="ticketAsunto" class="form-control" required>
											</div>

											<div class="form-group">
												<label for="ticketDetalle">Detalle *:</label>
												<textarea name="detalle" id="ticketDetalle" class="form-control" rows="6" required></textarea>
											</div>
											<p><strong>* Campos obligatorios</strong></p>
											<input type="hidden" name="email" value="{{$user->email}}">
											<input type="hidden" name="user_id" value="{{$user->id}}">
											<input type="hidden" name="name" value="{{$user->name}}">
											<input type="hidden" name="last_name" value="{{$user->last_name}}">

											{{csrf_field()}}

											<button type="submit" class="btn btn-primary enviar-ticket">
												Enviar
											</button>
											<div class="enviando">
												...Enviando Mensaje...
											</div>


										</form><!--/form-->

									</div><!--/div-->

									<script type="text/javascript">
										$(".enviar-ticket").click(function(e){
											e.preventDefault();
											var tipo = $("#ticketTipo").val();
											if(!tipo){
												alert('Debe de elegir el tipo de tiket');
											}else{
												$(".enviando").show();
												$("#ticket-form-create").submit();												
											}
											
										});

									</script>
								</div><!--/col-->
							</div><!--/row-->
							
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 pull-right user-sidebar"><!--col-->
							
							@include('frontend.layouts.includes.user_right_sidebar')
							
						</div><!--/col-->

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection