@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso 
@endsection

@section('user-scripts')
<script type="text/javascript" src="{{asset('js/users.js')}}"></script>
@endsection
@section('page-specific-scripts')
<script src="https://checkout.culqi.com/v2"></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		
		
		<div class="contenedor user-main"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-2 desktop-profile-menu"><!--user nav-->

					@include('frontend.layouts.includes.user_menu')
					
				</div><!--/user nav-->

				<div class="col-xs-12 col-sm-10 user-box"><!--user box-->
					
					<div class="row"><!--row-->
						
						<div class="col-xs-12 col-sm-12"><!--col-->
							
							<div class="row"><!--row-->
								<div class="col-xs-12 col-sm-12 col-md-7"><!--col-->									

									@if ($errors->any())
									    <div class="alert alert-danger">
									        <ul>
									            @foreach ($errors->all() as $error)
									                <li>{{ $error }}</li>
									            @endforeach
									        </ul>
									    </div>
									@endif

									@if(session()->has('exito'))
									<div class="alert alert-success">
										{{session()->get('exito')}}
									</div>
									@endif

									<!--FORMULARIO PARA LAS COMPRAS-->

									@if($hasOrders != null)
									<div class="panel panel-default"><!--panel-->
									  <div class="panel-heading">
									  	<h4><strong>{{$user->name}} {{$user->last_name}} - FINALIZAR COMPRA</strong></h4>
									  </div>
									  <div class="panel-body">
									    <div class="row"><!--row-->
									    	
									    	<?php 
									    		$culqi_description = "";
									    	?>
										    
										    @foreach($orders as $order)
										    <div class="col-xs-6 col-sm-8">
										    	<h4>{{$order->description}}</h4>
										    </div>
										    <div class="col-xs-6 col-sm-4">
										    	<h5> S/ {{$order->amount}} | <button type="button" class="btn btn-danger remove-order" data-id="{{$order->id}}">X</button></h5> 
										    </div>
										    <?php $culqi_description .= $order->description . " | "; ?>
										    @endforeach	
										    <form action="{{route('user.remove.order')}}" method="POST" id="remover-orden">
										    	<input type="hidden" name="orden-id" id="orden-id-remover" value="">
										    	{{csrf_field()}}
										    </form>
										    <script type="text/javascript">
										    	$(".remove-order").click(function(){
										    		$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Removiendo de su carrito de compras..</div></div></div>");
										    		var $orden = $(this).data('id');
										    		$("#orden-id-remover").val($orden);
										    		$("#remover-orden").submit();
										    	});
										    </script>
										    <!--TOTAL-->
										    <div class="clearfix"></div>
										    <div class="col-xs-8 col-sm-8">										    	
										    </div>
										    <div class="col-xs-4 col-sm-4">
										    	<h4><strong>TOTAL: S/ <span class="total-price">{{$total}}</span></strong></h4>
										    </div>

										    <hr style="height:1px; width:100%;">

										    <h4 style="padding-left:15px;">Otras revistas que te podrían interesar</h4>
										    
										    	@foreach($otras as $otra)
										    	<div class="col-xs-6 col-md-3">
										    		@if(!empty($otra->main_pic))
													<figure class="checkout-otras-pic">
														<a href="{{route('revistas.preview', ['slug' => $otra->slug])}}" title="{{$otra->name}}" class="thumbnail">
														@if(Storage::disk('revista')->exists($otra->main_pic . "." . $otra->extension))
													      <img src="{{route('frontend.revista.privateimage', ['filename' => $otra->main_pic, 'extension' => $otra->extension])}}">
													      @endif
													    </a>
													</figure>
													@endif		
										    		<h5><a href="{{route('revistas.preview', ['slug' => $otra->slug])}}">{{$otra->name}}</a></h5>
										    	</div>
										    	@endforeach
										    

										    

										    

									    </div><!--/row-->
									  </div>
									</div><!--/panel-->
									

									@else

									<div class="alert alert-info">
										Usted no tiene ninguna orden de compra en su carrito de compras.
									</div>

									@endif

									<!--/FIN DEL FORMULARIO PARA LAS COMPRAS-->
								
									
								</div><!--/col-->

								<!--PAGUE-->
								<div class="col-xs-12 col-sm-12 col-md-5"><!--col-->

								@if($hasOrders != null)
									<!-- CHOOSE PAYMENT TYPE -->
									<div class="panel panel-primary"><!--panel-->

										<div class="panel-heading"><!--panel heading-->
											<h4><strong>FINALIZAR COMPRA</strong></h4>
										</div><!--/panel heading-->

										<div class="panel-body"><!--panel body-->
											
											<div class="row"><!--row-->

									    		<div class="col-xs-12 col-sm-12"><!--col-->										    			
									    			
									    			<h4>Pago con tarjeta de crédito</h4>
									    			<p>Ingrese a continuación los datos solicitados para finalizar su proceso de compra.</p>

													<div class="alert alert-danger culqi-errors"></div>
													@if(session()->has('errorCulqi'))
													<div class="alert alert-danger">
														<p>Hubo un error al procesar su pago con la tarjeta ingresada:</p>
														<p><strong>{{session()->get('errorCulqi')}}</strong></p>
													</div>
													@endif

													<!-- CULQI -->

													<div id="show-creditcard-payment"><!--checkout form payment-->
													<?php 
														$culqi_total = $total * 100;
													?>
													<script>
													    Culqi.publicKey = 'pk_live_MkSnaxsbMFfSAnKL';
													    Culqi.init();
													</script>

													<div class="row">

													
														<form action="{{route('user.culq.payment')}}" method="POST" id="culqi-card-form">

														  <div class="col-xs-12 col-sm-12"><!--COL-->
														  	<div class="form-group">
														      	<label>
														         	<span>Correo Electrónico</span>
														      		<input type="text" size="50" data-culqi="card[email]" id="card[email]" class="form-control" name="culqiEmail" required>
														    	</label>
														  	</div>
														  </div><!--/COL-->

														  <div class="col-xs-12 col-sm-8"><!--COL-->
														  	<div class="form-group">
															    <label>
															      <span>Número de tarjeta</span>
															    </label>  
															    <input type="text" size="20" data-culqi="card[number]" id="card[number]" class="form-control" required>																    
															</div>
														  </div><!--/COL-->

														  <div class="col-xs-12 col-sm-4"><!--COL-->
														  	<div class="form-group">
															    <label class="cvc-box">
															      <span>CVC</span> <span class="glyphicon glyphicon-info-sign show-cvc-example" aria-hidden="true"></span>
															      <div class="this-is-cvc">
															      	<img src="{{asset('images/cvc-code.jpg')}}">
															      </div>
															    </label>  
															    <input type="text" size="4" data-culqi="card[cvc]" id="card[cvv]" class="form-control" required>																    
															</div>
														  </div><!--/COL-->

														  <div class="col-xs-12">
														  	<label>
															      <span>Fecha expiración (MM/YYYY)</span>
															    </label>
														  </div>

														  <div class="col-xs-12 col-sm-4">
														  	<div class="form-group">															    
															      <input type="text" size="2" data-culqi="card[exp_month]" id="card[exp_month]" class="form-control" placeholder="MM" maxlength="2" required>							    
															</div>
														  </div>

														  <div class="col-xs-12 col-sm-8">
														  	<div class="form-group">														  		
															    <input type="text" size="4" data-culqi="card[exp_year]" id="card[exp_year]" class="form-control" placeholder="YYYY" maxlength="4" required>	
														  	</div>
														  </div>
														  <input type="hidden" name="culqiToken" value="" id="culqi-token">
														  <input type="hidden" name="culqiDescription" value="{{$culqi_description}}" id="culqi-description">
														  <input type="hidden" name="culqiAmount" value="{{$culqi_total}}" id="culqi-amount">
														  @foreach($orders as $order)
														  <input type="hidden" name="revistas_id[]" value="{{$order->revista_id}}">
														  <input type="hidden" name="order_description[]" value="{{$order->description}}">
														  <input type="hidden" name="order_code[]" value="{{$order->code}}">
														  <input type="hidden" name="order_id[]" value="{{$order->id}}">
														  @endforeach
														  
														  {{csrf_field()}}
														  <div class="col-xs-12">
														  	<button class="btn btn-primary" id="pagar-con-culqi">Pagar</button>
														  </div>
														</form>
														
													</div>
													
													
													<!-- Muestra el Checkout de Culqi -->
													
													</div><!--/checkout form payment-->
													<br>
									    		</div><!--/col-->									    		

									    		

									    	</div><!--/row-->

										</div><!--/panel body-->

									</div><!--/panel-->
									<p class="peque">Al efectuar su compra usted esta aceptando nuestros <a href="{{route('terminos')}}" target="_blank">términos y condiciones de uso</a></p>
								@endif
							
									
									
								</div><!--/col-->

							</div><!--/row-->
							
						</div><!--/col-->

						

					</div><!--/row-->

				</div><!--/user box-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection

@section('footer_page_scripts')

<script type="text/javascript" src="{{asset('js/culqicheckout.js')}}"></script>
@endsection