<?php $page_id = "home"; ?>
@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Revista Angeles - Te traemos el paraíso
@endsection

@section('page-specific-scripts')
<link rel="stylesheet" type="text/css" href="{{asset('css/slick.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('css/slick-theme.css')}}"/>
<script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
@endsection

@section('content')

@if(!empty($ad))
<div class="home-main-ad">
	<div>
		<a href="#" class="close-h-ad">X</a>
		@if($ad->type == "image")
		<a href="{{$ad->redirect_to}}" target="_blank"><img src="{{Storage::disk('home')->url($ad->media_name)}}"></a>
		@elseif($ad->type == "video")
		{!!$ad->media_name!!}
		@endif
	</div>
</div>
@endif
	<div class="home-banner"><!--home banner-->	
		<div class="home-logo">
			<a href="{{route('home')}}"><img src="{{asset('images/logo.png')}}" alt="Revista Angeles" class="logo"></a>
		</div>
		<div class="carousel"><!--carousel-->
			<div class="activar-carousel">
				@foreach($banners as $banner)
					@if($banner->type == "image")
					<?php 
						$image = Storage::disk('home')->url($banner->image_name);
					?>
					<div style="background-image: url({{$image}});" class="diapos">
						<a href="{{$banner->url}}" class="home-comprar-ahora">Comprar ahora</a>			
					</div>
					@else
					<div class="diapos banner-home-video">
						@if(Storage::disk('home')->exists($banner->video_cover))
						<div class="banner-video-portada" style="background-image:url({{Storage::disk('home')->url($banner->video_cover)}});">
							<a href="#"></a>
						</div>
						@endif
						@if(Storage::disk('home')->exists($banner->video_name))
						<a href="javascript:void(0)" class="activar-video-chrome" onclick="alertar();" id="activar-video-chrome"></a>
						<video id="home-video-banner" preload="true">
						  <source src="{{ Storage::disk('home')->url($banner->video_name) }}" type="video/mp4">				  
						</video>
						<script type="text/javascript">
							function alertar(){
								$("#home-video-banner").get(0).play(); 
							}

						</script>	
						@endif						
										
					</div>
					@endif
				@endforeach
			</div>
		</div><!--/carousel-->	
	</div><!--/home banner-->
	
	<div class="home-main"><!--main-->
	<?php 
		$n = 1;
		$vid = 1;
		$t = count($boxes);
	?>
		@foreach($boxes as $box)
			@if($n == 1 && $t > 0)
			<div class="home-row"><!--home row-->
			@endif

				<div class="home-boxes"><!--home-boxes-->

					<div class="home-image<?php if($box->type == "video"){ echo " video-homepage";}?>">
						@if($box->type == "image")
						<?php $imagen = Storage::disk('home')->url($box->image_name); ?>
						<figure style="background-image:url({{$imagen}});"></figure>
						<div><!--div-->
							
							<div class="tabla"><!--tabla-->
								<div class="tabla-celda"><!--tabla celda-->
									<h2>
										<a href="{{$box->url}}">{{$box->description}}</a>
									</h2>
									
								</div><!--/tabla celda-->
							</div><!--/tabla-->
							
						</div><!--/div-->
						@else
							@if(Storage::disk('home')->exists($box->video_name))
								@if(Storage::disk('home')->exists($box->video_cover))
								<div class="video-portada" style="background-image:url({{Storage::disk('home')->url($box->video_cover)}});">
									<a href="#"></a>
								</div>
								@endif
							<video loop preload="auto" class="video-ang-h" id="mob-hv-{{$vid}}">
								<source src="{{Storage::disk('home')->url($box->video_name)}}" type="video/mp4" />							
							</video>
							@else
							{!!$box->video_name!!}
							@endif
						@endif
						
					</div>

				</div><!---homeboxes-->

			<?php 
				$n++;
				$vid++;
				$t--; 
			?>
			@if($n == 4 || $t == 0)
			</div><!--/home row-->
			<?php $n = 1;?>
			@endif

		
		@endforeach
		

		<div class="clearfix"></div>
		<a href="{{route('revistas')}}" class="home-more-blog">Más Revistas</a>


		<!--PARADISE-->

		<div class="home-more-row"><!--more-->

			<h3 class="section-title">PARADISE</h3>

			@foreach($angeles as $angel)
			<div class="home-boxes"><!--home-boxes-->

				<div class="home-image"><!--home image-->

					<figure style="background-image:url(<?php if(Storage::disk('hotmas')->exists($angel->main_pic)){ echo Storage::disk('hotmas')->url($angel->main_pic); } ?>);"></figure>
					<div><!--div-->
						<div class="tabla"><!--tabla-->
							<div class="tabla-celda"><!--tabla celda-->
								<h4><a href="{{route('hot.angel', ['slug' => $angel->slug])}}">{{$angel->nombre}}</a></h4>
							</div><!--/tabla celda-->
						</div><!--/tabla-->
					</div><!--/div-->
					
				</div><!--/home image-->

			</div><!---homeboxes-->
			@endforeach
			<div class="clearfix"></div>
			<a href="{{route('hot.max')}}" class="home-more-blog">Más Angeles</a>
						
			
		</div><!--more-->

		<!-- BLOG -->

		<div class="home-blog-row"><!--blog-->

			<h3 class="section-title">Blog</h3>
			
			@foreach($posts as $post)
			<div class="home-blog-box"><!--blog box home-->
				<div>
					<?php $categorySlug = $post->getCategorySlug(); ?>
					<?php $p_image = Storage::disk('blog')->url($post->main_image); ?>
					<figure style="background-image:url({{$p_image}});">
						<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}"></a>
					</figure>
					<section>
						<h5><a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}">{{$post->titulo}}</a></h5>
						<p>
							@if(!empty($post->extracto))
							{{$post->extracto}}
							@else
							<?php echo strip_tags(substr($post->contenido, 0, 180)); ?>
							@endif
							...
						</p>
						<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}" class="read-more">Continuar leyendo</a>
					</section>
				</div>
			</div><!--/blog box home-->
			@endforeach

			

			

			<div class="clearfix"></div>

			<a href="{{route('blog')}}" class="home-more-blog">Más notas</a>

		</div><!--/blog-->

	</div><!--/main-->

@endsection

@section('footer_page_scripts')
<script type="text/javascript">
$(document).ready(function(){

  $(window).load(function(){

  	$('.activar-carousel').slick({
	    dots: false,
	  	infinite: true,
	  	speed: 800,
	  	slidesToShow: 1,
	  	slidesToScroll: 1,
	  	autoplay: true,
	  	autoplaySpeed: 3000,
	  	pauseOnHover: false
	  });
  	
  	var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

	  if($("#home-video-banner").length > 0){
	  	$('.activar-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){	  
		  	if(!isMobile){
		  		if(nextSlide == 0){
			  		$("#home-video-banner").get(0).play();
			  	}
		  	}  
		});	  
	  }

	  //start video
	  if(!isMobile){  		
		  if($("a#activar-video-chrome").length > 0){
		  	$("a#activar-video-chrome")[0].click(function(){}); 

		  }	 	
	  }

  });  
    
});
</script>
<script type="text/javascript" src="{{asset('js/homevideo.js')}}?v=2"></script>
@endsection