@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Hot + - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title"><!--main title-->
			<h1>Paradise</h1>
		</header><!--/main title-->
		
		<div class="contenedor all-issues hotmas"><!--/contenedor-->
			
			<div class="home-more-row"><!--more-->

				@foreach($angeles as $angel)
				<div class="home-boxes "><!--home-boxes-->

					<div class="home-image"><!--home image-->

					<figure style="background-image:url(<?php if(Storage::disk('hotmas')->exists($angel->main_pic)){ echo Storage::disk('hotmas')->url($angel->main_pic); } ?>);"></figure>
					<div><!--div-->
						<div class="tabla"><!--tabla-->
							<div class="tabla-celda"><!--tabla celda-->
								<h4><a href="{{route('hot.angel', ['slug' => $angel->slug])}}">{{$angel->nombre}}</a></h4>
							</div><!--/tabla celda-->
						</div><!--/tabla-->
					</div><!--/div-->

					</div><!--/home image-->

				</div><!---homeboxes-->

				
					@if($loop->iteration%3 == 0)
					<div class="clearfix"></div>
					@endif

					@if($loop->iteration%6 == 0)
						@if(!empty($ad))
						<div class="clearfix"></div>						
						<div class="ads ad-revista-preview paradise-ad"><!--ads-->							
							<img src="{{Storage::disk('home')->url($ad->media_name)}}">										
						</div><!--/ads-->
						@endif	
					@endif

				@endforeach

				{{$angeles->links()}}
				

				<div class="clearfix"></div>

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection