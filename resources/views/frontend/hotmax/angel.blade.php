@extends('frontend.layouts.main')

@section('meta-description')
{{$angel->meta_description}}
@endsection

@section('meta-keywords')
{{$angel->meta_keywords}}
@endsection

@section('meta-title')
	@if(!empty($angel->meta_title)) {{$angel->meta_title}} @else {{$angel->nombre}} @endif - Revista Angeles - Te traemos el paraíso 
@endsection

@section('page-specific-scripts')
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es-419'}
</script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title blog-title"><!--main title-->
			<h1>{{$angel->nombre}}</h1>			
		</header><!--/main title-->
		
		<div class="contenedor blog-main-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-9 single-post"><!--all posts-->					
					
					<div class="angel-post"><!--angel post-->

					<article>
						{!! $angel->descripcion !!}
					</article>

					@foreach($angel->hots()->orderBy('position')->get() as $image)

						<div class="angel-pic">
							@if(Storage::disk('hotmas')->exists($image->name))
							<img src="{{Storage::disk('hotmas')->url($image->name)}}">
							@endif
						</div>

					@endforeach
					<ul class="single-post-share">
							<li>
								Compártelo en las redes:
							</li>
							<li class="fb-blog-single">
								<div class="fb-share-button" data-href="{{route('hot.angel', ['slug' => $angel->slug])}}" data-layout="button" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartir</a></div>
							</li>
							<li class="twitter-blog-single">
								<a class="twitter-share-button"
								  href="https://twitter.com/share"
								  data-size="large"
								  data-text="custom share text"
								  data-url="{{route('hot.angel', ['slug' => $angel->slug])}}"
								  data-hashtags="example,demo"
								  data-via="twitterdev"
								  data-related="twitterapi,twitter">
								<img src="{{asset('images/tweet.png')}}">
								</a>
							</li>
							<li class="youtube-blog-single">
								<a href="#"></a>
								<!-- Place this tag where you want the share button to render. -->
								<div class="g-plus" data-action="share" data-height="24" data-href="{{route('hot.angel', ['slug' => $angel->slug])}}"></div>
							</li>
						</ul>
					<div class="clearfix"></div>
						
					</div><!--/angel post-->
					
					<!-- OTROS -->

					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-12 otros-posts"><!--otros posts-->

						<header class="otros-posts-title">
							<h3>Otros Ángeles</h3>
						</header>

						<div class="row"><!--row-->

							@foreach($otros as $otro)
							<div class="col-xs-12 col-sm-4 all-posts-preview"><!--preview-->
								<div>
								<?php 
									if(Storage::disk('hotmas')->exists($otro->main_pic)) {
										$image = Storage::disk('hotmas')->url($otro->main_pic);
									}
								?>
									<figure style="background-image:url({{$image}})">
										<a href="{{route('hot.angel', ['slug' => $otro->slug])}}" title="{{$otro->nombre}}"></a>
									</figure>
									<section>
										<h2>
											<a href="{{route('hot.angel', ['slug' => $otro->slug])}}" title="{{$otro->nombre}}">
												{{$otro->nombre}}
											</a>
										</h2>
										
									</section>
								</div>
							</div><!--/preview-->
							@endforeach
							
							

							

						</div><!--/row-->
					</div><!--/otros posts-->

				</div><!--/all posts-->



				@include('frontend.layouts.includes.blog_sidebar')

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection
