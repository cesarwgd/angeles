@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Ayuda - Revista Angeles - Te traemos el paraíso 
@endsection


@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title nosotros-maintitle"><!--main title-->
			<h1>Somos Ángeles Revista</h1>
			<h4>Traemos el paraíso de ángeles para su deleite</h4>
					
		</header><!--/main title-->
		
		<div class="contenedor contacto-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-12 nosotros-text caja-full-texto align-left"><!--form-->
				<hr>
				<br><br>	

				<h3>¿Cómo compro una revista?</h3>
				<p>
					Para comprar una revista necesitas tener una cuenta en nuestro sitio web. Si ya tienes una cuenta en nuestro sitio web puedes acceder <a href="{{url('/login')}}">AQUÍ</a>. Si aún no tienes una cuenta con nosotros puedes registrarte <a href="url('/register')">AQUÍ</a>.
				</p>
				<img src="{{asset('images/login.jpg')}}">
				<p>
					Una vez que has creado tu cuenta, accede a esta y luego en nuestra sección de <a href="{{route('revistas')}}">REVISTAS</a> haz clic en la revista que deseas comprar. Luego haz clic en <strong>COMPRAR</strong>. La revista sera agregada a tu carrito de compras.
				</p>
				<img src="{{asset('images/elegir-revista.jpg')}}">
				<img src="{{asset('images/agregando-carrito.jpg')}}">
				<p>	
					Luego elige la opción con la que deseas pagar: <strong>TARJEA DE CRÉDITO</strong> o <strong>DEPÓSITO BANCARIO</strong>.
				</p>
				<img src="{{asset('images/shopping-cart.jpg')}}">

				<h4><strong>Pago con tarjeta de crédito</strong></h4>
				<img src="{{asset('images/pagar-tarjeta.jpg')}}">
				<p>Ingresa los datos solicitados para hacer el pago con tu tarjeta de crédito y luego haz clic en <strong>PAGAR</strong>. Espera un momento mientras el sistema procesa tu compra. Una vez efectuada la transacción recibirás en un lapso de 15 minutos un correo electrónico con la notificación de la compra realizada (revisa tu bandeja de spam ya que aveces puede llegar ahí).
				</p>
				<img src="{{asset('images/comprando-revista.jpg')}}">
				<p>Inmediatamente después de haber realizado la compra serás enviado a la sección de tus revistas donde figuran todas las revistas que has comprado.</p>
				<img src="{{asset('images/revista-comprada.jpg')}}">
				<p>Si relizas la compra por medio de depósito bancario en un lapso de 24 hrs hábiles estaremos confirmando tu compra y habilitandote la revista que compraste.</p>

				<h3>Compre una revista. ¿Cómo hago para verla?</h3>
				<p>
					Una vez que hayas comprado una revista, esta aparecerá en tu sección <strong>MIS REVISTAS</strong>. Haz clic en la imagen miniatura de lar evista o en el botón <strong>VER REVISTA</strong> y seras inmediatamente enviado a la página de la revista.
				</p>
				<img src="{{asset('images/mis-revistas.jpg')}}">

				<p>
					En la sección de la revista que compraste tendras un menú al lado izquierdo con los siguientes botones:<br>
					<strong>INICIO:</strong> Para ser enviado al homepage del sitio web.
					<br>
					<strong>MI PERFIL:</strong>	Para ser enviado a la pagina de tu perfil.<br>
					<strong>MIS REVISTAS:</strong> Te mostrara un sub menu con todas las revistas que has adquirido y podrás acceder a ellas haciendo clic en la imagen miniatura o en el nombre de la revista.				
				</p>
				<img src="{{asset('images/revistas-menu.jpg')}}">
				<p>
					<strong>Páginas:</strong> Te mostrará un sub menú con todas las paginas de la revista que estas mirando en ese momento.
				</p>
				<img src="{{asset('images/paginacion.jpg')}}">

				<p>
					<strong>Video:</strong> Este botón te permite acceder al video de la revista que has adquirido. Un popup con el códito del video aparecerá. Copialo e introducelo en la casilla correspondiente para poder visualizar el video.
				</p>
				<p>
					<strong>Zoom a la foto: </strong>Si estas mirando la revista desde tu Laptop o PC podras agrandar la imagen haciendo <strong>clic</strong> en esta y luego girando la rueda del mouse para agrandar o reducir la imagen. Así también, arrastrando el mouse y apretando al mismo tiempo sin soltar el clic izquierdo podrás mover la imagen de la revista agrandada.
				</p>
				<img src="{{asset('images/zoom-2.jpg')}}">
				<h3>Mi tarjeta de crédito fue rechaza</h3>
				<p>
					Asegurate de realizar tu compra con una <strong>tarjeta de crédito</strong> y <strong>no</strong> con una tarjeta de <strong>débito</strong>.<br>
					Cuando tu tarjeta es rechazada, un mensaje dentro de una casilla roja aparecerá indicándote el motivo del rechazo. Este motivo es proporcionado por la entidad procesadora que realiza la transacción en línea (nosotros <strong>no almacenamos la información</strong> de tu tarjeta). Si crees que dicho motivo no es correcto, te recomendamos contactarte con tu banco para que te den mayor información sobre el motivo del rechazo.
				</p>


				<h3>¿Puedo pagar con depósito/transferencia bancaria?</h3>
				<p>
					<strong>Sí</strong>. Para realizar una compra con depósito bancario o transferencia bancaria, elige la opción <strong>Pagar con depósito bancario</strong> e introduce los datos solicitados.
				</p>
				<img src="{{asset('images/deposito-bancario.jpg')}}">
				<p>Recibirás un correo electrónico con el número de tu orden de compra y la cuenta a la cual tienes que depositar, así como también las instrucciones a seguir para finalizar el proceso de compra. Revisa en tu bandeja de <strong>spam</strong> si es que no encuentras el correo en tu <strong>inbox</strong>. Si por algún motivo no recibes el correo; en la sección <strong>MIS ORDENES</strong> aparecerá el código de tu compra reciente. Contactanos a <strong>soporte@angelesrevista.com</strong> indicandonos el número de tu orden de compra.</p>
				<p>
				<p>
					Una vez que hayas seguido las instrucciones indicadas en el correo de confirmación de tu orden de compra, en un lapso de 24hrs hábiles estaremos validando tu compra y habilitándote la revista que adquiriste.
				</p>


				<h3>Tickets de soporte</h3>
				<p>
					Si tienes alguna consulta o deseas reportar algún problema con tu cuenta o recibir ayuda sobre algún tema en específico, puedes hacerlo mediante un ticket de soporte.
				</p>
				<p>Haz clic en el botón <strong>TICKETS DE SOPORTE</strong> y luego clic en <strong>CREAR TICKET DE AYUDA</strong></p>

				<p>Elige el <strong>tipo</strong> de ticket que vas a enviar y completa los datos solicitados y luego haz clic en <strong>ENVIAR</strong>. En un lapso de 24 hrs hábiles estaremos respondiendo tu ticket de soporte, el cual podrás responder por el mismo medio.</p>
				<p>Si luego de responder tu ticket de soporte no recibimos ninguna respuesta por parte tuya en un lapso de <strong>48hrs hábiles</strong>, daremos por cerrado el ticket de soporte y para contactarte con nosotros por ese medio tendras que aperturar un <strong>nuevo ticket</strong>.</p>

				<h3>¿Puedo eliminar mi cuenta?</h3>
				<p><strong>Sí</strong>. Pero <strong>ten en cuenta que si eliminas tu cuenta perderas acceso a todas las revistas que has comprado con dicha cuenta y ya no volverás a tener acceso a estas nunca más asi vuelvas a crear nuevamente tu cuenta con el mismo correo/cuenta de facebook asociado a la cuenta que eliminaste</strong>.</p>
				<p>
					Para eliminar tu cuenta, ingresa a tu perfil y luego haz clic en el botón <strong>ELIMINAR CUENTA</strong> ubicado en el lado derecho (inferior en móbiles). Una ventana de confirmación aparecera. Si reamente estas seguro que deseas eliminar tu cuenta entonces haz clic nuevamente en el botón <strong>ELIMINAR CUENTA</strong> de dicha ventana de confirmación.
				</p>
				<img src="{{asset('images/borrar-1.jpg')}}">
				<img src="{{asset('images/borrar-2.jpg')}}">

				</div><!--/form-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection