@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Blog - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title"><!--main title-->
			<h1>Blog</h1>

			<ul class="nav nav-pills" style="text-align: center; margin-top: 15px;">		  
			  @foreach($categories as $category)
			  <li role="presentation" style="display: inline-block; float: none;">
			  	<a href="{{route('blog.category', ['categorySlug' => $category->slug])}}" style="color:#000;">{{$category->nombre}}</a>
			  </li>	
			  @endforeach	  
			</ul>
		</header><!--/main title-->

		
		
		<div class="contenedor blog-main-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-9 all-posts"><!--all posts-->
					<div class="row"><!--row-->
						
						@foreach($posts as $post)
						<div class="col-xs-12 col-sm-4 all-posts-preview"><!--preview-->
							<div>
								<?php 
										$imagen = "";
										if(Storage::disk('blog')->exists($post->main_image)){
											$imagen = Storage::disk('blog')->url($post->main_image);
										}
									?>
									<figure style="background-image:url({{$imagen}});">
										<?php $categorySlug = $post->getCategorySlug(); ?>
										<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}"></a>
									</figure>
								<section>
									<h2>
										<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}">
											{{$post->titulo}}
										</a>
									</h2>
									<p>{{$post->extracto}}</p>
									<a href="{{route('blog.single', ['category' => $categorySlug, 'slug' => $post->slug])}}" title="{{$post->titulo}}" class="read-more">Continuar leyendo</a>
								</section>
							</div>
						</div><!--/preview-->
							@if($loop->iteration % 3 == 0)
							<div class="clearfix"></div>
							@endif
						@endforeach

					</div><!--/row-->

					{{$posts->links()}}

				</div><!--/all posts-->

				@include('frontend.layouts.includes.blog_sidebar')

				

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection