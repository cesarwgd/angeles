@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Nosotros- Revista Angeles - Te traemos el paraíso 
@endsection


@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title nosotros-maintitle"><!--main title-->
			<h1>Somos Ángeles Revista</h1>
			<h4>Traemos el paraíso de ángeles para su deleite</h4>			
					
		</header><!--/main title-->
		
		<div class="contenedor contacto-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-12 nosotros-text caja-full-texto"><!--form-->
				<hr>
				<br><br>	

				<p>El privilegio que nos ha concedido nuestro talento ha permitido llegar a tener de cerca a cada ángel que llega al paraíso. ¿Cuál es ese talento?</p>
				<p>
					Es el arte de plasmar a la perfección en fotografías y en lenguaje audiovisual la belleza de las ángeles.
				</p>
				<p>
					Esta travesía comenzó hace 5 años, tras pasar una odisea luchando con entidades adversas que quisieron alejarnos y quitarnos el talento sin tener éxito. Este privilegio nos fue concedido a través de la experiencia y la confianza de nuestro trabajo que se caracteriza por el minucioso cuidado de mostrar el arte que poseen las mujeres más bellas en sus cuerpos esculturales como divinidad convirtiéndose en ángeles al desnudo.
				</p>
				<p>
					Hoy este sacrificio para llegar al paraíso es una realidad y nuestro mandato es hacer que a través de nuestros lentes puedan tener de cerca y conocer la divinidad de nuestras ángeles.
				</p>
				<p>El paraíso está más cerca, puedes ver lo que ocurre dentro de él y puedes acceder a él en todos lados.</p>

				</div><!--/form-->

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection