@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Productos Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Productos - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title"><!--main title-->
			<h1>Productos</h1>
		</header><!--/main title-->
		
		<div class="contenedor all-issues"><!--/contenedor-->
			
			<div class="row"><!--row-->


				<div class="col-xs-12 col-sm-6 pull-right">

					@if(session()->has('error'))
						<div class="alert alert-danger">
							{{session()->get('error')}}
						</div>
					@endif

					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					
					<div class="pull-right">
						<form action="{{route('frontend.buscar.producto')}}" method="GET">
				  
						  <div class="form-group" style="display: inline-block;">						    
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por nombre" name="nombre" required>
						  </div>						  
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
					<br><br><br><br>
				</div>
				<div class="clearfix"></div>

				@if(count($products) > 0)
				@foreach($products as $product)
				<div class="col-xs-12 col-sm-3 col-lg-2 issues-box"><!--issues box-->
					<div class="issue-portrait">
						<a href="{{route('frontend.productos.detail', ['slug' => $product->slug])}}" title="{{$product->name}}">
							<?php 
								$main_image = $product->productImages()->where('position', 1)->first();
							?>
							@if($main_image != null)
							<img src="{{Storage::disk('products')->url($main_image->name)}}" class="angel-img">
							@else
							<img src="" class="angel-img">
							@endif
						</a>
					</div>
					<h2>
						<a href="{{route('frontend.productos.detail', ['slug' => $product->slug])}}" title="{{$product->name}}">
						{{$product->name}}
						</a>
					</h2>
					
					@if($product->stock == 0)
					<h4><strong>AGOTADO</strong></h4>
					@endif
				</div><!--/issues box-->
					

				@endforeach
				@endif
				

				<div class="clearfix"></div>

			</div><!--/row-->

			@if(count($products) > 0)
			{{$products->links()}}
			@endif

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection