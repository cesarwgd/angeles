@extends('frontend.layouts.main')

@section('meta-description')
	{{$product->meta_description}}
@endsection

@section('meta-keywords')
	{{$product->meta_keywords}}
@endsection

@section('meta-title')
	@if(!empty($product->meta_title)) {{$product->meta_title}} @else {{$product->name}} @endif - Revista Angeles - Te traemos el paraíso 
@endsection

@section('content')
<div class="porfavor-acceda">
	<div>		
		<h5>
			Por favor <a href="{{url('login')}}">INGRESE</a> a su cuenta o <a href="{{url('register')}}">REGÍSTRESE</a> para proceder con la compra de la revista.
		</h5>
		<a href="#" class="cerrar-porfavor">X</a>
	</div>
</div>	


	<div class="main"><!--main-->

		
		
		<div class="contenedor revista-detail-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-6 revista-detail-mainpic"><!--pics-->

					<figure class="show-main-detailpic">
					<?php 
						$img = 1;
						$files = $product->productImages()->orderBy('position', 'asc')->get();
					?>
						@if($files != null)
						@foreach($files as $file)
							
						<img src="{{Storage::disk('products')->url($file->name)}}" alt="{{$product->name}}" id="thumb<?php echo $img; ?>">
							<?php $img++; ?>	
						@endforeach	
						@endif
						
																	
					</figure>

					<div class="thumbs-container">
						<div class="show-thumbs-box">
						<?php $thumb = 1; ?>
						@if($files != null)
						@foreach($files as $file)
							<div class="thumbs-detail-preview">
								<a href="#thumb<?php echo $thumb; ?>" data-imgurl="{{Storage::disk('products')->url($file->name)}}" title="{{$product->name}}">
									<img src="{{Storage::disk('products')->url($file->name)}}" alt="{{$product->name}}">
								</a>
							</div>
							<?php $thumb++; ?>
						@endforeach
						@endif						
							
						</div>
					</div>
					
				</div><!--/pics-->
				<script type="text/javascript">
					$(".thumbs-detail-preview a").click(function(){
						$('.show-main-detailpic img').hide();
						var image = $(this).attr('href');						
						$('.show-main-detailpic').find(image).fadeIn();
						return false;
					});
				</script>

				<div class="col-xs-12 col-sm-6 revista-detail-texts"><!--texts-->

					<h1>{{$product->name}}</h1>
					@if(session()->has('exito'))
					<div class="alert alert-success">
						Producto agregado con éxito a su carrito de compras. Haga clic <a href="{{route('user.checkout')}}">ACÁ</a> para proceder a pagar su compra o <a href="{{route('revistas')}}">continue comprando más revistas</a>
					</div>
					@endif
					
					<h3 class="detail-price">S/. {{$product->price}}</h3>
					<h4>Precio aproximado en dólares: <strong>${{ round($product->price/3.28, 2) }}</strong></h4>
					<div class="clearfix"></div>
					<section>
						{!! $product->description !!}
					</section>
					@if($product->dimensions != null)
					<h3>Dimensiones:</h3>
					<p><strong>
						{{$product->dimensions}}
					</strong></p>
					@endif
					@if($product->size != null)
					<h3>Tallas disponibles:</h3>
					<p><strong>
						{{$product->size}}
					</strong></p>
					@endif
					@if($product->color != null)
					<h3>Colores disponibles:</h3>
					<p><strong>
						{{$product->color}}
					</strong></p>
					@endif
					<div class="clearfix"></div>
					
					@if($product->stock == 0)
					<div class="alert alert-danger">
						<strong>PRODUCTO AGOTADO</strong>
					</div>
					@else
					<form action="{{route('user.product.place.order')}}" method="POST" id="agregar-al-carrito">
						<input type="hidden" name="product_id" value="{{$product->id}}">
						
						<div class="row"><!--row-->
							<br>
							<div class="col-xs-12 col-sm-8 col-md-6">
								@if($product->size != null)
									<?php 
										$tallas = explode(",", $product->size);
									?>
									<label>Talla:</label>
									<select class="form-control" name="size" required>
										<option value="" disabled selected>----</option>
										@foreach($tallas as $talla)
										<option value="{{$talla}}">{{$talla}}</option>
										@endforeach
									</select><br>
									@endif

									@if($product->color != null)
									<?php 
										$colores = explode(",", $product->color);
									?>
									<label>Color:</label>
									<select class="form-control" name="color" required>
										<option value="" disabled selected>----</option>
										@foreach($colores as $color)
										<option value="{{$color}}">{{$color}}</option>
										@endforeach
									</select><br>
									@endif
									<label>Cantidad:</label>
									<select name="quantity" class="form-control">
										<option value="1" selected>1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select><br>
							</div>

						</div><!--/row-->

						{{csrf_field()}}
						@if(!Auth::user())
						<a href="#" class="detail-comprar" id="buy-not-logeddin">Comprar</a>
						<script type="text/javascript">
							$("#buy-not-logeddin").click(function(){
								$('.porfavor-acceda').fadeIn();
								return false;
							});
						</script>
						@else
							
							<button type="submit" class="detail-comprar" id="revista-place-order">
								Comprar
							</button>
							
						@endif
					</form>
					@endif

					<div class="clearfix"></div>
					
					
					<ul class="share-detail">
						<li class="fb-share-detail">
							<a href="#"></a>
						</li>
						<li class="twitter-share-detail">
							<a href="#"></a>
						</li>
						<li class="gplus-share-detail">
							<a href="#"></a>
						</li>
					</ul>
				
				</div><!--/texts-->
				
			</div><!--/row-->

			<div class="ads ad-revista-preview"><!--ads-->
				@if($ad != null)
					<img src="{{Storage::disk('home')->url($ad->media_name)}}">
				@endif				
			</div><!--/ads-->

			<div class="similar-products"><!--similar products-->

				<h2>También te recomendamos</h2>

				<div class="row"><!--row-->
				@if($otros != null)
				@foreach($otros as $otro)
				<?php 
					$otro_img = $otro->productImages()->where('position', 1)->first();
				?>
				<div class="col-xs-12 col-sm-4"><!--col-->
					@if($otro_img != null)
					<figure class="recomendamos-imgs">
							<img src="{{Storage::disk('products')->url($otro_img->name)}}" alt="{{$otro->name}}">
							<a href="{{route('frontend.productos.detail', ['slug' => $otro->slug])}}" title="{{$otro->name}}"></a>							
						</figure>
					@else
					<figure class="recomendamos-imgs">
							<img src="" alt="{{$otro->name}}">
							<a href="{{route('frontend.productos.detail', ['slug' => $otro->slug])}}" title="{{$otro->name}}"></a>							
						</figure>
					@endif
						
						<h4>
							<a href="{{route('frontend.productos.detail', ['slug' => $otro->slug])}}" title="{{$otro->name}}">{{$otro->name}}</a>
						</h4>
					</div><!--/col-->
				@endforeach
				@endif
					

					


				</div><!--/row-->

			</div><!--/similar products-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection
@section('footer_page_scripts')
<script type="text/javascript">
	$("#agregar-al-carrito").submit(function(){
		$('body').append("<div class='procesando-compra'><div class='tabla'><div class='tabla-celda'>..Agregando a su carrito de compras..</div></div></div>");
	});
</script>
@endsection