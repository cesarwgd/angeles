@extends('frontend.layouts.main')

@section('meta-description')
Angeles es una revista para hombres con las mejores modelos de Latinoamérica
@endsection

@section('meta-keywords')
Revista Angeles, modelos peruanas, modelos argentinas, modelos colombianas, Angeles Peru
@endsection

@section('meta-title')
Contacto - Revista Angeles - Te traemos el paraíso 
@endsection

@section('page-specific-scripts')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
	
	<div class="main"><!--main-->

		<header class="main-title"><!--main title-->
			<h1>Contáctanos</h1>
		</header><!--/main title-->
		
		<div class="contenedor contacto-box"><!--/contenedor-->
			
			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-6 formulario-contacto"><!--form-->
					@if(session()->has('captcha_error'))
					<div class="alert alert-danger">
						{{session()->get('captcha_error')}}
					</div>
					@endif
					@if(session()->has('exito'))
					<div class="alert alert-success">
						{{session()->get('exito')}}
					</div>
					@endif
					<form method="post" action="{{route('post.contacto')}}" id="contact-form">
						<input type="text" name="nombres" placeholder="* Nombres Y Apellidos">
						@if($errors->has('nombres'))
                            <p class="text-warning">Campo obligatorio y no mayor a 255 caracteres</p>
                            @endif    
						<input type="email" name="email" placeholder="* Correo Electrónico">
						@if($errors->has('email'))
                            <p class="text-warning">Campo obligatorio y no mayor a 255 caracteres</p>
                            @endif    
						<input type="text" name="phone" placeholder="* Teléfono">
						@if($errors->has('phone'))
                            <p class="text-warning">Campo obligatorio y no mayor a 255 caracteres</p>
                            @endif    
						<input type="text" name="subject" placeholder="* Asunto">
						@if($errors->has('subject'))
                            <p class="text-warning">Campo obligatorio y no mayor a 255 caracteres</p>
                            @endif    
						<textarea name="mensaje" rows="6" placeholder="* Mensaje"></textarea>
						@if($errors->has('mensaje'))
                            <p class="text-warning">Campo obligatorio y no mayor a 255 caracteres</p>
                            @endif    
						<span class="peque">* Campos obligatorios</span>
						<div class="recaptcha-box" id="recaptcha-box">
							<div class="g-recaptcha" data-sitekey="6Lejhz4UAAAAAGNoimQNhLW34kR3TC86NCbS-aYd"></div>						
						</div>
						<input type="submit" name="send" value="Enviar" id="envialo">
						{{csrf_field()}}
					</form>
				</div><!--/form-->
				<script type="text/javascript">
		            $(document).ready(function(){
		                $("#contact-form").submit(function(event){
		                    var verified = grecaptcha.getResponse();
		                    if(verified.length === 0){
		                        event.preventDefault();
		                    }else{
		                        $("#envialo").prop('disabled', true);
		                    }
		                });
		            });
		        </script>
				<div class="col-xs-12 col-sm-6 contacto-text">
				<h3>Comunicate con el paraiso</h3>
				<h6><strong>Dejanos tu mensaje para cualquier consulta</strong></h6>
				<hr>
					<p>Somos una revista digital que te muestra el paraíso femenino entre nosotros. Dirigimos nuestro talento para resaltar lo más preciado de nuestras mujeres en el mejor estilo natural.</p>
				</div>

			</div><!--/row-->

		</div><!--/contenedor-->

	</div><!--/main-->
@endsection