@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Blog</h1>

			<hr>

			<div class="row"><!--row-->
				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif
				<div class="col-xs-4 col-sm-3">
				<a href="{{route('backend.blog.new')}}">
					<button class="btn btn-primary">
						
							<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Nota
						
					</button>
					</a>
				</div>
				<div class="col-xs-8 col-sm-6 col-sm-offset-3">
					<div class="pull-right">
						<form class="form-inline" action="{{route('backend.blog.searchresult')}}" method="GET">
					  
						  <div class="form-group">
						    <label for="inputBuscar" class="sr-only">Password</label>
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por título" name="titulo">
						  </div>
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Título</th>
							<th>Estado</th>  
							<th>Creado el:</th> 
							<th>Actualizado el:</th> 
							<th>Editar</th> 
							<th>Borrar</th> 
						</tr> 
					</thead> 
					<tbody> 
						<?php 
							$items = $posts->firstItem();
						?>
						@foreach($posts as $post)
						<tr> 
							<th scope="row">{{$items}}</th> 
							<td>
								<a href="{{route('backend.blog.update', ['id' => $post->id])}}">
									{{$post->titulo}}
								</a>
							</td>
							<td><strong>{{$post->estado}}</strong></td> 
							<td>{{$post->created_at}}</td> 
							<td>{{$post->updated_at}}</td>
							<td>
								<button class="btn btn-primary">
									<a href="{{route('backend.blog.update', ['id' => $post->id])}}">Editar</a>
								</button>
							</td> 
							<td>
								<button class="btn btn-danger">
									<a href="{{route('backend.blog.delete', ['id' => $post->id])}}" class="delete-post-btn">Eliminar</a>
								</button>
							</td>  
						</tr> 
						<?php $items++; ?>
						@endforeach
						
					  </tbody> 
				  </table>
				{{$posts->links()}}

			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->
<script type="text/javascript">
	$(function(){
		$("a.delete-post-btn").click(function(e){
			
			if(!confirm("Esta seguro?")){
				e.preventDefault();
			}
		});
	});
</script>
@endsection