@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Categoría Modelos:</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			<br><br>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			    <br><br>
			@endif

			<div class="row"><!--row-->

				<div class="col-xs-12 col-sm-4"><!--col-->
					<form action="{{route('backend.postmodel.categorias')}}" method="post"><!--form-->
						
					<div class="form-group"><!--form group-->
						<label for="CategoriaNombre">Nombre de la categoría:</label>
						<input type="text" name="nombre" class="form-control" placeholder="nombre de la categoría" id="CategoriaNombre">
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaSlug">Slug (sin espacios, sólo guiones):</label>
						<input type="text" name="slug" class="form-control" placeholder="slug de la categoría" id="CategoriaSlug">
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaPadre">Padre:</label>
						<select name="parent" id="CategoriaPadre">

							<option disabled selected>--Ninguno--</option>
							@foreach($categorias as $categoria)
							<option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
							@endforeach							
							
						</select>
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaDescripción">Descripción:</label>
						<textarea name="descripcion" class="form-control" placeholder="descripción de la categoría" id="CategoriaDescripcion"></textarea>
						
					</div><!--/form group-->

					{{csrf_field()}}

					<button type="submit" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
					</button>

					</form><!--/form-->

				</div><!--/col-->

				<div class="col-xs-12 col-sm-8"><!--col-->

					<table class="table table-hover"> 
						<thead> 
							<tr> 
								<th>#</th> 
								<th>Categoría</th> 
								<th>Descripción</th>
								<th>Padre</th>
								<th></th>								
							</tr> 
						</thead> 
						<tbody> 

							@foreach($categorias as $categoria)
							<tr> 
								<th scope="row">{{$loop->iteration}}</th> 
								<td>{{$categoria->nombre}}</td> 
								<td>{{$categoria->nombre}}</td> 
								<td>
									@if($categoria->parent)
									{{$categoria->getParentName($categoria->parent)}}
									@endif
								</td>
								<td>
									<button class="btn btn-primary">
										<a href="{{route('backend.viewmodel.categorie', ['id' => $categoria->id])}}">Editar</a>
									</button>
								</td> 
							</tr>
							@endforeach 
							
						  </tbody> 
					  </table>
					
				</div><!--/col-->


			</div>	

		</div><!--/container-->

	</div><!--/main-->

@endsection