@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Ticket ID # {{$ticket->id}}:</h1>

			<hr>

			<div class="row"><!--row-->
				
				
				<div class="col-xs-8 col-sm-6">
					
						<form class="form-inline" action="{{route('backend.update.ticket', ['id' => $ticket->id])}}" method="POST">
					  
						  <div class="form-group">
						    <label>Estado: </label>
						    <select name="estado" required class="form-control">
						    	<option value="abierto"@if($ticket->estado == "abierto") selected @endif>Abierto</option>
						    	<option value="cerrado"@if($ticket->estado == "cerrado") selected @endif>Cerrado</option>
						    </select>
						  </div>
						  {{csrf_field()}}
						  <button type="submit" class="btn btn-primary">
						   Actualizar
						   </button>
						</form>
					
				</div>
			</div><!--/row-->

			@if(session()->has('exito'))
			<br><br>
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			<br><br>
			<div class="m-bottom-60"><!--v top 60-->

				<p><strong>Usuario:</strong> 
					@if(!empty($ticket->user))
					{{$ticket->user->name}} {{$ticket->user->last_name}}
					@endif
				</p>
				<p><strong>Asunto:</strong> {{$ticket->asunto}} </p>
				<p><strong>Tipo:</strong> {{$ticket->tipo}} </p>
				
				<div class="row"><!--row-->
					
					<div class="col-xs-12 col-sm-8">

						<!--TICKET ORIGINAL PADRE-->
						<div class="panel panel-default">
							<div class="panel-heading">
								Asunto: {{$ticket->asunto}} | Creado el: {{$ticket->created_at}}
							</div>
							 <div class="panel-body">
							    {{$ticket->mensaje}}
							 </div>						  
						</div>
						
						@foreach($mensajes as $mensaje)
						<div class="panel panel-<?php if($mensaje->user_id != $ticket->user_id){ echo "primary"; }else{ echo "default"; }?>">
							<div class="panel-heading">
								Creado el: {{$mensaje->created_at}}
							</div>
							 <div class="panel-body">
							    {{$mensaje->mensaje}}
							 </div>						  
						</div>
						@endforeach

					</div>
					<div class="clearfix"></div>
					<!-- RESPONDER TICKET -->
					@if(!empty($ticket->user))
					<div class="col-xs-12 col-sm-8">
						<form action="{{route('backend.reply.ticket', ['id' => $ticket->id])}}" method="POST">
							<div class="form-group">
								<textarea name="detalle" class="form-control" rows="8" required></textarea>
								<input type="hidden" name="from_user" value="{{Auth::user()->id}}">
								<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
								<input type="hidden" name="replied_to" value="{{$ticket->user->id}}">
								<input type="hidden" name="email" value="{{$ticket->user->email}}">
								<input type="hidden" name="ticket_id" value="{{$ticket->id}}">
								<input type="hidden" name="asunto" value="Respuesta a ticket de soporte #{{$ticket->id}}">
							</div>
							{{csrf_field()}}
							<button class="btn btn-primary">
								Responder
							</button>
						</form>
						
					</div>
					@else
					<div class="col-xs-12 col-sm-8">
						Usuario eliminado
					</div>
					@endif
					<!-- RESPONDER TICKET -->
					

				</div><!--/row-->
				
				  
			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->

@endsection