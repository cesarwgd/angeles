@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Ordenes de venta</h1>

			<hr>

			@if(session()->has('no_existe'))
				<div class="col-xs-12">
					<div class="alert alert-warning">
						{{session()->get('no_existe')}}
					</div>
				</div>
				@endif

			<div class="row"><!--row-->
				
				<div class="col-xs-6 col-sm-4">
					
					<form class="form-inline" action="{{route('backend.ventas.searchresult')}}" method="GET">
				  
					  <div class="form-group">
					    <label for="inputBuscar" class="sr-only"># Orden</label>
					    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por codigo" name="code">
					  </div>
					  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
					</form>					
					
				</div>

				<div class="col-xs-12 col-sm-12" style="margin-top: 25px;">
					<form class="form-inline" action="{{route('backend.ventas.filtersearch')}}" method="GET" id="filter-form">
						<div class="form-group" style="margin-bottom: 20px;">							
							<a class="btn btn-primary show-filtro-revistas">FILTRAR POR REVISTA</a>
						</div>

						<div class="form-group">
							<label>Tipo:</label>
							<select class="form-control" name="type" id="select-tipo">
								<option disabled selected>----</option>
								<option value="revista">Revista</option>
								<option value="video">Video</option>								
							</select>
						</div>

						<div class="form-group">
							<label>Estado:</label>
							<select class="form-control" name="estado" id="select-estado">
								<option selected disabled>-----</option>
								<option value="iniciada">Incompleta</option>
								<option value="pendiente">Pendiente</option>
								<option value="completada">Completada</option>
								<option value="cancelada">Cancelada</option>
							</select>
						</div>

						<div class="form-group">
							<label>Mes:</label>
							<select class="form-control" name="mes" id="select-mes">
								<option selected disabled>----</option>								
								<option value="01">Enero</option>
								<option value="02">Febrero</option>
								<option value="03">Marzo</option>
								<option value="04">Abril</option>
								<option value="05">Mayo</option>
								<option value="06">Junio</option>
								<option value="07">Julio</option>
								<option value="08">Agosto</option>
								<option value="09">Septiembre</option>
								<option value="10">Octubre</option>
								<option value="11">Noviembre</option>
								<option value="12">Diciembre</option>								
							</select>
						</div>

						<div class="form-group">
							<label>Año:</label>
							<select class="form-control" name="anio" id="select-anio">
								<option selected disabled>----</option>								
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
								<option value="2026">2026</option>
								
							</select>
						</div>

						<!--FILTRO POR PAIS-->
						<div class="form-group">
							<label>País:</label>
							<select class="form-control" name="pais" id="select-pais">
								<option selected disabled>----</option>								
								@foreach($countries as $country)
								<option value="{{$country}}">{{$country}}</option>
								@endforeach
								
								
								
							</select>
						</div>

						<button class="btn btn-success">
							BUSQUEDA FILTRADA
						</button>

						<!--revistas-->
						<div class="clearfix"></div>
						<div class="form-group filtrar-por-revistas" id="select-revista" style="display: none; padding-top:20px;">
							
							@foreach($revistas as $revista)
							<div class="checkbox">
							    <label>
							      <input type="checkbox" name="filter-revista[]" value="{{$revista->id}}"> {{$revista->name}}
							    </label>
							  </div>								
							@endforeach
						</div>

					</form>
					<script>
						$("a.show-filtro-revistas").click(function(){
							$("#select-revista").toggle();
							return false;
						});

						$("#filter-form").submit(function(){
							var revista = $("#select-revista input[type=checkbox]:checked").length,
								tipo = $("#select-tipo").val(),
								estado = $("#select-estado").val(),
								mes = $("#select-mes").val(),
								anio = $("#select-anio").val()
								pais = $("#select-pais").val();

							if(revista == 0 && !tipo && !estado && !mes && !anio && !pais){
								alert("Debe de elegir por lo menos 1 filtro de busqueda");
								return false;
							}	

							/*if(!revista){
								alert("Debe de elegir por lo menos el filtro REVISTA");
								return false;
							}*/

							if(mes && !anio){
								alert("Eliga el año");	
								return false;
							}


						});
					</script>
				</div>
				<br><br>
				<!--ventas-->
				<div class="col-xs-12 col-sm-12" style="margin-top: 25px;">
					<h4>TOTAL BRUTO: <strong><u>S/. {{$total}}</u></strong> | 
					TOTAL NETO: <strong><u>S/. {{round($total - ($total * 0.0473), 2)}}</u></strong> | 
					TOTAL REVISTAS VENDIDAS: <u>{{$revistas_vendidas}}</u></h4>
				</div>

			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Codigo</th> 
							<th>Tipo</th> 
							<th>Status</th>
							<th>Usuario</th> 
							<th>Revista</th> 
							<th>Cantidad S/.</th>
							<th>País</th>
							<th>Completada el:</th>  
						</tr> 
					</thead> 
					<tbody> 
					<?php 
						$items = $orders->firstItem();
					?>
						@foreach($orders as $order)
						<tr> 
							<th scope="row">{{$items}}</th> 
							<td>
								<a href="{{route('backend.order.view', ['id' => $order->id])}}">{{$order->code}}</a>
							</td> 
							<td>{{$order->type}}</td> 
							<td>
								@if($order->status == "iniciada")
								<span class="label label-default">Incompleta</span>
								@elseif($order->status == "pendiente")
								<span class="label label-info">En Proceso</span>
								@elseif($order->status == "cancelada")
								<span class="label label-danger">Cancelada</span>
								@else
								<span class="label label-success">Completada</span>
								@endif
							</td>							
							
							<td>
								@if(!empty($order->user))
								{{$order->user->name}} {{$order->user->last_name}}
								@else
								<strong>Usuario Eliminado</strong>
								@endif
							</td> 
							
							<td>{{$order->description}}</td>
							<td>S/ {{$order->amount}}</td> 
							<td>{{$order->country}}</td>
							<td>{{$order->updated_at}}</td>  
						</tr> 
						<?php $items++; ?>
						@endforeach
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->
			

			{{$orders->appends($search_queries)->links()}}

		</div><!--/container-->

	</div><!--/main-->

@endsection