@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Usuarios:</h1>

			<hr>
			

			<div class="row"><!--row-->
				<div class="col-xs-12 col-sm-12">

				<button class="btn btn-success" style="display:inline;">
						<a href="{{route('backend.csv.users')}}">Exportar todos a CSV</a>
					</button>

					<form class="form-inline" action="{{route('backend.user.search')}}" method="GET" style="display:inline-block;"><!--form-->
					  
					  <div class="form-group">
					    <label for="inputBuscar" class="sr-only">Password</label>
					    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por correo" name="email">
					  </div>
					  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
					</form><!--/form-->
				</div>
				
				@if(session()->has('exito'))
				<div class="col-xs-12">
					<div class="alert alert-success">
						{{session()->get('exito')}}
					</div>
				</div>
				@endif

				@if(session()->has('no_existe'))
				<div class="col-xs-12">
					<div class="alert alert-warning">
						{{session()->get('no_existe')}}
					</div>
				</div>
				@endif

			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Nombres</th> 
							<th>Apellidos</th> 
							<th>Correo</th>
							<th>Tipo</th>  
						</tr> 
					</thead> 
					<tbody> 
					<?php 
						$items = $users->firstItem();
					?>
						@foreach($users as $user)
						<tr> 
							<th scope="row">{{$items}}</th> 
							<td><a href="{{route('backend.user.detail', ['id' => $user->id])}}">{{$user->name}}</a></td> 
							<td><a href="{{route('backend.user.detail', ['id' => $user->id])}}">{{$user->last_name}}</a></td> 
							<td>{{$user->email}}</td> 
							<td>
								@foreach($user->roles as $role)
								{{$role->nombre}}
								@endforeach
							</td>
						</tr> 
						<?php $items++; ?>
						@endforeach
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

			{{ $users->links() }}

		</div><!--/container-->

	</div><!--/main-->

@endsection