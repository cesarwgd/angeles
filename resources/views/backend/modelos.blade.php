@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Modelos</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			<div class="row"><!--row-->
				<div class="col-xs-4 col-sm-3">
				<a href="{{route('backend.modelos.create')}}">
					<button class="btn btn-primary">						
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Modelo						
					</button>
					</a>
				</div>
				<div class="col-xs-8 col-sm-6 col-sm-offset-3">
					<div class="pull-right">
						<form class="form-inline" action="{{route('backend.modelos.searchresult')}}" method="GET">
					  
						  <div class="form-group">						    
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por nombre" name="nombre">
						  </div>						  
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Nombre</th> 
							<th>Creado el:</th> 
							<th>Publicado el:</th> 
							<th>Imagenes</th> 
							<th>Editar</th> 
							<th>Borrar</th> 
						</tr> 
					</thead> 
					<tbody> 
					<?php 
						$items = $angels->firstItem();
					?>
						@foreach($angels as $angel)
						<tr> 
							<th scope="row">{{$items}}</th> 
							<td><a href="{{route('backend.modelo.view', ['id' => $angel->id])}}">{{$angel->nombre}}</a></td> 
							<td>{{$angel->created_at}}</td> 
							<td>{{$angel->updated_at}}</td>
							<td>
								<button class="btn btn-info">
									<a href="{{route('backend.modelo.images', ['id' => $angel->id])}}">Imagenes</a>
								</button>
							</td>
							<td>
								<button class="btn btn-primary">
									<a href="{{route('backend.modelo.view', ['id' => $angel->id])}}">Editar</a>
								</button>
							</td>
							<td>
								<button class="btn btn-danger">
									<a href="{{route('backend.modelo.delete', ['id' => $angel->id])}}" class="delete-post-btn">Borrar</a>
								</button>
							</td> 
						</tr> 
						<?php $items++; ?>
						@endforeach
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

			{{$angels->links()}}

		</div><!--/container-->

	</div><!--/main-->
<script type="text/javascript">
	$(function(){
		$("a.delete-post-btn").click(function(e){
			
			if(!confirm("Esta seguro?")){
				e.preventDefault();
			}
		});
	});
</script>
@endsection