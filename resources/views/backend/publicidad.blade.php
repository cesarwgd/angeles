@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-header-scripts')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Anuncios Publicitarios</h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<!--FORM TO CREATE NEW BANNERS-->
				<form action="{{route('backend.post.anuncios')}}" method="POST" enctype="multipart/form-data">

					<div class="col-xs-12 col-sm-9">
						<div class="agregar-nuevas"><!--agregar nuevas-->		    			
						</div><!--/agregar nuevas-->
						<button class="btn btn-primary agregar-banner-btn">
							Agregar Fila +
						</button>
					</div>
					

					

					<div class="col-xs-12 col-sm-3">
						{{csrf_field()}}
						<button type="submit" class="btn btn-primary pull-right">
							CREAR
						</button>
					</div>

				</form>
				<!--/FORM TO CREATE NEW BANNERS-->

				<div class="clearfix"></div>

				<br><br>
				<h2>
					Anuncios Actuales:
				</h2>
				<hr>

				<!--FORM TO UPDATE THE EXISTING ONES-->
				<form action="{{route('backend.update.anuncios')}}" method="post" enctype="multipart/form-data">

					<div class="col-xs-12 col-sm-9 banners-existentes"><!--col-->

						<ul id="sortable">
				    		<!--sortable-->

				    		@foreach($ads as $ad)
				    		<li class="col-xs-12 col-md-12 row-home-files" id="filebox-{{$ad->id}}">				    			

				    			<div class='row'><!--row-->
				    				<div class='col-xs-6 col-sm-4 homebanners-update-rows'>
				    					<div class='form-group'>
				    						<label>Tipo:</label>
				    						<select name='type[]' class="form-control update-hbanners-select" required>
				    							<option selected disabled value=''>Eliga el tipo de archivo</option>
				    							<option value='image'<?php if($ad->type == "image"){ echo " selected"; } ?>>Imagen</option>
				    							<option value='video'<?php if($ad->type == "video"){ echo " selected"; } ?>>Video</option>
				    						</select>
				    						<div class='media-type-box'>
				    							<div class='media-image'<?php if($ad->type == "image"){ echo " style='display:block';"; } ?>>
				    								<label>Agrege la imagen, JPG/PNG/JPEG no mayor a 2MB:</label>
				    								<input type='file' name='image_name[]' class="form-control">
				    								@if(Storage::disk('home')->exists($ad->media_name))
				    								<a href="javascript:void(0)" class="thumbnail" data-class="">
												      <img src="{{Storage::disk('home')->url($ad->media_name)}}">
												    </a>
				    								@endif
				    							</div>

				    							<div class='media-video'<?php if($ad->type == "video"){ echo " style='display:block';"; } ?>>
				    								<label>Agrege el embed del video:</label>
				    								<input type='text' name='video_name[]' class="form-control" value="{{$ad->media_name}}">
				    								<div class="backend-home-video">
				    									{!!$ad->media_name!!}
				    								</div>
				    							</div>
				    						</div>
				    					</div>
				    					
				    				</div>

				    				<div class="col-xs-6 col-sm-2">
				    					<input type='hidden' name='position[]' value='{{$ad->position}}' class='posicion'>
				    					<input type='hidden' name='id[]' value='{{$ad->id}}' class='los-ids'>
				    					<div class='form-group'>
				    						<label>Ubicación:</label>
				    						<select name="location[]" required class="form-control">
				    							<option value="homepage"<?php if($ad->location == "homepage"){ echo " selected"; } ?>>Homepage</option>
				    							<option value="revistas"<?php if($ad->location == "revistas"){ echo " selected"; } ?>>Revistas (Publicas)</option>
				    							<option value="hot"<?php if($ad->location == "hot"){ echo " selected"; } ?>>Hot +</option>
				    							<option value="blog"<?php if($ad->location == "blog"){ echo " selected"; } ?>>Blog</option>
				    						</select>
				    					</div>

				    				</div>

				    				<div class="col-xs-6 col-sm-2">				    					
				    					<div class='form-group'>
				    						<label>Activo?:</label>
				    						<select name="active[]" required class="form-control">
				    							<option value="1"<?php if($ad->active == 1){ echo " selected"; } ?>>Activo</option>
				    							<option value="2"<?php if($ad->active == 2){ echo " selected"; } ?>>Inactivo</option>				    							
				    						</select>
				    					</div>

				    				</div>

				    				<div class="col-xs-6 col-sm-3">
				    					<div class='form-group'>
				    						<label>URL a donde redirecciona:</label>
				    						<input type='text' name='url[]' class='form-control' value="{{$ad->redirect_to}}">
				    					</div>
				    				</div>
				    				<div class="col-xs-6 col-sm-1">
				    					<br>
		    							<button class="btn btn-warning delete-row-banner" data-id="{{$ad->id}}">X</button>
				    				</div>

				    			</div><!--/row-->								   
							    
				    		</li>
				    		@endforeach

				    		<!--sortable-->
				    	</ul>

					</div><!--/col-->

					<div class="col-xs-12 col-sm-3"><!--col-->
						{{csrf_field()}}
						<button type="submit" class="btn btn-primary pull-right"<?php if(count($ads) == 0){ echo " disabled"; } ?>>
							ACTUALIZAR
						</button>
						<br><br>
						<hr>
						<br>
						<div class="append-here-message"></div>
						<button id="update-banner-position" class="btn btn-info pull-right"<?php if(count($ads) == 0){ echo " disabled"; } ?>>
							ACTUALIZAR POSICIONES
						</button>
					</div><!--/col-->


				</form>
				<!--/FORM TO UPDATE THE EXISTING ONES-->

					


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->
	

@endsection

@section('page-footer-scripts')


<script type="text/javascript">

	var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.host;
	var rowPosition = {{count($ads)}}; // valor obtenido del controlador dependiendo de cuantos filas hay en la tabla

	// ADD ROWS

	$('.agregar-banner-btn').click(function(){
		
		rowPosition++;

		var fila = "<div class='row each-fila'>"
					+ "<div class='col-xs-6 col-sm-4 select-type'>"
					+ "<div class='form-group'>"
		    		+ "<label>Tipo:</label>" 
		    		+ "<select name='type[]' class='form-control choose-media' required>"		
		    		+ "<option selected disabled value=''>Eliga el tipo de archivo</option>"			
		    		+ "<option value='image'>Imagen</option>"				
		    		+ "<option value='video'>Video</option>"				
		    		+ "</select>"					
		    		+ "<div class='media-type-box'>"					
		    		+ "<div class='media-image'>"					
		    		+ "<label>Agrege la imagen, JPG/PNG/JPEG no mayor a 2MB:</label>"				
		    		+ "<input type='file' name='image_name[]' class='form-control'>"				
		    		+ "</div>"					
		    		+ "<div class='media-video'>"						
		    		+ "<label>Agrege el embed del video:</label>"						
		    		+ "<input type='text' name='video_name[]' class='form-control'>"					
		    		+ "</div>"					
		    		+ "</div>"						
		    		+ "</div>"						
		    		+ "</div>"					
		    		+ "<div class='col-xs-6 col-sm-2'>"				
		    		+ "<div class='form-group'>"		    				
		    		+ "<label>Ubicacion:</label>"			
		    		+ "<select class='form-control' required name='location[]'>"
		    		+ "<option value='' disabled selected>-----</option>"
		    		+ "<option value='homepage'>Homepage</option>"
		    		+ "<option value='revistas'>Revistas (Publicas)</option>"
		    		+ "<option value='hot'>Hot +</option>"
		    		+ "<option value='blog'>Blog</option>"
		    		+ "</select>"		
		    		+ "</div>"		
		    		+ "</div>"
		    		+ "<div class='col-xs-6 col-sm-2'>"				
		    		+ "<div class='form-group'>"		    				
		    		+ "<label>Activo?:</label>"			
		    		+ "<select class='form-control' required name='active[]'>"		    		
		    		+ "<option value='1' selected>Activo</option>"
		    		+ "<option value='2'>Inactivo</option>"		    		
		    		+ "</select>"		
		    		+ "</div>"		
		    		+ "</div>"						
		    		+ "<div class='col-xs-6 col-sm-3'>"			
		    		+ "<div class='form-group'>"				
		    		+ "<label>URL a donde redirecciona:</label>"				
		    		+ "<input type='text' name='url[]' class='form-control'>"			
		    		+ "</div>"
		    		+ "</div>"		
		    		+ "<div class='col-xs-6 col-sm-1'>"		
		    		+ "<br><button class='btn btn-warning remove-row'>X</button>"			
		    		+ "</div>"				
		    		+ "</div>";

		$('.agregar-nuevas').append(fila);
		return false;
	})

	// REMOVE ROW
	$(document).on('click', '.remove-row', function(){
		$(this).parents('.each-fila').remove();
		/*if(rowPosition != 0){
			rowPosition--;
		}*/
	});


	// SELECT MEDIA TYPE ON CREATE NEW
	$(document).on('change', '.choose-media', function(){
		var type = $(this).val();
		if(type == "image"){
			$(this).parents('div.select-type').find('.media-image').show();
			$(this).parents('div.select-type').find('.media-video').hide();			
		}else{
			$(this).parents('div.select-type').find('.media-video').show();
			$(this).parents('div.select-type').find('.media-image').hide();
		}
	});

	//SELECT MEDIA TYPE FOR EXISTING ONES
	$(".update-hbanners-select").change(function(){
		var tipo = $(this).val();
		if(tipo == "image"){
			$(this).parents('.homebanners-update-rows').find('.media-image').show();
			$(this).parents('.homebanners-update-rows').find('.media-video').hide();			
		}else{
			$(this).parents('.homebanners-update-rows').find('.media-video').show();
			$(this).parents('.homebanners-update-rows').find('.media-image').hide();
		}
	});
	
	// DELETE BANNERS

	$(".delete-row-banner").click(function(e){
		e.preventDefault();
		var idErase = $(this).data('id'),
			parentId = $(this).parents('.row-home-files').attr('id');
		

		$("body").append("<div class='loading'></div>");
		$.ajax({
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type: 'post',
			dataType: 'json',
			url: protocol + "/administrador/anuncios/delete",
			data: {'id': idErase},
			success: function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-success'>Banner borrado exitosamente</div>");
				$("#"+parentId).remove();
				updatePositionOnDelete(response);
				
			}		
		}).fail(function(response){
			$(".loading").remove();
            $(".append-here-message").html("<div class='alert alert-danger'>Hubo un error. Intente de nuevo</div>");
            
        });
	});

	// SAVE POSITION CHANGES AND FILE CLASS

	$("#update-banner-position").click(function(e){
		e.preventDefault();
		// append el loading div
		$("body").append("<div class='loading'></div>");
		// seteamos los arrays que van a albergar la data
		var revista = $(this).data('revista');
		var positions = [];
		var ids = [];		

		// recolectamos la data y la ponemos en sus respectivos arrays
		$(".row-home-files").each(function(i){
			var currentPos = $(this).find('.posicion').attr('value');
			var currentId = $(this).find('.los-ids').attr('value');

			positions.push(currentPos);
			ids.push(currentId);
		});
		

		if(ids.length > 0){
			// ejecutamos el ajax call
			$.ajax({
				headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
				type: 'post',
				dataType: 'json',
				url: protocol + "/administrador/anuncios/update/positions",
				data: {positions: positions, ids: ids},
				success: function(response){
					$(".loading").remove();
					$(".append-here-message").html("<div class='alert alert-success'>"+response+"</div>");	
					
				}		

			}).fail(function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-danger'>Error al guardar los cambios</div>");
				
			});
		}else{
			alert('No hay banners');
		}
		
	});

	
	// update positions on ajax delete
	function updatePositionOnDelete(response)
	{
		for (i = 0; i < response.length; i++) {
			$("#filebox-"+response[i]['id']+" .posicion").attr('value', response[i]['position']);
		}
	}

</script>
<script type="text/javascript">
	$(function(){
		$( "#sortable" ).sortable({
			update: function(event, ui) {
				
		        $('.row-home-files').each(function(i) { 
		           var pos = $('.row-home-files').index(this); // updates the data object
		           $(this).find('.posicion').attr('value', pos + 1); // updates the attribute
		        });
		    }
		});
	});
</script>
@endsection