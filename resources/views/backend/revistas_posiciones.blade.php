@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-header-scripts')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>
				Revistas -  Posiciones de última a primera 				
			</h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-info">
					{{session()->get('exito')}}
				</div>
				@endif

				<div class="append-here-message">				
				</div>
				

			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Revista</th> 						
							
						</tr> 
					</thead>

				 </table>	
					
				<form method="post" action="{{route('backend.revistas.saveposition')}}">
					<ul id="sortable">
					@foreach($revistas as $revista)
					
						<li class="revista-row">
							<table class="table table-hover"> 
				    			<tr>
				    				<td><span data-pos="{{$revista->position}}" class="revista-posicion">{{$revista->position}}</span></td>
					    			<td><a href="{{route('backend.revista.view', ['id' => $revista->id])}}">
								      {{$revista->name}}
								    </a>
					    			<input type="hidden" name="position[]" value="{{$revista->position}}" class="field-position">
					    			<input type="hidden" name="revista_id[]" value="{{$revista->id}}">							   
								    
								    </td>
				    			</tr>
						    </table>	
			    		</li>
						
					    		
			    		
					
					
					@endforeach
					</ul>
				    {{csrf_field()}}
					<button type="submit" class="btn btn-primary">
						Guardar
					</button>
				</form>
						
				  

			</div><!--v top 60-->

			

		</div><!--/container-->

	</div><!--/main-->

@endsection
@section('page-footer-scripts')
<script type="text/javascript">
	$(function(){
		var datos = [];

		$( "#sortable" ).sortable({
			update: function(event, ui) {
				
		        $('.revista-row').each(function(i) { 
		        	var elemento = $('.revista-row');
		           var pos = elemento.length - elemento.index(this); // updates the data object
		           $(this).find('.revista-posicion').attr('data-pos', pos).text(pos); // updates the attribute
		           $(this).find('.field-position').val(pos); // updates the attribute
		        });
		    }
		});
	});
</script>
@endsection