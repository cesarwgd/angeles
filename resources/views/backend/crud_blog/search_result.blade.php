@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Blog</h1>

			<hr>

			<div class="row"><!--row-->
				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif
				
				<div class="col-xs-8 col-sm-6">
					
						<form class="form-inline" action="{{route('backend.blog.searchresult')}}" method="GET">
					  
						  <div class="form-group">
						    <label for="inputBuscar" class="sr-only">Password</label>
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por título" name="titulo">
						  </div>
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Título</th> 
							<th>Creado el:</th> 
							<th>Actualizado el:</th> 
							<th>Editar</th> 
							<th>Borrar</th> 
						</tr> 
					</thead> 
					<tbody> 

						@if($posts == null)
						<tr> 
							
							<td>
								{{$error}}
							</td>  
						</tr> 
						@else
							@foreach($posts as $post)
							<tr> 
								<th scope="row">1</th> 
								<td>
									<a href="{{route('backend.blog.update', ['id' => $post->id])}}">
										{{$post->titulo}}
									</a>
								</td> 
								<td>{{$post->created_at}}</td> 
								<td>{{$post->updated_at}}</td>
								<td>
									<button class="btn btn-primary">
										<a href="{{route('backend.blog.update', ['id' => $post->id])}}">Editar</a>
									</button>
								</td> 
								<td>
									<button class="btn btn-danger">
										<a href="{{route('backend.blog.delete', ['id' => $post->id])}}" class="delete-post-btn">Eliminar</a>
									</button>
								</td>  
							</tr> 
							@endforeach
						@endif
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->
<script type="text/javascript">
	$(function(){
		$("a.delete-post-btn").click(function(e){
			
			if(!confirm("Esta seguro?")){
				e.preventDefault();
			}
		});
	});
</script>
@endsection