@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Nombre de usuario</h1>

			<hr>

			<div class="row"><!--row-->
				
				<form method="post" action="#" enctype="multipart/form-data"><!--form-->
					<div class="col-xs-12 col-sm-9"><!--col-->

						<div class="form-group">
							<label for="NombreAdmin">Nombre:</label>
							<input type="text" name="nombre" placeholder="Nombre" class="form-control" id="NombreAdmin">
						</div>

						<div class="form-group">
							<label for="CorreoAdmin">Correo:</label>
							<input type="email" name="mail" placeholder="@Correo" class="form-control" id="CorreoAdmin">
						</div>

						<div class="form-group">
							<label for="PasswordAdmin">Password:</label>
							<input type="password" name="password" placeholder="Contraseña" class="form-control" id="PasswordAdmin">
						</div>
						


					</div><!--/col-->

					<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->

						<div class="form-group">
							<label for="AdminRol">Rol:</label>
							<select name="rol" id="AdminRol" class="form-control">
								<option selected value="publicado">Editor</option>
								<option selected value="borrador">Modelo</option>
								<option selected value="borrador">Admin</option>
							</select>
						</div>						

						<div class="form-group">
							<label>Imagen destacada:</label>
							<div class="imagen-destacada">
								<figure>
									<a href="#" class="thumbnail">
							      <img src="..." alt="...">
							    </a>
								</figure>
							    <input type="file" id="exampleInputFile" name="imagen_destacada">
							</div>

						</div>

						<button class="btn btn-primary" type="submit">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
						</button>

					</div><!--/col-->
					{{csrf_field()}}
				</form><!--/form-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

	

@endsection