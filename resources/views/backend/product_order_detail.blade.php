@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Orden #: {{$order->code}}</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>Codigo</th> 							
							<th>Status</th>
							<th>Teléfono:</th>
							<th>Cantidad</th> 
							<th>Total S/.</th>
							<th>Producto</th>
							<th>Tallas:</th>
							<th>Color:</th>
							<th>Usuario</th>
							<th>Dirección</th>
							<th>Creada el:</th> 
							<th>Actualizada el:</th>  
						</tr> 
					</thead> 
					<tbody> 
						<form action="{{route('backend.product.order.update', ['id' => $order->id])}}" method="post">
						<tr> 
							<td>{{$order->code}}</td> 							
							<td>
								<select name="status">
									<option value="iniciada"@if($order->status == "iniciada") selected @endif>Incompleta</option>
									<option value="completada"@if($order->status == "completada") selected @endif>Completada</option>
									<option value="cancelada"@if($order->status == "cancelada") selected @endif>Cancelada</option>
								</select>
							</td>
							<td>{{$order->contact_phone_number}}</td>
							<td>{{$order->quantity}}</td>
							<td>{{$order->amount}}</td>
							<td>
								{{$order->description}}
							</td>
							<td>
								{{$order->talla}}
							</td>
							<td>
								{{$order->color}}
							</td>
							<td>
								<a href="{{route('backend.user.detail', ['id' => $order->user->id])}}">{{$order->user->name}} {{$order->user->last_name}}</a>
							</td>
							<td>
								{{$order->shipping_address}}
							</td>
							<td>{{$order->created_at}}</td> 
							<td>{{$order->updated_at}}</td>  
							<input type="hidden" name="user_id" value="{{$order->user_id}}">
							<input type="hidden" name="user_email" value="{{$order->user->email}}">
							<input type="hidden" name="revista_id" value="{{$order->product_id}}">
							<input type="hidden" name="user_name" value="{{$order->user->name}}">
							<input type="hidden" name="user_last_name" value="{{$order->user->last_name}}">
						</tr> 
						{{csrf_field()}}
					  </tbody> 
				  </table>
				  <button class="btn btn-primary" type="submit">
				  	Actualizar
				  </button>
				</form>

			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->

@endsection