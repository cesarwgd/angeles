@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$categoria->nombre}}:</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			<br><br>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			    <br><br>
			@endif

			<div class="row"><!--row-->

				<div class="col-xs-12 col-sm-4"><!--col-->
					<form action="{{route('backend.updatemodel.categorie', ['id' => $categoria->id])}}" method="post"><!--form-->
						
					<div class="form-group"><!--form group-->
						<label for="CategoriaNombre">Nombre de la categoría:</label>
						<input type="text" name="nombre" class="form-control" placeholder="nombre de la categoría" id="CategoriaNombre" value="{{$categoria->nombre}}">
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaSlug">Slug (sin espacios, sólo guiones):</label>
						<input type="text" name="slug" class="form-control" placeholder="slug de la categoría" id="CategoriaSlug" value="{{$categoria->slug}}">
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaPadre">Padre:</label>
						<select name="parent" id="CategoriaPadre">

							<option value="">--Ninguno--</option>
							@foreach($categorias as $cat)
								@if($cat->id != $categoria->id)
								<option value="{{$cat->id}}" @if($categoria->parent == $cat->id) selected @endif>{{$cat->nombre}}</option>
								@endif
							@endforeach							
							
						</select>
					</div><!--/form group-->

					<div class="form-group"><!--form group-->
						<label for="CategoriaDescripción">Descripción:</label>
						<textarea name="descripcion" class="form-control" placeholder="descripción de la categoría" id="CategoriaDescripcion">{{$categoria->descripcion}}</textarea>
						
					</div><!--/form group-->

					{{csrf_field()}}

					<button type="submit" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Actualizar
					</button>

					</form><!--/form-->

				</div><!--/col-->

				<div class="col-xs-1 col-sm-4 pull-right">
					<form method="post" action="{{route('backend.deletemodel.categorie', ['id' => $categoria->id])}}">
						{{csrf_field()}}						
						<button type="submit" class="alert alert-danger">
							Eliminar
						</button>
					</form>
				</div>


			</div>	

		</div><!--/container-->

	</div><!--/main-->

@endsection