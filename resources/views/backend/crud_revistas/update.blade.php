@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$revista->name}}</h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif

				<div class="col-xs-12">
					<button class="btn btn-info">
						<a href="{{route('backend.revistas.files', ['id' => $revista->id])}}">Imagenes</a>
					</button>
					 <button class="btn btn-info">
						<a href="{{route('backend.revista.video', ['id' => $revista->id])}}"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> Video</a>
					</button>
					<button class="btn btn-info">
						<a href="{{route('watch.issue', ['hash_slug' => $revista->hash_slug, 'page' => 1])}}" target="_blank">Ver Revista</a>
					</button>
				</div>
				<br><br><br>
				<div class="clearfix"></div>
				
				<form method="post" action="{{route('backend.revista.update', ['revista' => $revista->id])}}" enctype="multipart/form-data"><!--form-->

					<div class="col-xs-12 col-sm-9"><!--col-->

						<div class="form-group">
							<label>Título:</label>
							<input type="text" name="name" placeholder="Título" class="form-control" value="{{$revista->name}}">
						</div>
						<div class="form-group">
							<label>URL:</label>
							<input type="text" name="slug" placeholder="URL" class="form-control" value="{{$revista->slug}}">
							<p class="peque">* Tiene que ser única | Reemplazar los espacios por "-" "_" sin ñ ni caracteres alfanuméricos</p>
						</div>
						<div class="form-group">
							<label>Descripción:</label>
							<textarea name="description" class="form-control wysiwyg-content" rows="12">								
							@if(!empty($revista->description))
							{!! $revista->description !!}
							@endif
							</textarea>
						</div>


						<div class="form-group">
							<label for="extracto">Descripción corta:</label>
							<textarea name="short_description" class="form-control" rows="3" id="extracto">{!! $revista->short_description !!}</textarea>
						</div>

						<div class="form-group">
							<label>Embed del video intriga:</label>
							<textarea name="video_intriga" class="form-control" rows="3">{!! $revista->video_intriga !!}</textarea>
							<br>
							<div>
								@if(!empty($revista->video_intriga))
									{!! $revista->video_intriga !!}
								@endif
							</div>
						</div>


						<hr>

						<div class="panel panel-info"><!--panel-->

						  <div class="panel-heading">SEO</div>

						  <div class="panel-body"><!--panel body-->

						    <div class="form-group">
						    	<label>Meta Title</label>
						    	<input type="text" name="meta_title" class="form-control" placeholder="Meta Title - Entre 30 a 50 palabras"@if(!empty($revista->meta_title)) value="{{$revista->meta_title}}" @endif>
						    </div>
						    <div class="form-group">
						    <label>Meta Keywords</label>
						    	<input type="text" name="meta_keywords" class="form-control" placeholder="Meta Keywords"@if(!empty($revista->meta_title)) value="{{$revista->meta_keywords}}" @endif>
						    </div>
						    <div class="form-group">
						    	<label>Meta Description</label>
						    	<textarea name="meta_description" rows="3" placeholder="Meta Description - Entre 75 a 90 Palabras" class="form-control">{{$revista->meta_description}}</textarea>
						    </div>
						  
						  </div><!--/panel body-->
						
						</div><!--/panel-->


					</div><!--/col-->

					<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->

						<div class="form-group">
							<label for="precio">SKU (5 caracteres):</label>
							<input type="text" name="sku" id="sku" placeholder="Codigo de artículo"@if(!empty($revista->sku)) value="{{$revista->sku}}" @endif>
							<p class="peque">* Tiene que ser único</p>
						</div>

						<div class="form-group">
							<label for="type">Tipo:</label>
							<select name="type" id="type">
								<option value="revista"<?php if($revista->type == "revista"){ echo " selected"; } ?>>Revista</option>
								<option value="video"<?php if($revista->type == "video"){ echo " selected"; } ?>>Video</option>
							</select>
						</div>

						<div class="form-group">
							<label for="precio">Precio:</label>
							<input type="text" name="price" id="precio" placeholder="Sin la 'S/.'" value="{{$revista->price}}">
						</div>

						<div class="form-group">
							<label for="publicada">Publicar:</label>
							<select name="published" id="publicada">
								<option value="1" <?php if($revista->published == 1){ echo "selected"; } ?>>
									Sí
								</option>
								<option value="2" <?php if($revista->published == 2){ echo "selected"; } ?>>
									No
								</option>
							</select>
						</div>

						<div class="form-group">
							<label>Imagen destacada:</label>
							<div class="imagen-destacada">
								@if(!empty($revista->main_pic))
								<figure>

									<a href="#" class="thumbnail">
									@if(Storage::disk('revista')->exists($revista->main_pic . "." . $revista->extension))
							      <img src="{{route('backend.getrevista.image', ['filename' => $revista->main_pic, 'extension' => $revista->extension])}}">
							      @endif
							    </a>
								</figure>
								@endif
							    <input type="file" id="exampleInputFile" name="main_pic">
							</div>

						</div>
						{{csrf_field()}}
						<button class="btn btn-primary" type="submit">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
						</button>

					</div><!--/col-->
					
				</form><!--/form-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

	<script>
    var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector: ".wysiwyg-content",
        height: 300,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>

@endsection