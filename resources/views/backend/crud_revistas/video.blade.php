@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1><a href="{{route('backend.revista.view', ['id' => $revista->id])}}">{{$revista->name}}</a></h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif
				
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach($errors->all() as $error)
							<li>{{$error}}</li>
							@endforeach
						</ul>
					</div>
				@endif
				

					<div class="col-xs-12 col-sm-9 m-bottom-60"><!--col-->
						<h4>Agregue un nuevo video:</h4>	
						<form method="post" action="{{route('backend.revista.postvideo', ['id' => $revista->id])}}" id="video-form">
							<div class="form-group">						
								<label>Codigo embed del video:</label>
								<input type="text" name="name" class="form-control">
								
							</div>
							<div class="form-group">						
								<label>Contraseña:</label>
								<input type="text" name="extra" class="form-control">	
							</div>
							{{csrf_field()}}
							<button class="btn btn-primary">
								GUARDAR
							</button>
						</form>
						<div class="clearfix"></div>

						<hr>
						<h4>Actualice la información de los videos existentes:</h4>	
						<form action="{{route('backend.revista.updatevideo', ['id' => $revista->id])}}" id="update-video-form" method="post">
							<?php $videos = $revista->files()->where('type', 'video')->get(); ?>
							@foreach($videos as $video)
							<div class="form-group">						
								<label>Codigo embed del video:</label>
								<input type="text" name="name[]" value="{{$video->name}}" class="form-control">
								<br>
								<div class="form-group">
									{!! $video->name !!}
								</div>
								<input type="hidden" name="id[]" value="{{$video->id}}">
							</div>
							<div class="form-group">						
								<label>Contraseña:</label>
								<input type="text" name="extra[]" value="{{$video->extra}}" class="form-control">	
							</div>
							<div class="form-group">						
								<label>URL para móviles:</label>
								<input type="text" name="mobile_url[]" value="{{$video->mobile_url}}" class="form-control">	
							</div>
							<hr>
							@endforeach
							{{csrf_field()}}
							@if(count($videos) > 0)
							<button class="btn btn-primary">
								ACTUALIZAR
							</button>
							@endif
						</form>		
							
						<div class="append-here-message">							
						</div>
						
						
					</div><!--/col-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->
	

@endsection

