@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Ordenes de venta</h1>

			<hr>

			<div class="row"><!--row-->
				
				<div class="col-xs-8 col-sm-6 ">
					<div class="pull-left">
						<form class="form-inline" action="{{route('backend.productos.ventas.searchresult')}}" method="GET">
					  
						  <div class="form-group">
						    <label for="inputBuscar" class="sr-only"># Orden</label>
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por codigo" name="code">
						  </div>
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Codigo</th> 							
							<th>Status</th>
							<th>Usuario</th> 
							<th>Revista</th> 
							<th>Cantidad S/.</th>
							<th>País</th>
							<th>Completada el:</th>  
						</tr> 
					</thead> 
					<tbody> 

						@if($order == null)
						<tr>
							<td>
								{{$error}}
							</td>
						</tr>
						@else

						<tr> 
							<th scope="row">1</th> 
							<td>
								<a href="{{route('backend.product.order.view', ['id' => $order->id])}}">{{$order->code}}</a>
							</td> 							
							<td>
								@if($order->status == "iniciada")
								<span class="label label-default">Incompleta</span>
								@elseif($order->status == "pendiente")
								<span class="label label-info">En Proceso</span>
								@elseif($order->status == "cancelada")
								<span class="label label-danger">Cancelada</span>
								@else
								<span class="label label-success">Completada</span>
								@endif
							</td>	
							<td>
								@if(!empty($order->user))
								{{$order->user->name}} {{$order->user->last_name}}
								@else
								<strong>Usuario Eliminado</strong>
								@endif
							</td>
							<td>{{$order->description}}</td>
							<td>S/ {{$order->amount}}</td>
							<td>{{$order->country}}</td>  
							<td>{{$order->updated_at}}</td>  
						</tr> 
						
						@endif
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

			

		</div><!--/container-->

	</div><!--/main-->

@endsection