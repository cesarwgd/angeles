@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-header-scripts')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Home Featured</h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<!--FORM TO CREATE NEW BANNERS-->
				<form action="{{route('backend.post.homefeatured')}}" method="POST" enctype="multipart/form-data">

					<div class="col-xs-12 col-sm-9">
						<div class="agregar-nuevas"><!--agregar nuevas-->		    			
						</div><!--/agregar nuevas-->
						<button class="btn btn-primary agregar-banner-btn">
							Agregar Fila +
						</button>
					</div>
					

					

					<div class="col-xs-12 col-sm-3">
						{{csrf_field()}}
						<button type="submit" class="btn btn-primary pull-right">
							CREAR
						</button>
					</div>

				</form>
				<!--/FORM TO CREATE NEW BANNERS-->

				<div class="clearfix"></div>

				<br><br>
				<h2>
					Banners Actuales:
				</h2>
				<hr>

				<!--FORM TO UPDATE THE EXISTING ONES-->
				<form action="{{route('backend.update.homefeatured')}}" method="post" enctype="multipart/form-data">

					<div class="col-xs-12 col-sm-9 banners-existentes"><!--col-->

						<ul id="sortable">
				    		<!--sortable-->

				    		@foreach($boxes as $box)
				    		<li class="col-xs-12 col-md-12 row-home-files" id="filebox-{{$box->id}}">				    			

				    			<div class='row'><!--row-->
				    				<div class='col-xs-6 col-sm-4 homebanners-update-rows'>
				    					<div class='form-group'>
				    						<label>Tipo:</label>
				    						<select name='type[]' class="form-control update-hbanners-select" required>
				    							<option selected disabled value=''>Eliga el tipo de archivo</option>
				    							<option value='image'<?php if($box->type == "image"){ echo " selected"; } ?>>Imagen</option>
				    							<option value='video'<?php if($box->type == "video"){ echo " selected"; } ?>>Video</option>
				    						</select>
				    						<div class='media-type-box'>
				    							<div class='media-image'<?php if($box->type == "image"){ echo " style='display:block';"; } ?>>
				    								<label>Agrege la imagen, JPG/PNG/JPEG no mayor a 2MB:</label>
				    								<input type='file' name='image_name[]' class="form-control">
				    								@if(Storage::disk('home')->exists($box->image_name))
				    								<a href="javascript:void(0)" class="thumbnail" data-class="">
												      <img src="{{Storage::disk('home')->url($box->image_name)}}">
												    </a>
				    								@endif
				    							</div>

				    							<div class='media-video'<?php if($box->type == "video"){ echo " style='display:block';"; } ?>>
				    								<label>Agrege el embed del video:</label>
				    								<textarea name='video_name[]' class="form-control"><?php if(!Storage::disk('home')->exists($box->video_name)){ echo $box->video_name; } ?></textarea><br><br>								
				    								<input type='file' name='video_file[]' class="form-control">
				    								<br>
				    								<label>Portada del video para mobiles:</label><br>
				    								<input type="file" name="video_cover[]" class="form-control">
				    								@if(Storage::disk('home')->exists($box->video_name))
				    								<video width="320" height="240" controls>
													  <source src="{{ Storage::disk('home')->url($box->video_name) }}" type="video/mp4">
													</video>
														@if(Storage::disk('home')->exists($box->video_cover))
														<a href="#" class="thumbnail">
															<img src="{{Storage::disk('home')->url($box->video_cover)}}">
														</a>
														@endif
													@else
				    								<div class="backend-home-video">
				    									{!!$box->video_name!!}
				    								</div>
				    								@endif
				    							</div>
				    						</div>
				    					</div>
				    					
				    				</div>

				    				<div class="col-xs-6 col-sm-4">
				    					<input type='hidden' name='position[]' value='{{$box->position}}' class='posicion'>
				    					<input type='hidden' name='id[]' value='{{$box->id}}' class='los-ids'>
				    					<div class='form-group'>
				    						<label>Texto:</label>
				    						<textarea class='form-control' name='description[]'>{{$box->description}}</textarea>
				    					</div>

				    				</div>
				    				<div class="col-xs-6 col-sm-3">
				    					<div class='form-group'>
				    						<label>URL a donde redirecciona:</label>
				    						<input type='text' name='url[]' class='form-control' value="{{$box->url}}">
				    					</div>
				    				</div>
				    				<div class="col-xs-6 col-sm-1">
				    					<br>
		    							<button class="btn btn-warning delete-row-banner" data-id="{{$box->id}}">X</button>
				    				</div>

				    			</div><!--/row-->								   
							    
				    		</li>
				    		@endforeach

				    		<!--sortable-->
				    	</ul>

					</div><!--/col-->

					<div class="col-xs-12 col-sm-3"><!--col-->
						{{csrf_field()}}
						<button type="submit" class="btn btn-primary pull-right"<?php if(count($boxes) == 0){ echo " disabled"; } ?>>
							ACTUALIZAR
						</button>
						<br><br>
						<hr>
						<br>
						<div class="append-here-message"></div>
						<button id="update-banner-position" class="btn btn-info pull-right"<?php if(count($boxes) == 0){ echo " disabled"; } ?>>
							ACTUALIZAR POSICIONES
						</button>
					</div><!--/col-->


				</form>
				<!--/FORM TO UPDATE THE EXISTING ONES-->

					


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->
	

@endsection

@section('page-footer-scripts')


<script type="text/javascript">

	var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.host;
	var rowPosition = {{$cantidad}}; // valor obtenido del controlador dependiendo de cuantos filas hay en la tabla

	// ADD ROWS

	$('.agregar-banner-btn').click(function(){
		
		rowPosition++;

		var fila = "<div class='row each-fila'>"
					+ "<div class='col-xs-6 col-sm-4 select-type'>"
					+ "<div class='form-group'>"
		    		+ "<label>Tipo:</label>" 
		    		+ "<select name='type[]' class='form-control choose-media' required>"		
		    		+ "<option selected disabled value=''>Eliga el tipo de archivo</option>"			
		    		+ "<option value='image'>Imagen</option>"				
		    		+ "<option value='video'>Video</option>"				
		    		+ "</select>"					
		    		+ "<div class='media-type-box'>"					
		    		+ "<div class='media-image'>"					
		    		+ "<label>Agrege la imagen, JPG/PNG/JPEG no mayor a 2MB:</label>"				
		    		+ "<input type='file' name='image_name[]' class='form-control'>"				
		    		+ "</div>"					
		    		+ "<div class='media-video'>"						
		    		+ "<label>Agrege el embed del video:</label>"						
		    		+ "<textarea name='video_name[]' class='form-control'></textarea>"
		    		+ "<br><br>"
		    		+ "<input type='file' name='video_file[]' class='form-control'>"
		    		+ "<br><label>Portada del video para mobiles:</label><br><input type='file' name='video_cover[]' class='form-control'>"					
		    		+ "</div>"					
		    		+ "</div>"						
		    		+ "</div>"						
		    		+ "</div>"					
		    		+ "<div class='col-xs-6 col-sm-4'>"				
		    		+ "<div class='form-group'>"		    				
		    		+ "<label>Texto:</label>"			
		    		+ "<textarea class='form-control' name='description[]'></textarea>"		
		    		+ "</div>"		
		    		+ "</div>"			
		    		+ "<div class='col-xs-6 col-sm-3'>"			
		    		+ "<div class='form-group'>"				
		    		+ "<label>URL a donde redirecciona:</label>"				
		    		+ "<input type='text' name='url[]' class='form-control'>"			
		    		+ "</div>"
		    		+ "</div>"		
		    		+ "<div class='col-xs-6 col-sm-1'>"		
		    		+ "<br><button class='btn btn-warning remove-row'>X</button>"			
		    		+ "</div>"				
		    		+ "</div>";

		$('.agregar-nuevas').append(fila);
		return false;
	})

	// REMOVE ROW
	$(document).on('click', '.remove-row', function(){
		$(this).parents('.each-fila').remove();
		/*if(rowPosition != 0){
			rowPosition--;
		}*/
	});


	// SELECT MEDIA TYPE ON CREATE NEW
	$(document).on('change', '.choose-media', function(){
		var type = $(this).val();
		if(type == "image"){
			$(this).parents('div.select-type').find('.media-image').show();
			$(this).parents('div.select-type').find('.media-video').hide();			
		}else{
			$(this).parents('div.select-type').find('.media-video').show();
			$(this).parents('div.select-type').find('.media-image').hide();
		}
	});

	//SELECT MEDIA TYPE FOR EXISTING ONES
	$(".update-hbanners-select").change(function(){
		var tipo = $(this).val();
		if(tipo == "image"){
			$(this).parents('.homebanners-update-rows').find('.media-image').show();
			$(this).parents('.homebanners-update-rows').find('.media-video').hide();			
		}else{
			$(this).parents('.homebanners-update-rows').find('.media-video').show();
			$(this).parents('.homebanners-update-rows').find('.media-image').hide();
		}
	});
	
	// DELETE BANNERS

	$(".delete-row-banner").click(function(e){
		e.preventDefault();
		var idErase = $(this).data('id'),
			parentId = $(this).parents('.row-home-files').attr('id');
		

		$("body").append("<div class='loading'></div>");
		$.ajax({
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type: 'post',
			dataType: 'json',
			url: protocol + "/administrador/homepage-featured/delete/box",
			data: {'id': idErase},
			success: function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-success'>Banner borrado exitosamente</div>");
				$("#"+parentId).remove();
				updatePositionOnDelete(response);
				
			}		
		}).fail(function(response){
			$(".loading").remove();
            $(".append-here-message").html("<div class='alert alert-danger'>Hubo un error. Intente de nuevo</div>");
            
        });
	});

	// SAVE POSITION CHANGES AND FILE CLASS

	$("#update-banner-position").click(function(e){
		e.preventDefault();
		// append el loading div
		$("body").append("<div class='loading'></div>");
		// seteamos los arrays que van a albergar la data
		var revista = $(this).data('revista');
		var positions = [];
		var ids = [];		

		// recolectamos la data y la ponemos en sus respectivos arrays
		$(".row-home-files").each(function(i){
			var currentPos = $(this).find('.posicion').attr('value');
			var currentId = $(this).find('.los-ids').attr('value');

			positions.push(currentPos);
			ids.push(currentId);
		});
		

		if(ids.length > 0){
			// ejecutamos el ajax call
			$.ajax({
				headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
				type: 'post',
				dataType: 'json',
				url: protocol + "/administrador/homepage-featured/update/positions",
				data: {positions: positions, ids: ids},
				success: function(response){
					$(".loading").remove();
					$(".append-here-message").html("<div class='alert alert-success'>"+response+"</div>");	
					
				}		

			}).fail(function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-danger'>Error al guardar los cambios</div>");
				
			});
		}else{
			alert('No hay banners');
		}
		
	});

	
	// update positions on ajax delete
	function updatePositionOnDelete(response)
	{
		for (i = 0; i < response.length; i++) {
			$("#filebox-"+response[i]['id']+" .posicion").attr('value', response[i]['position']);
		}
	}

</script>
<script type="text/javascript">
	$(function(){
		$( "#sortable" ).sortable({
			update: function(event, ui) {
				
		        $('.row-home-files').each(function(i) { 
		           var pos = $('.row-home-files').index(this); // updates the data object
		           $(this).find('.posicion').attr('value', pos + 1); // updates the attribute
		        });
		    }
		});
	});
</script>
@endsection