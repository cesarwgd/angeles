@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-header-scripts')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection



@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1><a href="{{route('backend.modelo.view', ['id' => $angel->id])}}">{{$angel->nombre}}</a></h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
				
				<!--aca iba el form-->

					<div class="col-xs-12 col-sm-9 m-bottom-60"><!--col-->

						<div class="form-group">
							<label>Seleccione las imagenes:</label>
							<form method="post" action="{{route('angeles.dropzone', ['id' => $angel->id])}}" class="dropzone" id="my-awesome-dropzone">
								{{csrf_field()}}
							</form>							
						</div>

						<div class="append-here-message">							
						</div>

						<hr>

						<div class="panel panel-info"><!--panel-->

						  <div class="panel-heading">Arrastre las imagenes para cambiar su orden y luego haga click en el botón <strong>GUARDAR POSICIÓN</strong></div>

						  <div class="panel-body"><!--panel body-->

						    <div class="row total-images"><!--row-->

						    	<ul id="sortable">
						    		@foreach($angel->hots()->orderBy('position')->get() as $image)
						    		<li class="col-xs-6 col-md-3 file-image-thumb" id="filebox-{{$image->id}}">
						    			<a href="#" class="delete-file-image" data-id="{{$image->id}}" data-pos="{{$image->position}}">X</a>
									    <a href="#" class="thumbnail">
									      <img src="{{Storage::disk('hotmas')->url($image->name)}}">
									    </a>
						    		</li>
						    		@endforeach
						    	</ul>
							  
							</div><!--/row-->
						  
						  </div><!--/panel body-->
						
						</div><!--/panel-->
										
						<input type="hidden" id="angel-id" value="{{$angel->id}}">
						<button class="btn btn-primary" id="update-position">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar Posición
						</button>					

					</div><!--/col-->

					


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

	

@endsection
@section('page-footer-scripts')
<script type="text/javascript">
	$(function(){
		$("#enviar-imagenes").click(function(e){
			e.preventDefault();
			var imagenes = $("#imagenes").val(),
				este = $(this);
			if(imagenes.length > 0 ){
				$("#images-form").submit();
				este.prop('disabled', true);
			}else{
				alert('eliga primero imagenes para subir');
			}
		});
	});
</script>
<script src="{{asset('js/dropzone.js')}}"></script>
<script type="text/javascript">

	var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.host;

	Dropzone.options.myAwesomeDropzone = {
	  paramName: "file", // The name that will be used to transfer the file
	  maxFilesize: 50, // MB
	  acceptedFiles: "image/*",	
	  uploadMultiple: true,
	  //method: 'post',
	  init: function() {
	    this.on("successmultiple", function(file, response) { 
	    	HandleDropzoneFileUpload.handleSuccess(response);
	    	//console.log(response);
	    });
	    this.on("error", function(file, errorMessage){
	    	HandleDropzoneFileUpload.handleError(errorMessage);
	    });
	  }
	  
	};
            
	var HandleDropzoneFileUpload = {

		handleError : function(response){
			console.log("Error al subir los archivos");
		},

		handleSuccess : function(response){
			for (i = 0; i < response.length; i++) {
				var objeto = "<li class='col-xs-6 col-md-3 file-image-thumb' id='filebox-"+response[i]['id']+"'>"
							 + "<a href='#' class='delete-file-image' data-id='"+response[i]['id']+"' data-pos='"+response[i]['position']+"'>X</a>"
							 +   "<a href='#'' class='thumbnail'>"
							 +     "<img src='" + protocol  + "/images/angeles/" + response[i]['name']  + "'>"
							 +   "</a>"
							 + "</li>"

				$("ul#sortable").append(objeto);
			}
			$(".append-here-message").html("<div class='alert alert-success'>Imagenes agregadas con éxito</div>");
		}

	};

	// DELETE IMAGES

	$(document).on('click', 'a.delete-file-image', function(e){
		e.preventDefault();
		var idErase = $(this).data('id'),
			parentId = $(this).parent().attr('id');

		

		$(".total-images").append("<div class='loading'></div>");
		$.ajax({
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type: 'post',
			dataType: 'json',
			url: protocol + "/administrador/angeles/filedelete/" + idErase,
			data: {'id': idErase},
			success: function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-success'>Imagen borrada exitosamente</div>");
				$("#"+parentId).remove();
				updatePositionOnDelete(response);
				//console.log(response);
			}		
		}).fail(function(response){
			$(".loading").remove();
            $(".append-here-message").html("<div class='alert alert-danger'>Hubo un error. Intente de nuevo</div>");
            //console.log(response);
        });
	});

	// SAVE POSITION CHANGES

	$("#update-position").click(function(){
		$(".total-images").append("<div class='loading'></div>");
		var positions = [];
		var ids = [];

		$(".delete-file-image").each(function(i){
			var currentPos = $(this).attr('data-pos');
			var currentId = $(this).attr('data-id');

			positions.push(currentPos);
			ids.push(currentId);
		});

		if(ids.length > 0){
			$.ajax({
				headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
				type: 'post',
				dataType: 'json',
				url: protocol + "/administrador/angeles/fileposition",
				data: {positions: positions, ids: ids},
				success: function(response){
					$(".loading").remove();
					$(".append-here-message").html("<div class='alert alert-success'>"+response+"</div>");	
					//console.log(response);
				}		

			}).fail(function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-danger'>Error al guardar los cambios</div>");
				//console.log(response);
			});
		}else{
			alert("No hay imagenes");
		}
		
	});

	// update positions onajax delete
	function updatePositionOnDelete(response)
	{
		for (i = 0; i < response.length; i++) {
			$("#filebox-"+response[i]['id']+" .delete-file-image").attr('data-pos', response[i]['position']);
		}
	}

</script>
<script type="text/javascript">
	$(function(){
		$( "#sortable" ).sortable({
			update: function(event, ui) {
				
		        $('.file-image-thumb').each(function(i) { 
		           var pos = $('.file-image-thumb').index(this); // updates the data object
		           $(this).find('.delete-file-image').attr('data-pos', pos + 1); // updates the attribute
		        });
		    }
		});
	});
</script>
@endsection