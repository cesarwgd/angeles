@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$angel->nombre}}</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			<div class="row"><!--row-->

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				<div class="col-xs-12">
					<button class="btn btn-info">
						<a href="{{route('backend.modelo.images', ['angel' => $angel->id])}}">Agregar imagenes</a>
					</button>
				</div>
				<br><br><br>
				
				<form method="post" action="{{route('backend.modelo.update', ['id' => $angel->id])}}" enctype="multipart/form-data"><!--form-->

					<div class="col-xs-12 col-sm-9"><!--col-->

						<div class="form-group">
							<label>Título:</label>
							<input type="text" name="nombre" placeholder="Título" class="form-control" value="{{$angel->nombre}}">
						</div>
						<div class="form-group">
							<label>URL:</label>
							<input type="text" name="slug" placeholder="URL" class="form-control" value="{{$angel->slug}}">
							<p class="peque">* Tiene que ser única | Reemplazar los espacios por "-" "_" sin ñ ni caracteres alfanuméricos</p>
						</div>
						<div class="form-group">
							<label>Descripción:</label>
							<textarea name="descripcion" class="form-control wysiwyg-content" rows="12" id="wysiwyg-editor">
								@if(!empty($angel->descripcion))
								{!! $angel->descripcion !!}
								@endif
							</textarea>
						</div>

						<hr>

						<div class="panel panel-info"><!--panel-->

						  <div class="panel-heading">SEO</div>

						  <div class="panel-body"><!--panel body-->

						    <div class="form-group">
						    	<label>Meta Title</label>
						    	<input type="text" name="meta_title" class="form-control" placeholder="Meta Title - Entre 30 a 50 palabras"@if(!empty($angel->meta_title)) value="{{$angel->meta_title}}" @endif>
						    </div>
						    <div class="form-group">
						    <label>Meta Keywords</label>
						    	<input type="text" name="meta_keywords" class="form-control" placeholder="Meta Keywords"@if(!empty($angel->meta_keywords)) value="{{$angel->meta_keywords}}" @endif>
						    </div>
						    <div class="form-group">
						    	<label>Meta Description</label>
						    	<textarea name="meta_description" rows="3" placeholder="Meta Description - Entre 75 a 90 Palabras" class="form-control">@if(!empty($angel->meta_description)){{$angel->meta_description}} @endif</textarea>
						    </div>
						  
						  </div><!--/panel body-->
						
						</div><!--/panel-->


					</div><!--/col-->

					<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->											

						<div class="form-group">
							<label>Imagen destacada:</label>
							<div class="imagen-destacada">
								@if(!empty($angel->main_pic))
								<figure>
									<a href="#" class="thumbnail">
									@if(Storage::disk('hotmas')->exists($angel->main_pic))
								      <img src="{{Storage::disk('hotmas')->url($angel->main_pic)}}">
								      @endif
								    </a>
								</figure>
								@endif
							    <input type="file" id="exampleInputFile" name="main_pic">
							</div>

						</div>
						{{csrf_field()}}
						<button class="btn btn-primary" type="submit">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
						</button>

					</div><!--/col-->
					
				</form><!--/form-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

<script>
    
	var editor_id = "wysiwyg-editor";
tinymce.PluginManager.add('instagram', function(editor, url) {
        // Add a button that opens a window
        editor.addButton('instagram', {
            text: 'Instagram',
            icon: false,
            onclick: function() {
                // Open window
                editor.windowManager.open({
                    title: 'Instagram Embed',
                    body: [
                        {   type: 'textbox',
                            size: 40,
                            height: '100px',
                            name: 'instagram',
                            label: 'instagram'
                        }
                    ],
                    onsubmit: function(e) {
                        // Insert content when the window form is submitted
                        console.log(e.data.instagram);
                        var embedCode = e.data.instagram;
                        var script = embedCode.match(/<script.*<\/script>/)[0];
                        var scriptSrc = script.match(/".*\.js/)[0].split("\"")[1];
                        console.log(script, scriptSrc);

                        var sc = document.createElement("script");
                        sc.setAttribute("src", scriptSrc);
                        sc.setAttribute("type", "text/javascript");

                        var iframe = document.getElementById(editor_id + "_ifr");
                        var iframeHead = iframe.contentWindow.document.getElementsByTagName('head')[0];

                        tinyMCE.activeEditor.insertContent(e.data.instagram);
                        iframeHead.appendChild(sc);
                        setTimeout(function() {
                        	iframe.contentWindow.instgrm.Embeds.process();
                        }, 1000)
                        // editor.insertContent('Title: ' + e.data.title);
                    }
                });
            }
        });
    });


    var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector: ".wysiwyg-content",
        height: 300,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen instagram",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | instagram",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);

</script>

@endsection 