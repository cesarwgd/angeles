@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Modelos</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			<div class="row"><!--row-->
				
				<div class="col-xs-8 col-sm-6">
					
					<form class="form-inline" action="{{route('backend.modelos.searchresult')}}" method="GET">
				  
					  <div class="form-group">						    
					    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por nombre" name="nombre">
					  </div>					  
					  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
					</form>
					
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Nombre</th> 
							<th>Creado el:</th> 
							<th>Publicado el:</th> 
							<th>Imagenes</th> 
							<th>Editar</th> 
							<th>Borrar</th> 
						</tr> 
					</thead> 
					<tbody> 

						@if($angels == null)
						<tr> 
							
							<td>
								{{$error}}
							</td>
							
						</tr> 
						@else
							@foreach($angels as $angel)
							<tr> 
								<th scope="row">1</th> 
								<td><a href="{{route('backend.modelo.view', ['id' => $angel->id])}}">{{$angel->nombre}}</a></td> 
								<td>{{$angel->created_at}}</td> 
								<td>{{$angel->updated_at}}</td>
								<td>
									<button class="btn btn-info">
										<a href="{{route('backend.modelo.images', ['id' => $angel->id])}}">Imagenes</a>
									</button>
								</td>
								<td>
									<button class="btn btn-primary">
										<a href="{{route('backend.modelo.view', ['id' => $angel->id])}}">Editar</a>
									</button>
								</td>
								<td>
									<button class="btn btn-danger">
										<a href="{{route('backend.modelo.delete', ['id' => $angel->id])}}" class="delete-post-btn">Borrar</a>
									</button>
								</td> 
							</tr> 
							@endforeach
						@endif
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->			

		</div><!--/container-->

	</div><!--/main-->
<script type="text/javascript">
	$(function(){
		$("a.delete-post-btn").click(function(e){
			
			if(!confirm("Esta seguro?")){
				e.preventDefault();
			}
		});
	});
</script>
@endsection