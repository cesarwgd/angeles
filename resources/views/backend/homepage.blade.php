@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Homepage</h1>

			<hr>

			

			<div class="m-top-60"><!--v top 60-->
				
				<p><a href="{{route('backend.homebanners')}}">Banners</a></p>
				<p><a href="{{route('backend.homefeatured')}}">Featured</a></p>
				

			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->

@endsection