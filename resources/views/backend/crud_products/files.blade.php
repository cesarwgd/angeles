@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-header-scripts')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1><a href="{{route('backend.product.detail', ['id' => $product->id])}}">{{$product->name}}</a></h1>

			<hr>

			<div class="row"><!--row-->

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif
				
				

					<div class="col-xs-12 col-sm-9 m-bottom-60"><!--col-->

						<div class="form-group">
							<label>Seleccione las imagenes / <strong>COLOQUE LA PRINCIPAL AL INICIO</strong>:</label>
							<form action="{{route('backend.product.uploadimages', ['id' => $product->id])}}" class="dropzone" id="my-awesome-dropzone">
								{{csrf_field()}}
							</form>
							<!--<div class="imagen-destacada">								
							    <input type="file" id="imagenes" name="images[]" multiple>
							</div>-->
						</div>

						<div class="append-here-message">							
						</div>

						<hr>

						<div class="panel panel-info"><!--panel-->

						  <div class="panel-heading">Arrastre las imagenes para cambiar su orden y luego haga click en el botón <strong>GUARDAR POSICIÓN</strong></div>

						  <div class="panel-body"><!--panel body-->

						    <div class="row total-images"><!--row-->

						    	<ul id="sortable">
						    		@foreach($product->productImages()->orderBy('position')->get() as $image)
						    		<li class="col-xs-6 col-md-3 file-image-thumb" id="filebox-{{$image->id}}">
						    			<a href="#" class="delete-file-image" data-id="{{$image->id}}" data-pos="{{$image->position}}">X</a>									   
									    <a href="#" class="thumbnail">
									      <img src="{{Storage::disk('products')->url($image->name)}}">
									    </a>
						    		</li>
						    		@endforeach
						    	</ul>
							  
							</div><!--/row-->
						  
						  </div><!--/panel body-->
						
						</div><!--/panel-->
										
						
						<button class="btn btn-primary" id="update-position" data-revista="{{$product->id}}">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar Posición
						</button>	

					</div><!--/col-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->
	

@endsection

@section('page-footer-scripts')

<script src="{{asset('js/dropzone.js')}}"></script>
<script type="text/javascript">

	var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.host;

	Dropzone.options.myAwesomeDropzone = {
	  paramName: "file", // The name that will be used to transfer the file
	  maxFilesize: 50, // MB
	  acceptedFiles: "image/*",	
	  uploadMultiple: true,  
	  init: function() {
	    this.on("successmultiple", function(file, response) { 
	    	HandleDropzoneFileUpload.handleSuccess(response);
	    	//console.log(response);
	    });
	    this.on("error", function(file, errorMessage){
	    	HandleDropzoneFileUpload.handleError(errorMessage);
	    });
	  }
	};

	var HandleDropzoneFileUpload = {

		handleError : function(errorMessage){
			$(".append-here-message").html("<div class='alert alert-success'>"+errorMessage+"</div>");
		},

		handleSuccess : function(response){			
			$(".append-here-message").html("<div class='alert alert-success'>"+response+"</div>");
		}

	};

	// DELETE IMAGES

	$("a.delete-file-image").click(function(e){
		e.preventDefault();
		var idErase = $(this).data('id'),
			parentId = $(this).parent().attr('id');

		

		$(".total-images").append("<div class='loading'></div>");
		$.ajax({
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type: 'post',
			dataType: 'json',
			url: protocol + "/administrador/productos/imagenes/delete/" + idErase,
			data: {'id': idErase},
			success: function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-success'>Imagen borrada exitosamente</div>");
				$("#"+parentId).remove();
				updatePositionOnDelete(response);
				//console.log(response);
			}		
		}).fail(function(response){
			$(".loading").remove();
            $(".append-here-message").html("<div class='alert alert-danger'>Hubo un error. Intente de nuevo</div>");
            //console.log(response);
        });
	});

	// SAVE POSITION CHANGES AND FILE CLASS

	$("#update-position").click(function(){
		// append el loading div
		$(".total-images").append("<div class='loading'></div>");
		// seteamos los arrays que van a albergar la data
		var revista = $(this).data('revista');
		var positions = [];
		var ids = [];
		//var clases = [];

		// recolectamos la data y la ponemos en sus respectivos arrays
		$(".delete-file-image").each(function(i){
			var currentPos = $(this).attr('data-pos');
			var currentId = $(this).attr('data-id');

			positions.push(currentPos);
			ids.push(currentId);
		});

		/*$("a.thumbnail").each(function(i){
			var claseType = $(this).attr('data-class');
			clases.push(claseType);
		});*/

		if(ids.length > 0){
			// ejecutamos el ajax call
			$.ajax({
				headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
				type: 'post',
				dataType: 'json',
				url: protocol + "/administrador/productos/imagenes/position/" + revista,
				data: {positions: positions, ids: ids},
				success: function(response){
					$(".loading").remove();
					$(".append-here-message").html("<div class='alert alert-success'>"+response+"</div>");	
					//console.log(response);
				}		

			}).fail(function(response){
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-danger'>Error al guardar los cambios</div>");
				//console.log(response);
			});
		}else{
			alert('No hay imagenes');
		}
		
	});

	// ELEGIR COMO PREVIEW DE LA REVISTA

	/*$("a.thumbnail").click(function(){
		$(this).toggleClass('preview');
		// si tiene la clase preview se cambia su valor del data a preview
		var txt = $(this).hasClass('preview') ? 'preview' : 'content';
		$(this).attr('data-class', txt);
		return false;
	})*/

	// update positions onajax delete
	function updatePositionOnDelete(response)
	{
		for (i = 0; i < response.length; i++) {
			$("#filebox-"+response[i]['id']+" .delete-file-image").attr('data-pos', response[i]['position']);
		}
	}

</script>
<script type="text/javascript">
	$(function(){
		$( "#sortable" ).sortable({
			update: function(event, ui) {
				
		        $('.file-image-thumb').each(function(i) { 
		           var pos = $('.file-image-thumb').index(this); // updates the data object
		           $(this).find('.delete-file-image').attr('data-pos', pos + 1); // updates the attribute
		        });
		    }
		});
	});
</script>
@endsection