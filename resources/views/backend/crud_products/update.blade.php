@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$product->name}}</h1>

			<hr>
			<a href="{{route('backend.product.images', ['id' => $product->id])}}" class="btn btn-primary">
				IMAGENES
			</a>
			<br><br>

			<div class="row"><!--row-->

				@if ($errors->any())
				    <div class="alert alert-danger">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif

				@if(session()->has('exito'))
				<div class="alert alert-success">
					{{session()->get('exito')}}
				</div>
				@endif
				
				<form method="post" action="{{route('backend.product.update', ['id' => $product->id])}}" enctype="multipart/form-data" id="update-product-form"><!--form-->

					<div class="col-xs-12 col-sm-9"><!--col-->

						<div class="form-group">
							<label>Nombre:</label>
							<input type="text" name="name" placeholder="Nombre del producto" class="form-control" value="{{$product->name}}" required>
						</div>

						<div class="form-group">
							<label>URL del producto:</label><br>
							<strong>SIN ESPACIOS NI "Ñ" NI CARACTERES. REEMPLAZAR LOS ESPACIOS POR GUIONES "-" Y LAS Ñs POR N</strong>
							<input type="text" name="slug" placeholder="URL del producto" class="form-control" value="{{$product->slug}}" required>
						</div>
						<div class="form-group">
							<label>Descripción:</label>
							<textarea name="description" class="form-control wysiwyg-content" rows="12">{!!$product->description!!}</textarea>
						</div>	

						<div class="row"><!--row-->
							
							<div class="col-xs-6"><!--col-->

								<div class="form-group">
									<label for="size">Tallas:</label>
									<br>
									<div id="add-talla-here">
										<?php 
											if($product->size != null){
											$tallas = explode(",", $product->size);
											foreach ($tallas as $talla) {
										?>
										<div>
											<input type='text' name='tallas[]' maxlength='14' placeholder='SOLO NUMEROS' class='las-tallas' value="<?php echo $talla; ?>"> <a class='remove-this' href='#'><strong>X</strong></a><br><br>
										</div>
										<?php 
												}
											} 
										?>
									</div>
									<a class="btn btn-primary agrega-talla">
										Agregar talla
									</a>
									<input type="hidden" name="size" id="size">
								</div>
						
							</div><!--/col-->

							<div class="col-xs-6"><!--col-->
								
								<div class="form-group">
									<label for="size">Colores:</label>
									<br> 
									<div id="add-color-here">
										<?php 
											if($product->color != null){
											$colores = explode(",", $product->color);
											foreach ($colores as $color) {
										?>
										<div>
											<input type='text' name='colores[]' maxlength='20' placeholder='COLOR' class='los-colores' value="<?php echo $color; ?>"> <a class='remove-this' href='#'><strong>X</strong></a><br><br>
										</div>
										<?php 
												}
											} 
										?>
									</div>	
									<button class="btn btn-primary agrega-color">
										Agregar color
									</button>
									<input type="hidden" name="color" id="color">
								</div>

							</div><!--/col-->

						</div><!--/row-->			

						<hr>

						<div class="panel panel-info"><!--panel-->

						  <div class="panel-heading">SEO</div>

						  <div class="panel-body"><!--panel body-->

						    <div class="form-group">
						    	<label>Meta Title</label>
						    	<input type="text" name="meta_title" class="form-control" placeholder="Meta Title - Entre 30 a 50 palabras" maxlength="70" value="{{$product->meta_title}}">
						    </div>
						    <div class="form-group">
						    <label>Meta Keywords</label>
						    	<input type="text" name="meta_keywords" class="form-control" placeholder="Meta Keywords" maxlength="190" value="{{$product->meta_keywords}}">
						    </div>
						    <div class="form-group">
						    	<label>Meta Description</label>
						    	<textarea name="meta_description" rows="3" placeholder="Meta Description - Entre 75 a 90 Palabras" class="form-control" maxlength="120">{{$product->meta_description}}</textarea>
						    </div>
						  
						  </div><!--/panel body-->
						
						</div><!--/panel-->


					</div><!--/col-->

					<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->

						<div class="form-group">
							<label for="precio">SKU (5 caracteres):</label>
							<input type="text" name="sku" id="sku" placeholder="Codigo de artículo" maxlength="5" value="{{$product->sku}}">
						</div>

						<div class="form-group">
							<label for="stock">Stock:</label>
							<input type="number" name="stock" id="stock" value="{{$product->stock}}" required>
						</div>

						

						<div class="form-group">
							<label for="precio">Dimensiones:</label>
							<input type="text" name="dimensions" id="dimensions" placeholder="Dimensiones" value="{{$product->dimensions}}">
						</div>

						<div class="form-group">
							<label for="precio">Precio:</label>
							<input type="text" name="price" id="precio" placeholder="Sin la 'S/.'" value="{{$product->price}}" requried>
						</div>

						<div class="form-group">
							<label for="precio">Costo de envío:</label>
							<input type="text" name="delivery_price" id="delivery_price" placeholder="Costo de envío" value="{{$product->delivery_price}}">
						</div>


						<div class="form-group">
							<label for="publicada">Publicar:</label>
							<select name="published" id="publicada">
								<option value="1"<?php if($product->published == 1){ echo " selected"; } ?>>
									Sí
								</option>
								<option value="2"<?php if($product->published == 2){ echo " selected"; } ?>>
									No
								</option>
							</select>
						</div>

						<!--
						<div class="form-group">
							<label>Imagen destacada:</label>
							<div class="imagen-destacada">
								
							    <input type="file" id="exampleInputFile" name="main_pic">
							</div>

						</div>-->
						{{csrf_field()}}
						<button class="btn btn-primary" type="submit">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
						</button>

					</div><!--/col-->
					
				</form><!--/form-->


			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

	<script>
	// tallas y colores
	var talla_input = "<div><input type='text' name='tallas[]' maxlength='14' placeholder='SOLO NUMEROS' class='las-tallas'> <a class='remove-this' href='#'><strong>X</strong></a><br><br></div>",
		color_input = "<div><input type='text' name='colores[]' maxlength='20' placeholder='COLOR' class='los-colores'> <a class='remove-this' href='#'><strong>X</strong></a><br><br></div>";

	$(".agrega-talla").click(function(){
		$("#add-talla-here").append(talla_input);
		return false;
	});

	$(".agrega-color").click(function(){
		$("#add-color-here").append(color_input);
		return false;
	});

	$("body").on('click', 'a.remove-this', function(){
		$(this).parent().remove();
		return false;
	});

	//submit form

	$("#update-product-form").submit(function(){
		
		var tallas_num = $('.las-tallas').length;
		var colores_num = $('.los-colores').length;

		var total_tallas = [];
		var total_colores = [];

		if(tallas_num != null && tallas_num > 0){
			//for (var i = 0; i < tallas_num; i++) {
				$('.las-tallas').each(function() {
					var val = $(this).val();
				    if(val != ''){
				    	total_tallas.push(val);
				    }
				});
			//}
			//console.log(total_tallas);
			$("#size").val(total_tallas);
			//console.log($("#size").val());
		}

		if(colores_num != null && colores_num > 0){
			//for (var i = 0; i < tallas_num; i++) {
				$('.los-colores').each(function() {
					var valc = $(this).val();
				    if(valc != ''){
				    	total_colores.push(valc);
				    }
				});
			//}
			//console.log(total_tallas);
			$("#color").val(total_colores);
			//console.log($("#size").val());
		}

	});

	// tiny mce editor	
    var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector: ".wysiwyg-content",
        height: 300,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
</script>

@endsection