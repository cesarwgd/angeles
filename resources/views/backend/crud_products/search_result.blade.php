@extends('backend.layouts.main')

@section('page-metas')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Productos</h1>

			<hr>

			<div class="row"><!--row-->
			

			<div class="append-here-message">				
			</div>
				
				<div class="col-xs-8 col-sm-6">					
						<form class="form-inline" action="{{route('backend.productos.search')}}" method="GET">
					  
						  <div class="form-group">						    
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por nombre" name="name">
						  </div>
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>					
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>Revista</th> 
							<th>SKU</th> 
							<th>Precio</th>							
							<th>Publicada el:</th>
							<th>Actualizada el:</th>
							<th>Imagenes</th>							
							<th>Borrar</th> 
						</tr> 
					</thead> 
					<tbody> 
						@if($products == null)
						<tr>
							<td>
								{{$error}}
							</td>
						</tr>
						@else
							@foreach($products as $product)
							<tr id="fila-{{$product->id}}"> 
								<th scope="row">{{$loop->iteration}}</th> 
								<td>
									<a href="{{route('backend.product.detail', ['id' => $product->id])}}">
										{{$product->name}}
									</a>
								</td> 
								<td>{{$product->sku}}</td> 
								<td>{{$product->price}}</td>
								<td>{{$product->created_at}}</td> 
								<td>{{$product->updated_at}}</td> 
								<td>
									<button class="btn btn-info">
										<a href="{{route('backend.product.images', ['id' => $product->id])}}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Imagenes</a>
									</button>
								</td>								
								<td>
									<button class="btn btn-danger">
										<a href="#" class="borrar-revista" data-id="{{$product->id}}">Borrar</a>
									</button>
								</td> 
							</tr> 
							@endforeach
						@endif
						
					  </tbody> 
				  </table>
				
				  <div class="are-you-sure">
					<div class="box">
						<h4>Esta seguro que desea borrar esta cuenta?</h4>
						<button class="btn btn-warning no-borrar">
							<a href="#">Cancelar</a>
						</button>
						 <button class="btn btn-danger proceder-borrar">
							<a href="#">Eliminar</a>
						</button>
					</div>
				</div>

			</div><!--v top 60-->

			

		</div><!--/container-->

	</div><!--/main-->

@endsection
@section('page-footer-scripts')
<script type="text/javascript">
	$(function(){

		var id;
		var protocol = ('https:' == document.location.protocol ? 'https://' : 'http://') + window.location.host;

		$(".borrar-revista").click(function(){
			$(".are-you-sure").fadeIn();
			id = $(this).data('id');
			//console.log(id);
			return false;
		});

		$(".no-borrar").click(function(){
			$(".are-you-sure").fadeOut();
			return false;
		});

		$(".proceder-borrar").click(function(e){
			e.preventDefault();

			$('body').append("<div class='loading'></div>");

			$.ajax({
			headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
			type: 'post',
			dataType: 'json',
			url: protocol + "/administrador/producto/delete/" + id,
			data: {id: id},
			success: function(response){
				$(".are-you-sure").fadeOut();
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-success'>Revista borrada exitosamente</div>");
				$("#fila-"+id).remove();
				console.log(response);
			}		

			}).fail(function(response){
				$(".are-you-sure").fadeOut();
				$(".loading").remove();
				$(".append-here-message").html("<div class='alert alert-danger'>Error al guardar los cambios</div>");
				console.log(response);
			});
		});

	});
</script>
@endsection