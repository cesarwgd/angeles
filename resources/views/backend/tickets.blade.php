@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Support Tickets:</h1>

			<hr>

			<div class="row"><!--row-->
				
				
				<div class="col-xs-8 col-sm-6 pull-right">
					<div class="pull-right">
						<form class="form-inline" action="{{route('backend.search.ticket')}}" method="GET">
					  
						  <div class="form-group">
						    
						    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por ID" name="ticket_id">
						  </div>
						  {{csrf_field()}}
						  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
						</form>
					</div>
				</div>
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>First Name</th> 
							<th>Last Name</th> 
							<th>Email</th>
							<th>Ticket ID</th>
							<th>Creado el:</th>
							<th>Estado</th>
							<th></th> 
						</tr> 
					</thead> 
					<tbody> 
						
						@foreach($tickets as $ticket)
						<tr> 
							<td scope="row">1</td> 
							@if(!empty($ticket->user))
							<td>{{$ticket->user->name}}</td> 
							<td>{{$ticket->user->last_name}}</td> 
							<td>{{$ticket->user->email}}</td>
							@endif
							<td>{{$ticket->id}}</td>
							<td>{{$ticket->created_at}}</td>
							<td>
							@if($ticket->estado == "abierto")
								<span class="label label-primary">{{$ticket->estado}}</span>
							@else
								<span class="label label-success">{{$ticket->estado}}</span>
							@endif								
							</td> 
							<td>
								<button class="btn btn-primary">
									<a href="{{route('backend.view.ticket', ['id' => $ticket->id])}}">Ver Ticket</a>
								</button>
							</td> 
						</tr>
						@endforeach 
						
					  </tbody> 
				  </table>
				
				  {{$tickets->links()}}
			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->

@endsection