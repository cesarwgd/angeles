@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Usuarios Administradores:</h1>

			<hr>			

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 
							<th>#</th> 
							<th>First Name</th> 
							<th>Last Name</th> 
							<th>Email</th>
							<th>Rol</th> 
						</tr> 
					</thead> 
					<tbody> 

					@foreach($users as $user)
						<tr> 
							<th scope="row">{{$loop->iteration}}</th> 
							<td><a href="{{route('admin.detail.view', ['id' => $user->id])}}">{{$user->name}}</a></td> 
							<td><a href="{{route('admin.detail.view', ['id' => $user->id])}}">{{$user->last_name}}</a></td> 
							<td>{{$user->email}}</td>
							<td>
								@foreach($user->roles as $role)
								{{$role->nombre}}
								@endforeach
							</td> 
						</tr> 
					@endforeach						
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

			<div class="row"><!--row-->
				
				<div class="col-xs-12 col-sm-12">

					<h4>Agregar usuario:</h4>
					<hr>

					@if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					
					@if(session()->has('exito'))
					<div class="alert alert-success">
						{{session()->get('exito')}}
					</div>
					@endif

					<form class="form-inline" method="post" action="{{route('admin.create.new')}}">
				  
					  <div class="form-group">					    
					    <input type="text" class="form-control" placeholder="Nombre" name="name">
					    <input type="text" class="form-control" placeholder="Apellidos" name="last_name">
					    <input type="email" class="form-control" placeholder="@Correo" name="email">
					    <input type="password" class="form-control" placeholder="Contraseña / mínimo 6 caracteres" name="password">
					    <select name="role" class="form-control">
					    	<option disabled selected>Rol:</option>
					    	@foreach($roles as $role)
					    	<option value="{{$role->id}}">{{$role->nombre}}</option>					    	
					    	@endforeach
					    </select>
					  </div>
					  {{csrf_field()}}
					  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Usuario</button>
					</form>
					
				</div>
			</div><!--/row-->

		</div><!--/container-->

	</div><!--/main-->

@endsection