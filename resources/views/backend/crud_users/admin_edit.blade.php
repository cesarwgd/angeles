@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$user->name}} {{$user->last_name}}</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			<div class="row"><!--row-->
				
				<form method="post" action="{{route('admin.detail.update', ['id' => $user->id])}}" enctype="multipart/form-data"><!--form-->
					<div class="col-xs-12 col-sm-9"><!--col-->

						<div class="form-group">
							<label for="NombreAdmin">Nombre:</label>
							<input type="text" name="name" placeholder="Nombres" class="form-control" id="NombreAdmin" value="{{$user->name}}">
						</div>

						<div class="form-group">
							<label for="ApellidosAdmin">Apellidos:</label>
							<input type="text" name="last_name" placeholder="Apellidos" class="form-control" id="ApellidosAdmin" value="{{$user->last_name}}">
						</div>

						<div class="form-group">
							<label for="CorreoAdmin">Correo:</label>
							<input type="email" name="email" placeholder="@Correo" class="form-control" id="CorreoAdmin" value="{{$user->email}}">
						</div>

						<div class="form-group">
							<label for="PasswordAdmin">Password:</label>
							<input type="password" name="password" placeholder="******" class="form-control" id="PasswordAdmin">
						</div>
						


					</div><!--/col-->

					<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->

						<div class="form-group">
							<label for="AdminRol">Rol:</label>
							<select name="rol" id="AdminRol" class="form-control">
								<option value="4" @if($user->hasRole('editor')) selected @endif>Editor</option>
								<option value="3" @if($user->hasRole('angel')) selected @endif>Modelo</option>
								<option value="5" @if($user->hasRole('admin')) selected @endif>Admin</option>
							</select>
						</div>						

						<div class="form-group">
							<label>Imagen destacada:</label>
							<div class="imagen-destacada">
								@if(!empty($user->profile_pic))
									@if(Storage::disk('users')->exists($user->profile_pic))
									<figure>
										<a href="#" class="thumbnail">
									      <img src="{{Storage::disk('users')->url($user->profile_pic)}}">
									    </a>
									</figure>
									@endif
								@endif
							    <input type="file" id="profile_pic" name="profile_pic">
							</div>

						</div>
						{{csrf_field()}}
						<button class="btn btn-primary" type="submit">
							<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
						</button>

					</div><!--/col-->
					
				</form><!--/form-->



				<div class="col-xs-12">
					<button class="btn btn-danger" id="eliminar-cuenta">
						<a href="#">Eliminar cuenta</a>
					</button>
				</div>
				<div class="are-you-sure">
					<div class="box">
						<h4>Esta seguro que desea borrar esta cuenta?</h4>
						<button class="btn btn-warning no-borrar">
							<a href="#">Cancelar</a>
						</button>
						 <button class="btn btn-danger proceder-borrar">
							<a href="#">Eliminar</a>
						</button>
					</div>
				</div>
				<form action="{{route('admin.backend.delete', ['id' => $user->id])}}" method="post" class="hidden-form" id="hidden-form">
					{{csrf_field()}}
				</form>

			</div><!--/row-->		

			

		</div><!--/container-->

	</div><!--/main-->

	
<script type="text/javascript">
	$(function(){

		$("#eliminar-cuenta").click(function(){
			$(".are-you-sure").fadeIn();
			return false;
		});

		$(".no-borrar").click(function(){
			$(".are-you-sure").fadeOut();
			return false;
		});

		$(".proceder-borrar").click(function(){
			$("#hidden-form").submit();
		});

	});
</script>
@endsection