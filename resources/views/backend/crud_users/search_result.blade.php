@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Usuarios:</h1>

			<hr>

			<div class="row"><!--row-->
				<div class="col-xs-12 col-sm-12">
					<form class="form-inline" action="{{route('backend.user.search')}}" method="GET"><!--form-->
					  
					  <div class="form-group">					    
					    <input type="text" class="form-control" id="inputBuscar" placeholder="Buscar por correo" name="email">
					  </div>
					  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
					</form><!--/form-->
				</div>				
				
			</div><!--/row-->

			<div class="m-top-60"><!--v top 60-->
				
				<table class="table table-hover"> 
					<thead> 
						<tr> 							
							<th>Nombres</th> 
							<th>Apellidos</th> 
							<th>Correo</th>
							<th>Tipo</th>  
						</tr> 
					</thead> 
					<tbody> 
						
						@if($user == null)
						<tr> 							
							<td>{{$error}}</td>							
						</tr> 
						@else
						<tr> 							
							<td><a href="{{route('backend.user.detail', ['id' => $user->id])}}">{{$user->name}}</a></td> 
							<td><a href="{{route('backend.user.detail', ['id' => $user->id])}}">{{$user->last_name}}</a></td> 
							<td>{{$user->email}}</td> 
							<td>
								@foreach($user->roles as $role)
								{{$role->nombre}}
								@endforeach
							</td>
						</tr> 
						@endif
						
						
					  </tbody> 
				  </table>
				

			</div><!--v top 60-->

			

		</div><!--/container-->

	</div><!--/main-->

@endsection