@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>{{$user->name}} {{$user->last_name}}</h1>

			<hr>

			@if(session()->has('exito'))
			<div class="alert alert-success">
				{{session()->get('exito')}}
			</div>
			@endif

			@if(session()->has('falta'))
			<div class="alert alert-warning">
				{{session()->get('falta')}}
			</div>
			@endif

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif


			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1">Info</a></li>
			  <li role="presentation"><a href="#tab2">Revistas</a></li>
			  <li role="presentation"><a href="#tab3">Ordenes</a></li>
			</ul>

			<div class="tabs-container"><!--tabs container-->
				
				<div class="tab-box" id="tab1"><!--tab 1-->
					
					<div class="row"><!--row-->
				
					<form method="post" action="{{route('admin.detail.update', ['id' => $user->id])}}" enctype="multipart/form-data"><!--form-->
						<div class="col-xs-12 col-sm-9"><!--col-->

							<div class="form-group">
								<label for="NombreAdmin">Nombre:</label>
								<input type="text" name="name" placeholder="Nombre" class="form-control" id="NombreAdmin" value="{{$user->name}}">
							</div>

							<div class="form-group">
								<label for="ApellidosAdmin">Apellidos:</label>
								<input type="text" name="last_name" placeholder="Apellidos" class="form-control" id="ApellidosAdmin" value="{{$user->last_name}}">
							</div>

							<div class="form-group">
								<label for="CorreoAdmin">Correo:</label>
								<input type="email" name="email" placeholder="@Correo" class="form-control" id="CorreoAdmin" value="{{$user->email}}">
							</div>
							


						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->

							<div class="form-group">
								<label for="AdminRol">Rol:</label>
								<select name="rol" id="AdminRol" class="form-control">
									<option disabled>----</option>
									<option value="1" @if($user->hasRole('usuario')) selected @endif>Usuario</option>
									<option value="2" @if($user->hasRole('premium')) selected @endif>Premium</option>
									<option value="3" @if($user->hasRole('angel')) selected @endif>Angel</option>	
									<option value="4" @if($user->hasRole('editor')) selected @endif>Editor</option>	
									<option value="5" @if($user->hasRole('admin')) selected @endif>Admin</option>									
								</select>
							</div>						

							

							<button class="btn btn-primary" type="submit">
								<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Guardar
							</button>

						</div><!--/col-->
						{{csrf_field()}}
					</form><!--/form-->

						<div class="col-xs-12">
							<button class="btn btn-danger" id="eliminar-cuenta">
								<a href="#">Eliminar cuenta</a>
							</button>
						</div>
						<div class="are-you-sure">
							<div class="box">
								<h4>Esta seguro que desea borrar esta cuenta?</h4>
								<button class="btn btn-warning no-borrar">
									<a href="#">Cancelar</a>
								</button>
								 <button class="btn btn-danger proceder-borrar">
									<a href="#">Eliminar</a>
								</button>
							</div>
						</div>
						<form action="{{route('admin.backend.delete', ['id' => $user->id])}}" method="post" class="hidden-form" id="hidden-form">
							{{csrf_field()}}
						</form>

					</div><!--/row-->		

				</div><!--/tab 1-->

				<!---====================   TAB 2	 -->

				<div class="tab-box" id="tab2"><!--tab 2-->
					
					<div class="row"><!--row-->				
					

						<div class="col-xs-12 col-sm-9"><!--col-->
						<form id="delete-issue-form" method="post" action="{{route('backend.user.deleteissue', ['id' => $user->id])}}">
							<ul class="list-group">
							@foreach($user->revistas as $revista)


							  <li class="list-group-item">
							  	<div class="row">
							  		<div class="col-xs-8">
							  			{{$revista->name}}
							  		</div>
							  		<div class="col-xs-4">
							  			<input type="checkbox" name="borrar[]" value="{{$revista->id}}"> Borrar
							  		</div>
							  	</div>
							  </li>
							  
							
							@endforeach
							</ul>
							{{csrf_field()}}
							<button class="btn btn-warning" type="submit" onclick="return confirm('Seguro que desea borrar las revistas seleccionadas?');">
								Borrar revistas
							</button>				
						</form>
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->
							<form method="post" action="{{route('backend.user.addissue', ['id' => $user->id])}}"><!--form-->
								<div class="form-group">
									<label for="Agregarrevista">Agregar Revista:</label>
									<select multiple class="form-control" name="revista[]" id="AgregarRevista">
									  <option disabled selected>----</option>
										@foreach($revistas as $revista)
											@if(!$user->hasIssue($revista->hash_slug))
											<option value="{{$revista->id}}">{{$revista->name}} / {{$revista->sku}}</option>
											@endif
										@endforeach			
									</select>								
								</div>						
								{{csrf_field()}}

								<button class="btn btn-primary" type="submit">
									<span aria-hidden="true" class="glyphicon glyphicon-floppy-disk"></span> Agregar
								</button>
							
							</form><!--/form-->
						</div><!--/col-->
						


					</div><!--/row-->

				</div><!--/tab 2-->

				<!--========== TAB 3 ===================-->


				<div class="tab-box" id="tab3"><!--tab 3-->
					
					<div class="row"><!--row-->
				
					
						<div class="col-xs-12 col-sm-9"><!--col-->


						<table class="table table-hover"> 
							<thead> 
								<tr> 
									<th>#</th> 
									<th>Codigo de orden</th> 
									<th>Tipo de orden</th> 
									<th>Status</th>									
								</tr> 
							</thead> 
							<tbody> 
								@foreach($user->orders as $order)
								<tr> 
									<th scope="row">{{$loop->iteration}}</th> 
									<td><a href="{{route('backend.order.view', ['id' => $order->id])}}">{{$order->code}}</a></td> 
									<td>{{$order->type}}</td> 
									<td>{{$order->status}}</td> 									
								</tr> 
								@endforeach
								
							  </tbody> 
						  </table>
							

						  	{{csrf_field()}}
						</div><!--/col-->

						<div class="col-xs-12 col-sm-2 col-sm-offset-1"><!--col-->	

						</div><!--/col-->
						
					


					</div><!--/row-->

				</div><!--/tab 3-->


			</div><!--/tabs container-->

			

		</div><!--/container-->

	</div><!--/main-->

	
<script type="text/javascript">
	$(function(){

		$("#eliminar-cuenta").click(function(){
			$(".are-you-sure").fadeIn();
			return false;
		});

		$(".no-borrar").click(function(){
			$(".are-you-sure").fadeOut();
			return false;
		});

		$(".proceder-borrar").click(function(){
			$("#hidden-form").submit();
		});

	});
</script>
@endsection