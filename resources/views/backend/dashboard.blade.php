@extends('backend.layouts.main')

@section('content')

	<div class="main"><!--main-->
		
		<div class="container"><!--container-->
			
			<h1>Bienvenido al panel de administración</h1>

			<div class="m-top-60"><!--v top 60-->
				
				<div class="row"><!--row-->
				
					<div class="col-xs-12 col-sm-6"><!--col-->
						<div class="panel panel-primary"> <!--panel-->
							<div class="panel-heading"> 
								<h3 class="panel-title">Ultimas Ordenes de Compra</h3> 
							</div> 
							<div class="panel-body"> 
								<ul class="list-group">
								@foreach($orders as $order)
								  <li class="list-group-item">
								  	#<a href="{{route('backend.order.view', ['id' => $order->id])}}">{{$order->code}}</a> | {{$order->status}} | S/.{{$order->amount}}
								  </li>
								@endforeach  
								</ul>
							</div> 
						</div><!--/panel-->
					</div><!--/col-->


					<div class="col-xs-12 col-sm-6"><!--col-->
						<div class="panel panel-primary"> <!--panel-->
							<div class="panel-heading"> 
								<h3 class="panel-title">Ultimos Usuarios Registrados</h3> 
							</div> 
							<div class="panel-body"> 
								<ul class="list-group">
								@foreach($users as $user)
								  <li class="list-group-item">
								  	<a href="{{route('backend.user.detail', ['id' => $user->id])}}">{{$user->name}} {{$user->last_name}}</a> | {{$user->email}}
								  </li>
								@endforeach  
								</ul>
							</div> 
						</div><!--/panel-->
					</div><!--/col-->

					<div class="clearfix"></div>

					<div class="col-xs-12 col-sm-6"><!--col-->
						<div class="panel panel-primary"> <!--panel-->
							<div class="panel-heading"> 
								<h3 class="panel-title">Ultimos Tickets de Soporte</h3> 
							</div> 
							<div class="panel-body"> 
								<ul class="list-group">
								@foreach($tickets as $ticket)
								  <li class="list-group-item">
								  	<a href="{{route('backend.view.ticket', ['id' => $ticket->id])}}">{{$ticket->asunto}}</a>
								  	<br>
								  	@if(!empty($ticket->user))
								  	<strong>Usuario: </strong>{{$ticket->user->name}} {{$ticket->user->last_name}}
								  	@else
								  	<strong>Usuario: </strong>
								  	@endif
								  </li>
								@endforeach  
								</ul>
							</div> 
						</div><!--/panel-->
					</div><!--/col-->

				</div><!--/row-->

			</div><!--v top 60-->

		</div><!--/container-->

	</div><!--/main-->

@endsection