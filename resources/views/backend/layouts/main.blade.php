<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Angeles" />
<meta name="keywords" content="@yield('keywords')" />
<meta name="author" content="Basecamp" />
@yield('page-metas')
<title>Revista Ángeles - Traemos el paraiso</title>
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/backend.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('css/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ asset('js/jquery-1.11.0.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/tinymce/js/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
@yield('page-header-scripts')
</head>

<body>
	<div class="main-menu"><!--main menu-->
		<ul>
			<li>
				<a href="#">Usuario</a>
			</li>
			<li>
				<a href="{{route('backend.dashboard')}}"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Dashboard</a>
			</li>
			<li>
				<a href="{{route('backend.homepage')}}"><span class="glyphicon glyphicon-home"></span> Homepage</a>
			</li>
			<li>
				<a href="{{route('backend.modelos')}}"><span class="glyphicon glyphicon-apple" aria-hidden="true"></span> Modelos</a>
			</li>
			<li>
				<a href="{{route('backend.blog')}}"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Blog</a>
			</li>
			<li>
				<a href="{{route('backend.categorias')}}"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Categorias</a>
			</li>
			
			@if(Auth::user()->hasRole(['admin']))
			<li>
				<a href="{{route('backend.show.usuarios')}}"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Usuarios</a>
			</li>
			<li>
				<a href="{{route('backend.anuncios')}}"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> Publicidad</a>
			</li>
			<li>
				<a href="{{route('backend.revistas')}}">
					<span class="glyphicon glyphicon-fire" aria-hidden="true"></span> Revistas
				</a>
			</li>
			<li>
				<a href="{{route('backend.ventas')}}">
					<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Ordenes
				</a>
			</li>
			<li>
				<a href="{{route('backend.products.all')}}">
					<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Productos
				</a>
			</li>
			<li>
				<a href="{{route('backend.productos.ventas')}}">
					<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Ordenes de Productos
				</a>
			</li>
			<li>
				<a href="{{route('backend.show.tickets')}}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Support Tickets</a>
			</li>			
			<li>
				<a href="{{route('backend.show.admins')}}"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> Admins</a>
			</li>
			@endif
			<li>
				<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log out</a>
				<form action="{{route('backend.logout')}}" method="post" id="logout-form">{{csrf_field()}}</form>
			</li>
		</ul>
	</div><!--/main menu-->

	@yield('content')

<script type="text/javascript" src="{{asset('js/backend.js')}}"></script>
@yield('page-footer-scripts')
</body>
</html>	