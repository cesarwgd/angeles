@extends('frontend.layouts.main')

@section('content')
	
	<div class="main filler-padding-vertical">
		<h1>Página no encontrada</h1>
		<h5>La página que esta buscando no existe.</h5>
	</div>

@endsection