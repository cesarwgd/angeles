<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //belongs to many relationship user

    public function users()
    {
    	return $this->belongsToMany('App\User');
    }
}
