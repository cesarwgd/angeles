<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    

    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function revistas()
    {
    	return $this->belongsToMany('App\Models\Revista')->withTimestamps();
    }

    // agregar al carrito de compras

    public function placeOrder($data)
    {
    	$this->user_id = $data['user_id'];
    	$this->revista_id = $data['revista_id'];
    	$this->code = $data['user_id'] . $data['revista_id'] . uniqid() . rand(1, 9);
    	$this->type = $data['type'];
    	$this->status = "iniciada";
    	$this->amount = $data['amount'];
    	$this->description = $data['description'];
        $this->country = $data['country'];

        if(isset($data['source'])){
            $this->source = $data['source'];
        }

    	return $this->save();

    }

    public function hasIssueOrdered($user_id, $revista_id)
    {
        $order = $this->where([
                ['user_id', '=', $user_id],
                ['revista_id', '=', $revista_id],
                ['status', '!=', 'cancelada'],
            ])->get();
        
        if(count($order) > 0){
            return true;
        }

        return false;

    }

    // filter in the backend local scope

    public function scopeFilters($query, $filters)
    {
        if (isset($filters['filter-revista'])) {
            $query->whereIn('revista_id', $filters['filter-revista']);
        }
        if (isset($filters['type'])) {
            $query->where('type', $filters['type']);
        }
        if (isset($filters['estado'])) {
            $query->where('status', $filters['estado']);
        }
        if (isset($filters['pais'])) {
            $query->where('country', $filters['pais']);
        }
        if (isset($filters['mes'])) {
            $query->whereMonth('updated_at', $filters['mes']);
        }
        if (isset($filters['anio'])) {
            $query->whereYear('updated_at', $filters['anio']);
        }

        return $query;

    }

    

}
