<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Image;

class Revista extends Model
{
    // belongs to many relationship with user model
    public function users()
    {
    	return $this->belongsToMany('App\User');
    }

    // relationship with files

    public function files()
    {
    	return $this->hasMany('App\Models\File');
    }

    // ORDER 

    public function orders()
    {
        return $this->belongsToMany('App\Models\Order');
    }

    // CREATE REVISTA

    public function createRevista($data)
    {
        $this->name = $data['name'];
        $this->slug = $this->generateSlug($data['name']);
        /*posicion*/
        $total = count($this->all());
        $this->position = $total + 1;
        $this->published = $data['published'];
        $this->sku = $data['sku'];
        $this->type = $data['type'];
        $this->description = $data['description'];
        $this->short_description = $data['short_description'];
        $this->price = $data['price'];
        $this->hash_slug = hash('md5', $data['name'] . uniqid());
        
        $this->video_intriga = $data['video_intriga'];
        $this->meta_title = $data['meta_title'];
        $this->meta_keywords = $data['meta_keywords'];
        $this->meta_description = $data['meta_description'];
        
        // imagen
        if(!empty($data['main_pic'])){            
            $this->storeImage($data['main_pic']);
        }

        return $this->save();
    }

    // ACTUALIZAR REVISTA

    public function updateRevista($data)
    {
        $this->name = $data['name'];
        $this->slug = $data['slug'];
        $this->sku = $data['sku'];
        $this->type = $data['type'];
        $this->description = $data['description'];
        $this->short_description = $data['short_description'];
        $this->price = $data['price'];
        $this->published = $data['published'];
        //$this->hash_slug = hash('md5', $data['slug'] . uniqid());
        
        $this->video_intriga = $data['video_intriga'];
        $this->meta_title = $data['meta_title'];
        $this->meta_keywords = $data['meta_keywords'];
        $this->meta_description = $data['meta_description'];
        
        // imagen
        if(!empty($data['main_pic'])){
            // borramos la antigua imagen
            if(Storage::disk('revista')->exists($this->main_pic)){
                Storage::disk('revista')->delete($this->main_pic);
            }
            //guardamos la nueva imagen
            $this->storeImage($data['main_pic']);

        }

        return $this->update();
    }

    // Store Image
    public function storeImage($image)
    {
        
        $extension = $image->getClientOriginalExtension();
        $nombre = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
           

        $image_hash_name = hash('md5', $nombre) . "-" . time() . "-" . random_int(100, 999) . $nombre;
        $image_full_name = $image_hash_name . "." . $extension;

        $this->main_pic = $image_hash_name;
        $this->extension = $extension;

        // guardamos la revista

        Storage::disk('revista')->put($image_full_name, file_get_contents($image));

        // creamos el thumb

        $destinationPath = public_path('/ang/thumbnail'); // destino donde se guardara el thumb
        $thumb_name = $image_hash_name . "-thumb." . $extension;

        $thumb = Image::make(storage_path('app/revista/'.$image_full_name));

        return $thumb->resize(80, 120, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$thumb_name);

         
    }

    private function generateSlug($name)
    {
        $slug = "";
        $preSlug = str_slug($name, '-');
        
        $check = $this->where('slug', $preSlug)->get();
        // re visamos si ya hay otra revista con el mismo slug ya que el slug se genera del nombre de la revista
        if(count($check) > 0){
            $append = count($check) + 1;
            $slug = $preSlug . "-" . $append;
        }else{
            $slug = $preSlug;
        }

        return $slug;
         
    }

    
}
