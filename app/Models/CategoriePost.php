<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoriePost extends Model
{
    //
    protected $table = 'categorie_posts';

    //Get Parent Name By its ID
    public function getParentName($id)
    {	
    	$categoria = $this->where('id', $id)->first();
    	return $categoria->nombre;
    }
}
