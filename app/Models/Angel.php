<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Angel extends Model
{
    //
    public function hots()
    {
    	return $this->hasMany('App\Models\Hot');
    }

    // CREATE ANGEL

    public function createAngel($data)
    {
        $this->nombre = $data['nombre'];
        $this->descripcion = $data['descripcion'];
        $this->slug = $this->generateSlug($data['nombre']);   
        $this->meta_title = $data['meta_title'];
        $this->meta_keywords = $data['meta_keywords'];
        $this->meta_description = $data['meta_description'];
        
        // imagen
        if(!empty($data['main_pic'])){            
            $this->storeImage($data['main_pic']);
        }

        return $this->save();
    }

    //UPDATE ANGEL
    public function updateAngel($data)
    {
    	$this->nombre = $data['nombre'];
        $this->descripcion = $data['descripcion'];        
        $this->meta_title = $data['meta_title'];
        $this->meta_keywords = $data['meta_keywords'];
        $this->meta_description = $data['meta_description'];
        
        // imagen
    	if(!empty($data['main_pic'])){
    		// borramos la antigua imagen
    		if(Storage::disk('hotmas')->exists($this->main_pic)){
                Storage::disk('hotmas')->delete($this->main_pic);
            }
    		//guardamos la nueva imagen
    		$this->storeImage($data['main_pic']);

    	}

        return $this->update();
    }

    

    // Store Image
    public function storeImage($image)
    {
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = uniqid() . "-" . hash('md5', $main_image_name) . "-" . time() . "-" . random_int(100, 999) . $main_image_name;

        $this->main_pic = $image_full_name;

        return Storage::disk('hotmas')->put($image_full_name, file_get_contents($image));
    }

    
    private function generateSlug($name)
    {
        $slug = "";
        $preSlug = str_slug($name, '-');
        
        $check = $this->where('slug', $preSlug)->get();
        // re visamos si ya hay otra revista con el mismo slug ya que el slug se genera del nombre de la revista
        if(count($check) > 0){
            $append = count($check) + 1;
            $slug = $preSlug . "-" . $append;
        }else{
            $slug = $preSlug;
        }

        return $slug;
         
    }


}
