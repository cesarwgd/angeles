<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Models\CategoriePost;

class Post extends Model
{
    // CREATE POST

    public function createPost($data)
    {
    	$this->titulo = $data['titulo'];
    	$this->slug = $this->generateSlug($data['titulo']);
    	$this->contenido = $data['contenido'];
    	$this->extracto = $data['extracto'];
    	$this->meta_title = $data['meta_title'];
    	$this->meta_keywords = $data['meta_keywords'];
    	$this->meta_description = $data['meta_description'];
    	$this->estado = $data['estado'];
    	$this->categoria = $data['categoria'];
    	
    	// imagen
    	if(!empty($data['main_image'])){    		
    		$this->storeImage($data['main_image']);
    	}

    	return $this->save();
    	
    }

    // UPDATE POST

    public function updatePost($data)
    {
    	$this->titulo = $data['titulo'];

    	if($data['slug'] != $this->slug){
    		$this->slug = $data['slug'];
    	}
    	
    	$this->contenido = $data['contenido'];
    	$this->extracto = $data['extracto'];
    	$this->meta_title = $data['meta_title'];
    	$this->meta_keywords = $data['meta_keywords'];
    	$this->meta_description = $data['meta_description'];
    	$this->estado = $data['estado'];
    	$this->categoria = $data['categoria'];
    	
    	// imagen
    	if(!empty($data['main_image'])){
    		// borramos la antigua imagen
            if(Storage::disk('blog')->exists($this->main_image)){
                Storage::disk('blog')->delete($this->main_image);
            }
    		
    		//guardamos la nueva imagen
    		$this->storeImage($data['main_image']);

    	}

    	return $this->update();
    }

    // Store Image
    public function storeImage($image)
    {
    	$main_image_name = $image->getClientOriginalName();    	

    	$image_full_name = time() . '-' . $main_image_name;

    	$this->main_image = $image_full_name;

    	return Storage::disk('blog')->put($image_full_name, file_get_contents($image));
    }

    private function generateSlug($name)
    {
        $slug = "";
        $preSlug = str_slug($name, '-');
        
        $check = $this->where('slug', $preSlug)->get();
        // re visamos si ya hay otra revista con el mismo slug ya que el slug se genera del nombre de la revista
        if(count($check) > 0){
            $append = count($check) + 1;
            $slug = $preSlug . "-" . $append;
        }else{
            $slug = $preSlug;
        }

        return $slug;
         
    }

    // get post category name

    public function getCategorySlug()
    {
        $categorieSlug = CategoriePost::where('id', $this->categoria)->first();
        return $categorieSlug->slug;
    }

}
