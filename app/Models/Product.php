<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
//use Image;

class Product extends Model
{
    
    // table relationships

    public function users()
    {
    	return $this->belongsToMany('App\User');
    }

    public function productOrders()
    {
        return $this->belongsToMany('App\Models\ProductOrder');
    }

    //
    public function productImages()
    {
    	return $this->hasMany('App\Models\ProductImages');
    }

    // relationship with files

    /*public function files()
    {
    	return $this->hasMany('App\Models\File');
    }*/

    public function createProduct($data){

    	foreach ($data as $key => $value) {
    		// tallas y colores solo los uso en el view pero no tienen q ir en la bd-obviar esto corregir luego
    		if($key != '_token' && $key != "tallas" && $key != "colores"){
    			$this->$key = $value;
    		}            
    	}
        // product slug
        $this->slug = $this->generateSlug($data['name']);

    	return $this->save();
    }

    public function updateProduct($data){

    	foreach ($data as $key => $value) {
    		if($key != '_token' && $key != "tallas" && $key != "colores"){
    			$this->$key = $value;
    		}
    	}

    	return $this->update();
    }

    // store product images

    // Store Image
    public function storeImage($image)
    {
        
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = time() . "-" . random_int(100, 999) . $main_image_name; 

        Storage::disk('products')->put($image_full_name, file_get_contents($image));

        return $image_full_name;

         
    }

    // generate slug

    private function generateSlug($name)
    {
        $slug = "";
        $preSlug = str_slug($name, '-');
        
        $check = $this->where('slug', $preSlug)->get();
        // re visamos si ya hay otra revista con el mismo slug ya que el slug se genera del nombre de la revista
        if(count($check) > 0){
            $append = count($check) + 1;
            $slug = $preSlug . "-" . $append;
        }else{
            $slug = $preSlug;
        }

        return $slug;
         
    }
    

}
