<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Home extends Model
{
	protected $table = "homephotos";
    
    // Store Image
    public function storeImage($image)
    {
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = uniqid() . "-" . hash('md5', $main_image_name) . "-" . random_int(100, 999) . $main_image_name;

        $this->image_name = $image_full_name;

        return Storage::disk('home')->put($image_full_name, file_get_contents($image));
    }

    // Store Video Cover
    public function storeVideoCover($image)
    {
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = uniqid() . "-" . hash('md5', $main_image_name) . "-" . random_int(100, 999) . $main_image_name;

        $this->video_cover = $image_full_name;

        return Storage::disk('home')->put($image_full_name, file_get_contents($image));
    }

    public function storeVideo($video)
    {
    	$main_video_name = $video->getClientOriginalName();        

        $video_full_name = uniqid() . "-" . hash('md5', $main_video_name) . "-" . random_int(100, 999) . $main_video_name;

        $this->video_name = $video_full_name;

        return Storage::disk('home')->put($video_full_name, file_get_contents($video));
    }
}
