<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    protected $table = "product_order";

    // relationships

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

     public function products()
    {
    	return $this->belongsToMany('App\Models\Product')->withTimestamps();
    }

    // agregar al carrito de compras

    public function placeOrder($data)
    {
    	$this->user_id = $data['user_id'];
    	$this->product_id = $data['product_id'];
    	$this->code = $data['user_id'] . $data['product_id'] . uniqid() . rand(1, 9);
    	//$this->type = $data['type'];
    	$this->status = "iniciada";
    	$this->quantity = $data['quantity'];
    	$this->amount = $data['amount'];
        $this->delivery_amount = $data['delivery_amount'];
    	$this->description = $data['description'];
        $this->country = $data['country'];
        if(isset($data['talla'])){
            $this->talla = $data['talla'];
        }
        if(isset($data['color'])){
            $this->color = $data['color'];
        }

    	return $this->save();

    }

    /*public function hasIssueOrdered($user_id, $revista_id)
    {
        $order = $this->where([
                ['user_id', '=', $user_id],
                ['revista_id', '=', $revista_id],
                ['status', '!=', 'cancelada'],
            ])->get();
        
        if(count($order) > 0){
            return true;
        }

        return false;

    }*/

    public function scopeFilters($query, $filters)
    {
        if (isset($filters['filter-producto'])) {
            $query->whereIn('product_id', $filters['filter-producto']);
        }
        
        if (isset($filters['estado'])) {
            $query->where('status', $filters['estado']);
        }
        if (isset($filters['pais'])) {
            $query->where('country', $filters['pais']);
        }
        if (isset($filters['mes'])) {
            $query->whereMonth('updated_at', $filters['mes']);
        }
        if (isset($filters['anio'])) {
            $query->whereYear('updated_at', $filters['anio']);
        }

        return $query;

    }


}
