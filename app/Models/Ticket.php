<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    //
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // crear ticket
    public function createTicket($data)
    {
    	$this->user_id = $data['user_id'];
        $this->asunto = $data['asunto'];
    	$this->tipo = $data['tipo'];
    	$this->mensaje = $data['detalle'];
    	$this->initial = 1;
    	//$this->channel_id = $data['user_id'];
    	$this->from_user = $data['user_id'];

    	return $this->save(); 
    }

    public function userReplyTicket($data)
    {
    	$this->user_id = $data['user_id'];
        $this->asunto = $data['asunto'];   	
    	$this->mensaje = $data['detalle'];
    	$this->initial = 2;
    	$this->channel_id = $data['ticket_id'];
    	$this->from_user = $data['user_id'];

    	return $this->save(); 
    }

    public function adminReplyTicket($data)
    {
        $this->user_id = $data['user_id'];
        $this->asunto = $data['asunto'];          
        $this->mensaje = $data['detalle'];
        $this->initial = 2;
        $this->channel_id = $data['ticket_id'];
        $this->from_user = $data['user_id'];
        $this->replied_to = $data['replied_to'];

        return $this->save(); 
    }

}
