<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Hot extends Model
{
    //
    public function angels()
    {
    	return $this->belongsTo('App\Models\Angel');
    }

       

    // Store Image
    public function uploadImage($image)
    {
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = time() . "-" . random_int(100, 999) . $main_image_name; 

        Storage::disk('hotmas')->put($image_full_name, file_get_contents($image));

        return $image_full_name;

         
    }

}
