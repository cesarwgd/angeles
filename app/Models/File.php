<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    //
    public function revistas()
    {
    	return $this->belongsTo('App\Models\Revista');
    }

    // relationship woth products

    //
   /* public function products()
    {
        return $this->belongsTo('App\Models\Product');
    }*/

    // Store Image
    public function uploadImage($image)
    {

        $extension = $image->getClientOriginalExtension();
        $nombre = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);    

        $image_hash_name = uniqid() . "-" . time() . "-" . random_int(100, 999) . "-" . $nombre;
        $image_full_name = $image_hash_name . "." . $extension; 

        Storage::disk('revista')->put($image_full_name, file_get_contents($image));

        $image_data = [
            'img_nombre' => $image_hash_name,
            'img_extension' => $extension,
        ];

        return $image_data;

         
    }

}
