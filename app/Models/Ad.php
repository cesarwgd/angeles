<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Ad extends Model
{
    // Store Image
    public function storeImage($image)
    {
        $main_image_name = $image->getClientOriginalName();        

        $image_full_name = uniqid() . "-" . hash('md5', $main_image_name) . "-" . random_int(100, 999) . $main_image_name;

        $this->media_name = $image_full_name;

        return Storage::disk('home')->put($image_full_name, file_get_contents($image));
    }
}
