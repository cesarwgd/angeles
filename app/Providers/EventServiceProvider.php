<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewUserRegistered' => [
            'App\Listeners\AttachRole',
            'App\Listeners\SendWelcomeMail',
        ],

        'App\Events\OrderWithBankPayment' => [
            'App\Listeners\SendOrderNotification',            
        ],

        'App\Events\PaymentSubmitted' => [
            'App\Listeners\UpdateUserIssue',
            'App\Listeners\SendReceipt',            
        ],

        'App\Events\ProductPaymentSubmitted' => [            
            'App\Listeners\SendProductReceipt',            
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
