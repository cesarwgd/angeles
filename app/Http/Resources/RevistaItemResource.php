<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RevistaItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,            
            'sku' => $this->sku,
            'description' => $this->description,
            'short_description' => $this->short_description,
            'price' => $this->price,
            'main_pic' => $this->main_pic,
            'extension' => $this->extension,
            'video_intriga' => $this->video_intriga,            
        ];
    }
}
