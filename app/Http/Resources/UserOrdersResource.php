<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserOrdersResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'code' => $this->code,
            'type' => $this->type,            
            'status' => $this->status,
            'amount' => $this->amount,
            'description' => $this->description,
            'updated_at' => $this->updated_at,            
        ];
    }
}
