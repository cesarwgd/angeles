<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\ProductImages;

class ProductosResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $files = $this->productImages()->get();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'price' => $this->price,
            'sku' => $this->sku,
            'description' => $this->description,
            'stock' => $this->stock,
            'size' => $this->size,
            'color' => $this->color,
            'dimensions' => $this->dimensions,
            'delivery_price' => $this->delivery_price,       
            'files' => $files
        ];

    }
}
