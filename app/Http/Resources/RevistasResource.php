<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\File;

class RevistasResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        // obtenemos los thumbmails de la revista para mandarlos junto con los demas datos
        $thumbnails = $this->files()->where('class', 'preview')->get();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'position' => $this->position,
            'sku' => $this->sku,
            'description' => $this->description,
            'short_description' => $this->short_description,
            'price' => $this->price,
            'main_pic' => $this->main_pic,
            'extension' => $this->extension,
            'hash_slug' => $this->hash_slug,
            'video_intriga' => $this->video_intriga,
            'thumbails' => $thumbnails
        ];
    }
}
