<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\UserRevistasResource;
use App\Http\Resources\UserOrdersResource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'revistas' => UserRevistasResource::collection($this->revistas),
            'orders' => UserOrdersResource::collection($this->orders),
            /*'relationships' => [
                'revistas' => UserRevistasResource::collection($this->revistas),
                'orders' => UserOrdersResource::collection($this->orders),
            ],*/
        ];
    }
}
