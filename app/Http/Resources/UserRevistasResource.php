<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserRevistasResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,            
            'sku' => $this->sku,     
            'main_pic' => $this->main_pic,
            'extension' => $this->extension,
            'hashSlug' => $this->hash_slug          
        ];
    }
}
