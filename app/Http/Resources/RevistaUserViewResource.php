<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\File;

class RevistaUserViewResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,             
            'main_pic' => $this->main_pic,
            'extension' => $this->extension,            
            'files' => RevistaFilesResource::collection($this->files)
        ];
    }
}
