<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminData;
use App\Http\Requests\UpdateAdminData;
use App\Http\Requests\DeleteUserCms;
use App\Http\Requests\CmsAdminGeneral;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Role;
use App\Models\Revista;
use App\Models\Order;
use App\Models\Ticket;
use App\Mail\IssueAddedToUser;

class General extends Controller
{
    /*
    *
    *   DASHBOARD
    *
    */

    public function dashboard()
    {
        $data['orders'] = Order::orderBy('id', 'desc')->take(10)->get();
        $data['users'] = User::orderBy('id', 'desc')->take(10)->get();
        $data['tickets'] = Ticket::where('estado', 'abierto')->where('initial', 1)->orderBy('id', 'desc')->get();        

    	return view('backend.dashboard', $data);
    }

    
    /*
    *
    *   LISTA DE ADMINS
    *
    */

    public function adminsList()
    {
        $data['users'] = User::whereHas('roles', function($r){
                            $r->whereIn('nombre', ['angel', 'editor', 'admin'] );
                        })->get();
        $data['roles'] = Role::all();

        return view('backend.admins', $data);
    }

    public function adminPostNew(CreateAdminData $request)
    {
        $user = new User();
        $role = Role::where('id', $request['role'])->first();

        $user->name = $request['name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->save();
        $user->roles()->attach($role);

        $exito = "Usuario creado exitosamente";

        return redirect()->back()->with(['exito' => $exito]);

    }

    public function adminsViewDetail($id)
    {
        $user = User::find($id);
        return view('backend.crud_users.admin_edit', ['user' => $user]);
    }

    /*
    *
    *   ACTUALIZAR ADMINS Y USUARIOS
    *
    */

    public function adminUpdateInfo(UpdateAdminData $request, $id)
    {
        $user = User::find($id);

        $role = DB::table('role_user')->where('user_id', $id)->first();

        $user->name = $request['name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];

        if(!empty($request['password'])){
            $user->password = bcrypt($request['password']);
        }   
        if(!empty($request['profile_pic'])){            
            $image_name = $user->storeImage($request['profile_pic'], $user->id);
            $user->profile_pic = $image_name;
        }

        $user->update();

        // actualizamos los roles si es que los cambio

        if($role->role_id != $request['rol']){
            $role->role_id = $request['rol'];
            DB::table('role_user')->where('user_id', $id)->update(['role_id' => $request['rol']]);
        }

        $exito = "Información actualizada correctamente";

        return redirect()->back()->with(['exito' => $exito]);
        

    }
    
    /*
    *
    *   BORRAR ADMINS Y USUARIOS
    *
    */

    public function deleteAdmin(DeleteUserCms $request, $id)
    {
        $user = User::find($id);

        $admin = $user->hasAnyRole(['admin', 'editor', 'angel']);
        
        if(Auth::user()->id == $id){
            $user->delete();
            return Auth::logout();
        }

        $user->delete();
        $exito = "Usuario eliminado correctamente";

        if($admin){
            return redirect()->route('backend.show.admins')->with(['exito' => $exito]);
        }

        return redirect()->route('backend.show.usuarios')->with(['exito' => $exito]);
    }


    /*
    *
    *   LISTA DE USUARIOS
    *
    */

    public function usersList()
    {
        $users = User::paginate(20);
        return view('backend.usuarios', ['users' => $users]);
    }

    public function userDetail($id)
    {
        $data['user'] = User::find($id);

        // si el usuario ya no existe
        if(!$data['user']){
            return redirect()->back()->with(['no_existe' => 'El usuario no existe en la BD']);
        }

        $data['revistas'] = Revista::all();
        $data['orders'] = Order::all();            

        return view('backend.crud_users.user_detail', $data);
    }

    // USER SEARCH

    public function userSearch(Request $request)
    {
        $user = User::where('email', $request['email'])->first();

        if(!$user){
            $error = "No se encontró ningún usuario con ese correo";
            return view('backend.crud_users.search_result', ['user' => null, 'error' => $error]);

        }

        return view('backend.crud_users.search_result', ['user' => $user]);
    }

    /*
    *
    *   AGREGAR REVISTA AL USUARIO
    *
    */

    public function addIssueUser(CmsAdminGeneral $request, $id)
    {
        if(empty($request['revista'])){
            $falta = "Debe elegir por lo menos una revista para agregar al usuario";
            return redirect()->back()->with(['falta' => $falta]);
        }

        $user = User::find($id);
        $revistas = Revista::whereIn('id', $request['revista'])->get();
        // seteamos el array donde vamos a meter la info del producto agregado
        
        $mail_revistas = [];

        foreach($revistas as $revista){
            $user->revistas()->attach($revista);
            $mail_revistas[] = $revista->name;
        }

        // metemos la info a la variable que ira en el mail
        $data = [
            'user_name' => $user->name,
            'user_last_name' => $user->last_name,
            'revistas' => $mail_revistas,
        ];

        // enviamos el correo para notificarle al gil
        Mail::to($user->email)->send(new IssueAddedToUser($data));

        $exito = "Revistas agregadas con exito";

        return redirect()->back()->with(['exito' => $exito]);
        

    }

    /*
    *
    *   BORRAR REVISTA DEL USUARIO
    *
    */

    public function deleteIssueUser(CmsAdminGeneral $request, $id)
    {
        if(empty($request['borrar'])){
            $falta = "Debe elegir por lo menos una revista para borrar";
            return redirect()->back()->with(['falta' => $falta]);
        }

        $user = User::find($id);
        $revistas = Revista::whereIn('id', $request['borrar'])->get();

        // Hacemos detach a cada una de las revistas seleccionadas

        foreach($revistas as $revista){
            $user->revistas()->detach($revista);
            // En la tabla Orders cambiamos el estado de la orden a CANCELADA
            $order = Order::where('user_id', $user->id)->where('revista_id', $revista->id)->first();
            if($order != null){
                $order->status = "cancelada";
                $order->update();
            }
        }


        $exito = "Revistas eliminadas con exito";

        return redirect()->back()->with(['exito' => $exito]);
        
    }

    /*
    *
    *   EXPORTAR USUARIOS A UN CSV
    *
    */

    public function exportarUsuariosCsv()
    {
        $usuarios = User::whereHas('roles', function($query){
            $query->where('nombre', '!=', 'admin');
        })->orderBy('id', 'desc')->get();

        $filename = "docotros/usuarios_exportados.csv"; // location and name of the file stored in the server
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array(
            'Nombre', 
            'Apellidos',            
            'Email'
            )
        );

        foreach($usuarios as $user) {
            fputcsv($handle, array(
                $user['name'], 
                $user['last_name'],                 
                $user['email']
                )
            );
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        $name_file = 'usuarios_exportados-' . date('d-m-Y') . ".csv";

        return response()->download($filename, $name_file, $headers);
    }



}
