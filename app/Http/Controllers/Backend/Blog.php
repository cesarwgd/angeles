<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogPost;
use App\Http\Requests\UpdateBlogPost;
use App\Http\Requests\DeleteBlogPost;
use App\Models\Post;
use App\Models\CategoriePost;

class Blog extends Controller
{
    //
    public function all()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(20);
    	return view('backend.blog', ['posts' => $posts]);
    }

    // search result

    public function searchResult(Request $request)
    {
        $terms = explode(' ', $request['titulo']);

        $posts = Post::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('titulo', 'LIKE', '%'.$term.'%');
                    }
                })->get();

        if(count($posts) == 0){
            $error = "No se encontró ningún artículo con ese nombre";
            return view('backend.crud_blog.search_result', ['posts' => null, 'error' => $error]);

        }

        return view('backend.crud_blog.search_result', ['posts' => $posts]);
    }


    // CREATE POST

    public function createPostView()
    {
        $categorias = CategoriePost::all();
    	return view('backend.crud_blog.create', ['categories' => $categorias]);
    }

    public function createPostSave(StoreBlogPost $request)
    {
        $post = new Post();
        $post->createPost($request->all());   
        $exito = "Nota creada exitosamente";     

        return redirect()->route('backend.blog.update', ['id' => $post->id])->with(['exito' => $exito]);
    }


    // EDIT POST

    public function editPostView($id)
    {
        $data['categories'] = CategoriePost::all();
        $data['post'] = Post::find($id);

        return view('backend.crud_blog.edit', $data);

    }

    public function editPostSave(UpdateBlogPost $request, $id)
    {
        $post = Post::find($id);
        $post->updatePost($request->all());        
        $exito = "Nota actualizada exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    // Delete Post

    public function deletePost(DeleteBlogPost $request, $id)
    {
        $post = Post::find($id);
        $imagen = $post->main_image;

        $post->delete();
        // borramos su imagen de portada
        if(Storage::disk('blog')->exists($imagen)){
            Storage::disk('blog')->delete($imagen);
        }

        $exito = "Post borrado con éxito";

        return redirect()->route('backend.blog')->with(['exito' => $exito]);
    }
}
