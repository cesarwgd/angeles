<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategorieModelo;
use App\Models\CategoriePost;

class Categories extends Controller
{
    //
	public function categories()
	{
		return view('backend.categorias');
	}


	// MODELS

    public function allModelCategories()
    {
        $categorias = CategorieModelo::all();
    	return view('backend.categorias_modelos', ['categorias' => $categorias]);
    }

    public function postModelCategories(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'slug' => 'required|alpha_dash|unique:categorie_modelos,slug'
        ]);

        $categoria = new CategorieModelo();
        $categoria->nombre = $request['nombre'];
        $categoria->slug = $request['slug'];

        if($request->has('parent')){
            $categoria->parent = $request['parent'];
        }
        if($request->has('descripcion')){
            $categoria->descripcion = $request['descripcion'];
        }

        $categoria->save();
        $exito = "Categoría creada exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function viewModelCategorie($id)
    {
        $data['categoria'] = CategorieModelo::findOrFail($id);
        $data['categorias'] = CategorieModelo::all();

        return view('backend.categorias_modelos_view', $data);
    }

    public function updateModelCategorie(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'slug' => "required|alpha_dash|unique:categorie_modelos,slug,$id"
        ]);

        $categoria = CategorieModelo::find($id);
        $categoria->nombre = $request['nombre'];
        $categoria->slug = $request['slug'];

        if($request->has('parent')){
            $categoria->parent = $request['parent'];
        }
        if($request->has('descripcion')){
            $categoria->descripcion = $request['descripcion'];
        }

        $categoria->update();
        $exito = "Categoría actualizada exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function deleteModelCategorie($id)
    {
        $categoria = CategorieModelo::find($id);
        // actualizar los padres de los hijos en caso los haya - ver despues cuando usen esto de mejorar este proceso como lo que se hizo en acc
        $children = CategorieModelo::where('parent', $id)->get();
        if(count($children) > 0){
            foreach($children as $child){
                $child->parent = null;
                $child->update();
            }
        }
        // borrar la categoria
        $categoria->delete();
        $exito = "Categoría borrada exitosamente";

        return redirect()->route('backend.model.categorias')->with(['exito' => $exito]);
    }


    // BLOG

    public function allBlogCategories()
    {
        $categorias = CategoriePost::all();

    	return view('backend.categorias_blog', ['categorias' => $categorias]);
    }

    public function postBlogCategories(Request $request)
    {
    	$this->validate($request, [
            'nombre' => 'required|max:100',
            'slug' => 'required|alpha_dash|unique:categorie_posts,slug'
        ]);

        $categoria = new CategoriePost();
        $categoria->nombre = $request['nombre'];
        $categoria->slug = $request['slug'];

        if($request->has('parent')){
            $categoria->parent = $request['parent'];
        }
        if($request->has('descripcion')){
            $categoria->descripcion = $request['descripcion'];
        }

        $categoria->save();
        $exito = "Categoría creada exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function viewBlogCategorie($id)
    {
        $data['categoria'] = CategoriePost::findOrFail($id);
        $data['categorias'] = CategoriePost::all();

        return view('backend.categorias_blog_view', $data);
    }

    public function updateBlogCategorie(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:100',
            'slug' => "required|alpha_dash|unique:categorie_posts,slug,$id"
        ]);

        $categoria = CategoriePost::find($id);
        $categoria->nombre = $request['nombre'];
        $categoria->slug = $request['slug'];

        if($request->has('parent')){
            $categoria->parent = $request['parent'];
        }
        if($request->has('descripcion')){
            $categoria->descripcion = $request['descripcion'];
        }

        $categoria->update();
        $exito = "Categoría actualizada exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }



    public function deleteBlogCategorie($id)
    {
        $categoria = CategoriePost::find($id);
        // actualizar los padres de los hijos en caso los haya - ver despues cuando usen esto de mejorar este proceso como lo que se hizo en acc
        $children = CategoriePost::where('parent', $id)->get();
        if(count($children) > 0){
            foreach($children as $child){
                $child->parent = null;
                $child->update();
            }
        }
        // borrar la categoria
        $categoria->delete();
        $exito = "Categoría borrada exitosamente";

        return redirect()->route('backend.blog.categorias')->with(['exito' => $exito]);
    }


}
