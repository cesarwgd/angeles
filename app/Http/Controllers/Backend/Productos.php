<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImages;
use Illuminate\Support\Facades\Storage;

class Productos extends Controller
{
    // view all products

    public function viewAllProducts()
    {
    	$products = Product::paginate(20);
    	return view('backend.products', ['products' => $products]);
    }


    // product page detail

    public function viewProductDetail($id)
    {	
    	$product = Product::findOrFail($id);
    	return view('backend.crud_products.update', ['product' => $product]);
    }

    // create new product
    public function createNewProduct(){
    	return view('backend.crud_products.create');

    }

    public function saveNewProduct(Request $request)
    {

    	$this->validate($request, [
    		'name' => 'required|max:191',
    		'size' => 'max:250',
    		'color' => 'max:191',
    		'price' => 'required|max:20',
    		'dimensions' => 'max:191',
    		'delivery_price' => 'max:20'
    		]);

    	$product = new Product();
    	$product->createProduct($request->all());

    	return redirect()->route('backend.product.images', ['id' => $product->id])->with(['product' => $product]);
    }

    // product images

    public function productImages($id)
    {

    	$product = Product::findOrFail($id);

    	return view('backend.crud_products.files', ['product' => $product]);
    }

    // SUBIR IMAGENES POR MEDIO DE DROPZONE AJAX

    public function productImageDropZoneFiles(Request $request, $id)
    {
        // vemos si es que ya hay imagenes para esta modelo
        $previous = ProductImages::where('product_id', $id)->get();

        if($previous != null){
            // si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
            $n = count($previous) + 1;
        }else{
            // si no hay empieza desde 1
            $n = 1;
        }
       

        if(!empty($request->file('file'))){
            foreach($request->file('file') as $image){
                $file = new ProductImages();
                $save = $file->uploadImage($image);

                $file->product_id = $id;
                $file->name = $save;                
                $file->position = $n;
                $file->save();
                
                $n++;    
            }

            $exito = "Imagenes guardadas exitosamente | Por favor refresque la página";
           
            return response()->json($exito, 200);
        }

        
        $exito = "Debe elegir imagenes";

        return response()->json($exito, 500);
    }

    /*
    *
    *   BORRAR IMAGENES DE LA REVISTA POR MEDIO DE AJAX
    *
    */

    public function deleteImage(Request $request, $id)
    {
        //1  ubicar el objeto y obtener su posicion y la revista al que pertenece y nombre de imagen
        $file = ProductImages::find($id);
        $position = $file->position;
        $producto = $file->product_id;
        $nombre = $file->name;

        // 2 borrar el objeto y borrar la imagen de la carpeta tmb
        $file->delete();
        if(Storage::disk('products')->exists($nombre)){
            Storage::disk('products')->delete($nombre);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $files = ProductImages::where('product_id', $producto)->where('position', '>', $position)->get();
        $data = [];
        foreach($files as $file){
            $current = $file->position;
            $file->position = $current - 1;
            $file->update();
            $data[] = [
                'id' => $file->id,
                'position' => $file->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }

    /*
    *
    *   GUARDAR CAMBIOS DE POSICION Y DE CLASE DE IMAGEN VIA AJAX
    *
    */

    public function productUpdateImagePosition(Request $request)
    {        

        $position = $request['positions'];
        $ids = $request['ids'];
        //$clase = $request['clases'];

        $n = 0;

        if(count($ids) == 0){
            return response()->json("No hay imagenes para actualizar", 200);
        }

        while($n < count($ids)){
            $file = ProductImages::find($ids[$n]);
            $file->position = $position[$n];
            //$file->class = $clase[$n];
            $file->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    

    public function updateProduct(Request $request, $id)
    {

    	$this->validate($request, [
    		'name' => 'required|max:191',
    		'size' => 'max:250',
    		'color' => 'max:191',
    		'price' => 'required|max:20',
    		'dimensions' => 'max:191',
    		'delivery_price' => 'max:20',
    		'slug' => 'required|alpha_dash|max:250|unique:products,slug,'.$id,
    		]);

    	$product = Product::findOrFail($id);
    	$product->updateProduct($request->all());

    	return redirect()->back()->with(['exito' => 'Producto actualizdo correctamente']);

    }

    public function deleteProduct($id)
    {
    	$product = Product::find($id);
        //$revistas = Revista::where('position', '>', $revista->position)->get();
        //$main_pic = $revista->main_pic;
        // obtenemos la coleccion de files asociados con la revista
        $files = ProductImages::where('product_id', $id)->get();
        // Borramos la revista y su imagen de portada        
        $product->delete();
        /*if(Storage::disk('products')->exists($main_pic)){
            Storage::disk('products')->delete($main_pic);
        }*/
        // Borramos los archivos de la revista
        foreach($files as $file){
            if(Storage::disk('products')->exists($file->name)){
                Storage::disk('products')->delete($file->name);
            }
            $file->delete();
        }   
        /*atualizamos las posiciones*/
        /*if(count($revistas) > 0){
            foreach($revistas as $rev){
                $new_pos = $rev->position - 1;
                $rev->position = $new_pos;
                $rev->update();
            }
        }*/
        
        return response()->json();
    }

    public function searchResult(Request $request)
    {
        $terms = explode(' ', $request['sku']);

        $products = Product::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('sku', 'LIKE', '%'.$term.'%');
                    }
                })->get();

        if(count($products) == 0){
            $error = "No se encontró ningun producto con ese SKU";
            return view('backend.crud_products.search_result', ['products' => null, 'error' => $error]);

        }

        return view('backend.crud_products.search_result', ['products' => $products]);
    } 



}
