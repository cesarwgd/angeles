<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Banner;
use App\Models\Home;

class Homepage extends Controller
{
    //
    public function all()
    {
    	return view('backend.homepage');
    }

    // BANNERS

    public function homeBanners()
    {
        $banners = Banner::orderBy('position', 'asc')->get();

        if(count($banners) != 0){
            // si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
            $n = count($banners);
        }else{
            // si no hay empieza desde 1
            $n = 0;
        }

        return view('backend.home_banners', ['banners' => $banners, 'cantidad' => $n]);
    }

    public function postHomeBanners(Request $request)
    {

        if(!$request->has('type')){
            return redirect()->back()->with(['exito' => 'Debe crear por lo menos un banner']);
        }

        $this->validate($request, [
            'type.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
            'video_file.*' => 'sometimes|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4|max:4000',
            'video_cover.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
        ]);

        $n = 0;  

        // Asignar la posicion dependiendo de cuantos objetos hay en la tabla
        $all = count(Banner::all());
        if($all > 0){
            $p = $all + 1;
        }else{
            $p = 1;
        }       

        foreach($request['type'] as $a){

            $banner = new Banner();

            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                $banner->storeImage($request['image_name'][$n]);
               } 
            }            

            // video
            if($request->has('video_file')){
               if(isset($request['video_file'][$n])){                
                //Guardar la nueva
                $banner->storeVideo($request['video_file'][$n]);
               }  
            }
            // video cover
            if($request->has('video_cover')){
               if(isset($request['video_cover'][$n])){                
                //Guardar la nueva
                $banner->storeVideoCover($request['video_cover'][$n]);
               }  
            }
            // si es que tiene embed    
            if(!empty($request['video_name'][$n])){
                $banner->video_name = $request['video_name'][$n];
            }    

            $banner->type = $request['type'][$n];
            $banner->position = $p;
            $banner->description = $request['description'][$n];
            $banner->url = $request['url'][$n];      

            $banner->save();     
           
            $n++;
            $p++;
        }

       $exito = "Banners creados exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
        

    }

    public function updateHomeBanners(Request $request)
    {
        // Validar
        $this->validate($request, [
            'type.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
            'video_file.*' => 'sometimes|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4|max:4000',
            'video_cover.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
        ]);

        $n = 0;

        // ACTUALIZAR CADA BANNER

        while($n < count($request['id'])){
            $banner = Banner::findOrFail($request['id'][$n]);
            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($banner->image_name)){
                    Storage::disk('home')->delete($banner->image_name);
                }
                //Guardar la nueva
                $banner->storeImage($request['image_name'][$n]);
               } 
            }
            // video
            if($request->has('video_file')){
               if(isset($request['video_file'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($banner->video_name)){
                    Storage::disk('home')->delete($banner->video_name);
                }
                //Guardar la nueva
                $banner->storeVideo($request['video_file'][$n]);
               }  
            }
            // video cover
            if($request->has('video_cover')){
               if(isset($request['video_cover'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($banner->video_cover)){
                    Storage::disk('home')->delete($banner->video_cover);
                }
                //Guardar la nueva
                $banner->storeVideoCover($request['video_cover'][$n]);
               }  
            }
            // si es que tiene embed    
            if(!empty($request['video_name'][$n])){
                $banner->video_name = $request['video_name'][$n];
            }    
            

            $banner->type = $request['type'][$n];
            $banner->position = $request['position'][$n];
            $banner->description = $request['description'][$n];
            $banner->url = $request['url'][$n];
            $banner->update();

            $n++;
        }

        $exito = "Banners actualizados correctamente";

        return redirect()->back()->with(['exito' => $exito]);

    }

    // actualizar solo las posiciones via ajax
    public function updateHomePositions(Request $request)
    {
        $position = $request['positions'];
        $ids = $request['ids'];        

        $n = 0;

        if(count($ids) == 0){
            return response()->json("No hay imagenes para actualizar", 200);
        }

        while($n < count($ids)){
            $banner = Banner::find($ids[$n]);
            $banner->position = $position[$n];            
            $banner->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    public function deleteHomeBanner(Request $request)
    {
        //1  ubicar el objeto y obtener su posicion
        $banner = Banner::find($request['id']);
        $position = $banner->position;        
        $image = $banner->image_name;
        $video = $banner->video_name;
        $video_cover = $banner->video_cover;

        // 2 borrar el objeto y borrar la imagen de la carpeta tmb
        $banner->delete();
        if(Storage::disk('home')->exists($image)){
            Storage::disk('home')->delete($image);
        }
        if(Storage::disk('home')->exists($video)){
            Storage::disk('home')->delete($video);
        }
        if(Storage::disk('home')->exists($video_cover)){
            Storage::disk('home')->delete($video_cover);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $banners = Banner::where('position', '>', $position)->get();
        $data = [];
        foreach($banners as $ban){
            $current = $ban->position;
            $ban->position = $current - 1;
            $ban->update();
            $data[] = [
                'id' => $ban->id,
                'position' => $ban->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }

    /*
    *
    *   HOME FEATURED
    *
    *
    */

    public function homeFeatured()
    {
        $boxes = Home::orderBy('position', 'asc')->get();

        if(count($boxes) != 0){
            // si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
            $n = count($boxes);
        }else{
            // si no hay empieza desde 1
            $n = 0;
        }

        return view('backend.home_featured', ['boxes' => $boxes, 'cantidad' => $n]);
    }

    public function postHomeFeatured(Request $request)
    {
        if(!$request->has('type')){
            return redirect()->back()->with(['exito' => 'Debe crear por lo menos un banner']);
        }

        $this->validate($request, [
            'type.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
            'video_file.*' => 'sometimes|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4|max:4000',
            'video_cover.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
        ]);

        $n = 0; 

        // Asignar la posicion dependiendo de cuantos objetos hay en la tabla
        $all = count(Home::all());
        if($all > 0){
            $p = $all + 1;
        }else{
            $p = 1;
        }       

        foreach($request['type'] as $a){

            $box = new Home();

            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                $box->storeImage($request['image_name'][$n]);
               } 
            }     

            // video

            if($request->has('video_file')){
               if(isset($request['video_file'][$n])){
                $box->storeVideo($request['video_file'][$n]);
               } 
            }    

            // video cover
            if($request->has('video_cover')){
               if(isset($request['video_cover'][$n])){
                $box->storeVideoCover($request['video_cover'][$n]);
               } 
            }            

            if(!empty($request['video_name'][$n])){
                $box->video_name = $request['video_name'][$n];
            }

            $box->type = $request['type'][$n];
            $box->position = $p;
            $box->description = $request['description'][$n];
            $box->url = $request['url'][$n];      

            $box->save();     
           
            $n++;
            $p++;
        }

       $exito = "Banners creados exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function updateHomeFeatured(Request $request)
    {
        // Validar
        $this->validate($request, [
            'type.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
            'video_file.*' => 'sometimes|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4|max:4000',
            //'video_cover.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000',
        ]);

        $n = 0;

        // ACTUALIZAR CADA BOX

        while($n < count($request['id'])){
            $box = Home::findOrFail($request['id'][$n]);

            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($box->image_name)){
                    Storage::disk('home')->delete($box->image_name);
                }
                //Guardar la nueva
                $box->storeImage($request['image_name'][$n]);
               } 
            }

            // video
            if($request->has('video_file')){
               if(isset($request['video_file'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($box->video_name)){
                    Storage::disk('home')->delete($box->video_name);
                }
                //Guardar la nueva
                $box->storeVideo($request['video_file'][$n]);
               }  
            }
            // video cover
            if($request->has('video_cover')){
               if(isset($request['video_cover'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($box->video_cover)){
                    Storage::disk('home')->delete($box->video_cover);
                }
                //Guardar la nueva
                $box->storeVideoCover($request['video_cover'][$n]);
               }  
            }
            // si es que tiene embed    
            if(!empty($request['video_name'][$n])){
                $box->video_name = $request['video_name'][$n];
            }    
            
            $box->type = $request['type'][$n];
            $box->position = $request['position'][$n];
            $box->description = $request['description'][$n];
            $box->url = $request['url'][$n];
            $box->update();

            $n++;
        }

        $exito = "Banners actualizados correctamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function updateHfeaturedPosition(Request $request)
    {
        $position = $request['positions'];
        $ids = $request['ids'];        

        $n = 0;

        if(count($ids) == 0){
            return response()->json("No hay cajas para actualizar", 200);
        }

        while($n < count($ids)){
            $box = Home::find($ids[$n]);
            $box->position = $position[$n];            
            $box->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    public function deleteHomeFeatured(Request $request)
    {
        //1  ubicar el objeto y obtener su posicion
        $box = Home::find($request['id']);
        $position = $box->position;        
        $image = $box->image_name;
        $video = $box->video_name;
        $video_cover = $box->video_cover;

        // 2 borrar los archivos imagenes y videos respectivos
        $box->delete();
        if(Storage::disk('home')->exists($image)){
            Storage::disk('home')->delete($image);
        }
        if(Storage::disk('home')->exists($video)){
            Storage::disk('home')->delete($video);
        }
        if(Storage::disk('home')->exists($video_cover)){
            Storage::disk('home')->delete($video_cover);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $boxes = Home::where('position', '>', $position)->get();
        $data = [];
        foreach($boxes as $bx){
            $current = $bx->position;
            $bx->position = $current - 1;
            $bx->update();
            $data[] = [
                'id' => $bx->id,
                'position' => $bx->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }
    
}
