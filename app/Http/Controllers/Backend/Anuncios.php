<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Models\Ad;

class Anuncios extends Controller
{
    /*
    *
    *
    *
    */

    public function showAds()
    {
    	$ads = Ad::orderBy('position', 'asc')->get();

    	return view('backend.publicidad', ['ads' => $ads]);
    }

    public function postAds(Request $request)
    {
    	if(!$request->has('type')){
            return redirect()->back()->with(['exito' => 'Debe crear por lo menos un anuncio']);
        }

        $this->validate($request, [
            'type.*' => 'required',
            'location.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000'
        ]);

        $n = 0;  

        // Asignar la posicion dependiendo de cuantos objetos hay en la tabla
        $all = count(Ad::all());
        if($all > 0){
            $p = $all + 1;
        }else{
            $p = 1;
        }       

        // Creamos los anuncios
        foreach($request['type'] as $ad){

            $ad = new Ad();

            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                $ad->storeImage($request['image_name'][$n]);
               } 
            }            

            
				
            if(!empty($request['video_name'][$n])){
                $ad->media_name = $request['video_name'][$n];
            }

            $ad->type = $request['type'][$n];
            $ad->position = $p;
            $ad->location = $request['location'][$n];
            $ad->active = $request['active'][$n];
            $ad->redirect_to = $request['url'][$n];      

            $ad->save();     
           
            $n++;
            $p++;
        }

       $exito = "Banners creados exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function updateAds(Request $request)
    {
    	// Validar
        $this->validate($request, [
            'type.*' => 'required',
            'location.*' => 'required',
            'image_name.*' => 'sometimes|mimes:jpeg,jpg,png|max:2000'
        ]);

        $n = 0;

        // ACTUALIZAR CADA BANNER

        while($n < count($request['id'])){
            $ad = Ad::findOrFail($request['id'][$n]);
            if($request->has('image_name')){
               if(isset($request['image_name'][$n])){
                //Borrar la anterior
                if(Storage::disk('home')->exists($ad->media_name)){
                    Storage::disk('home')->delete($ad->media_name);
                }
                //Guardar la nueva
                $ad->storeImage($request['image_name'][$n]);
               } 
            }
            // si tiene embed de video
            if(!empty($request['video_name'][$n])){
                $ad->media_name = $request['video_name'][$n];
            }
            //$ad->video_name = $request['video_name'][$n];
            $ad->type = $request['type'][$n];
            $ad->position = $request['position'][$n];
            $ad->location = $request['location'][$n];
            $ad->active = $request['active'][$n];
            $ad->redirect_to = $request['url'][$n];
            $ad->update();

            $n++;
        }

        $exito = "Banners actualizados correctamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    public function updateAdPosition(Request $request)
    {
    	$position = $request['positions'];
        $ids = $request['ids'];        

        $n = 0;

        if(count($ids) == 0){
            return response()->json("No hay imagenes para actualizar", 200);
        }

        while($n < count($ids)){
            $ad = Ad::find($ids[$n]);
            $ad->position = $position[$n];            
            $ad->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    public function deleteAds(Request $request)
    {
    	//1  ubicar el objeto y obtener su posicion
        $ad = Ad::find($request['id']);
        $position = $ad->position;        
        $image = $ad->media_name;

        // 2 borrar el objeto y borrar la imagen de la carpeta tmb
        $ad->delete();
        if(Storage::disk('home')->exists($image)){
            Storage::disk('home')->delete($image);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $ads = Ad::where('position', '>', $position)->get();
        $data = [];
        foreach($ads as $a){
            $current = $a->position;
            $a->position = $current - 1;
            $a->update();
            $data[] = [
                'id' => $a->id,
                'position' => $a->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }

}
