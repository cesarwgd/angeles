<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Angel;
use App\Models\Hot;

class Angeles extends Controller
{
    // MOSTRAR TODOS

    public function all()
    {
    	$angels = Angel::orderBy('id', 'desc')->paginate(12);

    	return view('backend.modelos', ['angels' => $angels]);
    }

    // SEARCH RESULT

    public function searchResult(Request $request)
    {
        $terms = explode(' ', $request['nombre']);

        $angels = Angel::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('nombre', 'LIKE', '%'.$term.'%');
                    }
                })->get();

        if(count($angels) == 0){
            $error = "No se encontró ninguna revista con ese correo";
            return view('backend.crud_angeles.search_result', ['angels' => null, 'error' => $error]);

        }

        return view('backend.crud_angeles.search_result', ['angels' => $angels]);
    }

    // VIEW PARA CREAR

    public function create()
    {
    	return view('backend.crud_angeles.create');
    }

    public function postCreate(Request $request)
    {
    	$this->validate($request, [
    		'nombre' => 'required|max:255'
    		]);

    	$angel = new Angel();
    	$angel->createAngel($request->all());
    	$id = $angel->id;
    	$exito = "Pagina de ángel creada exitosamente";

    	return redirect()->route('backend.modelo.view', ['id' => $id])->with(['angel' => $angel]);
    }

    // IMAGENES

    public function angelImages($id)
    {
    	$angel = Angel::find($id);

    	if($angel){
    		return view('backend.crud_angeles.images', ['angel' => $angel]);
    	}

    	return response('Not found', 404);
    }

    // subir imagenes por medio de un post normal sin ajax ni dropzone

    public function postAngelImages(Request $request, $id)
    {
    	$this->validate($request, [
    		'images.*' => 'image|max:5000'
    		]);

    	// vemos si es que ya hay imagenes para esta modelo
    	$previous = Hot::where('angel_id', $id)->get();

    	if($previous != null){
    		// si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
    		$n = count($previous) + 1;
    	}else{
    		// si no hay empieza desde 1
    		$n = 1;
    	}

    	if(!empty($request->file('images'))){
    		foreach($request->file('images') as $image){
	    		$hot = new Hot();
	    		$save = $hot->uploadImage($image);

	    		$hot->angel_id = $id;
		    	$hot->name = $save;
		        $hot->position = $n;
		        $hot->save();

		        $n++;    
	    	}

	    	$exito = "Imagenes guardadas exitosamente";

    		return redirect()->back()->with(['exito' => $exito]);
    	}

    	
    	$exito = "Debe elegir imagenes";

    	return redirect()->back()->with(['exito' => $exito]);    	

    }

    // SUBIR IMAGENES POR MEDIO DE DROPZONE AJAX

    public function angelesDropZoneFiles(Request $request, $id)
    {
        // vemos si es que ya hay imagenes para esta modelo
        $previous = Hot::where('angel_id', $id)->get();

        if($previous != null){
            // si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
            $n = count($previous) + 1;
        }else{
            // si no hay empieza desde 1
            $n = 1;
        }

        $data = [];

       

        if(!empty($request->file('file'))){
           foreach($request->file('file') as $image){
                $hot = new Hot();
                $save = $hot->uploadImage($image);

                $hot->angel_id = $id;
                $hot->name = $save;
                $hot->position = $n;
                $hot->save();
                
                $data[] = $hot;
                $n++;    
            }

            $exito = "Imagenes guardadas exitosamente";
            
            return response()->json($data, 200);
        }

        
        $exito = "Debe elegir imagenes";

        return response()->json();
    }

    // BORRAR IMAGENES DEL ANGEL POR MEDIO DE AJAX

    public function angelDeleteFile(Request $request, $id)
    {
        //1  ubicar el objeto y obtener su posicion y el angel al que pertenece y nombre de imagen
        $hot = Hot::find($id);
        $position = $hot->position;
        $angel = $hot->angel_id;
        $nombre = $hot->name;

        // 2 borrar el objeto y borrar la imagen de la carpeta tmb
        $hot->delete();
        if(Storage::disk('hotmas')->exists($nombre)){
            Storage::disk('hotmas')->delete($nombre);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $files = Hot::where('angel_id', $angel)->where('position', '>', $position)->get();
        $data = [];
        foreach($files as $file){
            $current = $file->position;
            $file->position = $current - 1;
            $file->update();
            $data[] = [
                'id' => $file->id,
                'position' => $file->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }

    // GUARDAR CAMBIOS DE POSICION

    public function angelUpdateFilePosition(Request $request)
    {        

        $position = $request['positions'];
        $ids = $request['ids'];

        $n = 0;

        while($n < count($ids)){
            $file = Hot::find($ids[$n]);
            $file->position = $position[$n];
            $file->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    // ACTUALIZAR

    public function update($id)
    {
    	$angel = Angel::find($id);

    	return view('backend.crud_angeles.update', ['angel' => $angel]);
    }

    public function postUpdate(Request $request, $id)
    {
    	$this->validate($request, [
    		'nombre' => 'required|max:255',
            'slug' => "unique:angels,slug,$id"
    		]);

    	$angel = Angel::find($id);
    	$angel->updateAngel($request->all());
    	$exito = "Pagina de ángel actualizada exitosamente";

    	return redirect()->back()->with(['exito' => $exito]);

    }

    public function delete($id)
    {
    	$angel = Angel::find($id);

        $angel_id = $angel->id;
        $hot = Hot::where('angel_id', $angel_id)->get();

    	$angel->delete();

        // Borramos sus imagenes de la carpeta
        foreach ($hot as $file) {
            if(Storage::disk('hotmas')->exists($file->name)){
                Storage::disk('hotmas')->delete($file->name);
            }
            $file->delete();
        }

    	$exito = "Angel eliminada exitosamente";

    	return redirect()->route('backend.modelos')->with(['exito' => $exito]);
    }

}
