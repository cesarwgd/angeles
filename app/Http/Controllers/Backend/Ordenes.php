<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderUpdate;
use App\User;
use App\Models\Order;
use App\Models\Revista;
use App\Mail\IssueAddedToUser;

class Ordenes extends Controller
{   
    private $countries_filtered;

    public function __construct()
    {   
        // obtenemos solo los datos de la columna country
        $countries = Order::pluck('country');
        $array_countries = [];
        foreach ($countries as $country) {
            $array_countries[] = $country;
        }
        $this->countries_filtered = array_unique($array_countries);
    }

    //
    public function all()
    {
    	$data['orders'] = Order::orderBy('id', 'desc')->paginate(50);
        // Obtenemos una lista de los paises de donde estan comprando
        
        $data['countries'] = $this->countries_filtered;


        $ventas = Order::where('status', 'completada')->get();
        $ventas_mes = Order::where('status', 'completada')->whereMonth('updated_at', date('m'))->whereYear('updated_at', date('Y'))->get();
        $data['revistas'] = Revista::all();
        $total = 0.00;
        $total_mes = 0.00;
        
        foreach ($ventas as $amount) {
            $total += str_replace(',', '', $amount->amount);
        }

        if(count($ventas_mes) > 0){
            foreach ($ventas_mes as $vmes) {
                $total_mes += str_replace(',', '', $vmes->amount);
            }
        }


        $data['total'] = $total;
        $data['total_mes'] = $total_mes;
        $data['revistas_vendidas'] = count($ventas);
        $data['revistas_mes'] = count($ventas_mes);

    	return view('backend.orders', $data);
    }

    // search result

    public function searchResult(Request $request)
    {
        $orden = Order::where('code', $request['code'])->first();

        if(!$orden){
            $error = "No se encontró ninguna orden con el código proporcionado";
            return view('backend.order_search_result', ['order' => null, 'error' => $error]);
        }

        return view('backend.order_search_result', ['order' => $orden]);
    }

    /*
    *
    *   FILTRAR POR REVISTA / ESTADO / ANIO / MES
    *
    *
    ***/

    public function orderFilterResult(Request $request)
    {
        $data['revistas'] = Revista::all();
        $data['countries'] = $this->countries_filtered;     
        $total = 0.00;

        
        // ordenes paginadas para el view
        $data['orders'] = Order::filters($request->all())->paginate(100);

        // ordenes totales para la contabilidad
        $total_orders = Order::filters($request->all())->get();

        // vemos el estado de las ordenes si es q eligio dicho filtro estado
        if($request->has('estado')){
            if($request['estado'] == "completada"){
                $data['revistas_vendidas'] = count($total_orders);
    
                foreach ($total_orders as $amount) {
                    $total += str_replace(',', '', $amount->amount);
                }
                
                $data['total'] = $total;

                }else{
                    $data['revistas_vendidas'] = 0;
                    $data['total'] = $total;
                }
        }else{
             $data['revistas_vendidas'] = 0;
            $data['total'] = $total;
        }
        
        $paginationParameters = [];

        // array con las variables de la paginacion para el url
        foreach ($request->all() as $key => $value) {
            
            $paginationParameters[$key] = $value;
           
        }
        $data['search_queries'] = $paginationParameters;

        return view('backend.orders_filter', $data);
                     


    }

    /*
    *
    *   VER ORDEN
    *
    */

    public function orderView($id)
    {
    	$order = Order::find($id);        
    	return view('backend.order_detail', ['order' => $order]);
    }

    public function orderUpdate(OrderUpdate $request, $id)
    {
        $order = Order::find($id);
        $order->status = $request['status'];
        $order->update();

        // si el status es completada - osea que la compra se hizo por deposito - agregarle la revista y enviarle notificacion

        if($request['status'] == 'completada'){

            $mail_revistas = [];

            $user = User::find($request['user_id']);
            $revista = Revista::where('id', $request['revista_id'])->first();
            // seteamos el array donde vamos a meter la info del producto agregado
            
            $mail_revistas[] = $revista->name;            

            // metemos la info a la variable que ira en el mail
            $data = [
                'user_name' => $request['user_name'],
                'user_last_name' => $request['user_last_name'],
                'revistas' => $mail_revistas,
            ];

            $user->revistas()->attach($revista);

            // enviamos el correo para notificarle al gil
            Mail::to($request['user_email'])->send(new IssueAddedToUser($data));
        }

        // exito

        $exito = "Orden actualizada correctamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    // buscar

    public function orderSearch($code)
    {
    	$orders = Order::where('code', $code)->get();
    	return view('backend.orders', ['orders' => $orders]);

    }

    /*
    *
    *   EXPORTAR TODAS LAS ORDENES A CSV
    *
    */

    public function exportOrdersToCsv()
    {
        $orders = Order::orderBy('id', 'desc')->get();

        $filename = "docotros/ordenes_exportadas.csv"; // location and name of the file stored in the server
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array(
            'Codigo', 
            'Tipo',            
            'Status',
            'Usuario',
            'Revista',
            'Monto',
            'Creada el'
            )
        );

        foreach($orders as $order) {
            fputcsv($handle, array(
                    $order['code'],
                    $order['type'],
                    $order['status'],
                    $order['user']['name'] . " " . $order['user']['last_name'],
                    $order['description'],
                    "S/." . $order['amount'],
                    $order['created_at']
                )
            );
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        $name_file = 'reporte_ordenes-' . date('d-m-Y') . ".csv";

        return response()->download($filename, $name_file, $headers);
    }

}
