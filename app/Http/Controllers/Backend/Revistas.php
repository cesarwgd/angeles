<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRevista;
use App\Http\Requests\UpdateRevista;
use App\Http\Requests\DeleteRevista;
use Illuminate\Support\Facades\Storage;
use App\Models\Revista;
use App\Models\File;


class Revistas extends Controller
{
    //
    public function all()
    {
        $revistas = Revista::orderBy('id', 'desc')->paginate(10);
    	return view('backend.revistas', ['revistas' => $revistas]);
    }   

    public function revistasPositionShow()
    {
        $revistas = Revista::orderBy('position', 'desc')->get();
        return view('backend.revistas_posiciones', ['revistas' => $revistas]);
    }

    public function revistasPositionPost(Request $request)
    {
        $posiciones = $request['position'];
        $ids = $request['revista_id'];
        $n = 0;

        foreach($ids as $id){
            $revista = Revista::find($id);
            $revista->position = $posiciones[$n];
            $revista->update();
            $n++;
        }

        return redirect()->back()->with(['exito' => 'Posiciones actualizadas con exito']);

    }

    /*
    *
    *   BUSCAR REVISTA
    *
    */

    public function searchResult(Request $request)
    {
        $terms = explode(' ', $request['name']);

        $revistas = Revista::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('name', 'LIKE', '%'.$term.'%');
                    }
                })->get();

        if(count($revistas) == 0){
            $error = "No se encontró ninguna revista con ese correo";
            return view('backend.crud_revistas.search_result', ['revistas' => null, 'error' => $error]);

        }

        return view('backend.crud_revistas.search_result', ['revistas' => $revistas]);
    } 

    /*
    *
    *   CREAR UNA NUEVA REVISTA
    *
    */

    public function new()
    {
    	return view('backend.crud_revistas.create');
    }

    public function postNew(CreateRevista $request)
    {
        $revista = new Revista();
        $revista->createRevista($request->all());
        $id = $revista->id;

        $exito = "Revista creada exitosamente. Ahora proceda a agregar las imagenes de la revista";

        if($revista->type == "revista"){
            // si es de tipo revista se le envia a la seccion donde sube las imagenes
            return redirect()->route('backend.revistas.files', ['id' => $id])->with(['exito' => $exito]);
        }else{
            // si es de tipo video se le envia defrente a la seccion donde coloca video
            return redirect()->route('backend.revista.video', ['id' => $id])->with(['exito' => $exito]);
        }

    }


    /*
    *
    *   ACTUALIZAR REVISTA
    *
    */

    public function editView($id)
    {
        $revista = Revista::find($id);

        return view('backend.crud_revistas.update', ['revista' => $revista]);
    }

    public function revistaUpdate(UpdateRevista $request, $id)
    {
        $this->validate($request, [
                'name' => 'required|max:255',
                'slug' => "required|unique:revistas,slug,$id",
                'sku' => "required|unique:revistas,sku,$id",
                'price' => 'required'
            ]);

        $revista = Revista::find($id);
        $revista->updateRevista($request->all());

        $exito = "Revista actualizada exitosamente.";

        return redirect()->back()->with(['exito' => $exito]);

    }

    /*
    *
    *   AGREGAR IMAGENES A REVISTA
    *
    */

    public function issueFiles($id)
    {
        $revista = Revista::find($id);

        return view('backend.crud_revistas.files', ['revista' => $revista]);
    }

    // SUBIR IMAGENES POR MEDIO DE DROPZONE AJAX

    public function revistaDropZoneFiles(Request $request, $id)
    {
        // vemos si es que ya hay imagenes para esta modelo
        $previous = File::where('revista_id', $id)->get();

        if($previous != null){
            // si hay la posicion para las nuevas imagenes empieza desde el total de las antiguas mas 1
            $n = count($previous) + 1;
        }else{
            // si no hay empieza desde 1
            $n = 1;
        }
       

        if(!empty($request->file('file'))){
            foreach($request->file('file') as $image){
                $file = new File();
                $save = $file->uploadImage($image);

                $file->revista_id = $id;
                $file->name = $save['img_nombre'];
                $file->extension = $save['img_extension'];
                $file->position = $n;
                $file->save();
                
                $n++;    
            }

            $exito = "Imagenes guardadas exitosamente | Por favor refresque la página";
           
            return response()->json($exito, 200);
        }

        
        $exito = "Debe elegir imagenes";

        return response()->json($exito, 500);
    }

    /*
    *
    *   BORRAR IMAGENES DE LA REVISTA POR MEDIO DE AJAX
    *
    */

    public function revistaDeleteFile(Request $request, $id)
    {
        //1  ubicar el objeto y obtener su posicion y la revista al que pertenece y nombre de imagen
        $file = File::find($id);
        $position = $file->position;
        $revista = $file->revista_id;
        $nombre = $file->name;

        // 2 borrar el objeto y borrar la imagen de la carpeta tmb
        $file->delete();
        if(Storage::disk('revista')->exists($nombre)){
            Storage::disk('revista')->delete($nombre);
        }

        //3 actualizar la posicion de los demas objetos que no fueron borrados y cuya posicion estaba por encima del que fue borrado
        $files = File::where('revista_id', $revista)->where('position', '>', $position)->get();
        $data = [];
        foreach($files as $file){
            $current = $file->position;
            $file->position = $current - 1;
            $file->update();
            $data[] = [
                'id' => $file->id,
                'position' => $file->position,
            ];
        }

        // 4 enviar respuesta 200
        return response()->json($data, 200);
    }

    /*
    *
    *   GUARDAR CAMBIOS DE POSICION Y DE CLASE DE IMAGEN VIA AJAX
    *
    */

    public function revistaUpdateFilePosition(Request $request)
    {        

        $position = $request['positions'];
        $ids = $request['ids'];
        $clase = $request['clases'];

        $n = 0;

        if(count($ids) == 0){
            return response()->json("No hay imagenes para actualizar", 200);
        }

        while($n < count($ids)){
            $file = File::find($ids[$n]);
            $file->position = $position[$n];
            $file->class = $clase[$n];
            $file->update();
            $n++;
        }        

        $exito = "Cambios guardados exitosamente";

        return response()->json($exito, 200);
    }

    /*
    *
    *   VIDEO PARA LA REVISTA
    *
    */

    public function showVideo($id)
    {
        $revista = Revista::find($id);
        return view('backend.crud_revistas.video', ['revista' => $revista]);
    }

        // CREAR

    public function postVideo(UpdateRevista $request, $id)
    {
        $this->validate($request, [
                'name' => 'required'
            ]);

        $video = new File();
        $video->revista_id = $id;
        $video->name = $request['name'];
        if($request->has('extra')){
            $video->extra = $request['extra'];
        }
        $video->type = "video";
        $video->mobile_url = $request['mobile_url'];
        $video->save();

        $exito = "Video creado exitosamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

        // ACTUALIZAR

    public function updateVideo(UpdateRevista $request, $id)
    {
        $this->validate($request, [
                'name.*' => 'required'
            ]);
            
        for($i = 0; $i < count($request['id']); $i++){
            $video = File::find($request['id'][$i]);
            $video->name = $request['name'][$i];
            $video->extra = $request['extra'][$i];
            $video->mobile_url = $request['mobile_url'][$i];
            $video->update();
        }

        $exito = "Actualización exitosa";

        return redirect()->back()->with(['exito' => $exito]);

        
    }

    /*
    *
    *   BORRAR REVISTA
    *
    */

    public function revistaDelete(DeleteRevista $request, $id)
    {
        $revista = Revista::find($id);
        $revistas = Revista::where('position', '>', $revista->position)->get();
        $main_pic = $revista->main_pic;
        // obtenemos la coleccion de files asociados con la revista
        $files = File::where('revista_id', $id)->get();
        // Borramos la revista y su imagen de portada        
        $revista->delete();
        if(Storage::disk('revista')->exists($main_pic)){
            Storage::disk('revista')->delete($main_pic);
        }
        // Borramos los archivos de la revista
        foreach($files as $file){
            if(Storage::disk('revista')->exists($file->name)){
                Storage::disk('revista')->delete($file->name);
            }
            $file->delete();
        }   
        /*atualizamos las posiciones*/
        if(count($revistas) > 0){
            foreach($revistas as $rev){
                $new_pos = $rev->position - 1;
                $rev->position = $new_pos;
                $rev->update();
            }
        }
        
        return response()->json();
        
    }

    /*
    *
    *   GET IMAGE
    *
    */

    public function getRevistasImages($filename, $extension)
    {
        $image = $filename . "." . $extension;
        $file = Storage::disk('revista')->get($image);      
        
        return response($file, 200);
    }

    


}
