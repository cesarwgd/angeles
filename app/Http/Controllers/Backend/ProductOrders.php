<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\ProductOrder;
use App\Models\Product;

class ProductOrders extends Controller
{
	//

	private $countries_filtered;

    public function __construct()
    {   
        // obtenemos solo los datos de la columna country
        $countries = ProductOrder::pluck('country');
        $array_countries = [];
        foreach ($countries as $country) {
            $array_countries[] = $country;
        }
        $this->countries_filtered = array_unique($array_countries);
    }

    //
    public function all()
    {
    	$data['orders'] = ProductOrder::orderBy('id', 'desc')->paginate(50);
        // Obtenemos una lista de los paises de donde estan comprando
        
        $data['countries'] = $this->countries_filtered;


        $ventas = ProductOrder::where('status', 'completada')->get();
        $ventas_mes = ProductOrder::where('status', 'completada')->whereMonth('updated_at', date('m'))->whereYear('updated_at', date('Y'))->get();
        $data['productos'] = Product::all();
        $total = 0.00;
        $total_mes = 0.00;
        
        foreach ($ventas as $amount) {
            $total += $amount->amount;
        }

        if(count($ventas_mes) > 0){
            foreach ($ventas_mes as $vmes) {
                $total_mes += $vmes->amount;
            }
        }


        $data['total'] = $total;
        $data['total_mes'] = $total_mes;
        $data['productos_vendidos'] = count($ventas);
        $data['productos_mes'] = count($ventas_mes);

    	return view('backend.product_orders', $data);
    }

    // search result

    public function searchResult(Request $request)
    {
        $orden = ProductOrder::where('code', $request['code'])->first();

        if(!$orden){
            $error = "No se encontró ninguna orden con el código proporcionado";
            return view('backend.product_order_search_result', ['order' => null, 'error' => $error]);
        }

        return view('backend.product_order_search_result', ['order' => $orden]);
    }

    /*
    *
    *   FILTRAR POR REVISTA / ESTADO / ANIO / MES
    *
    *
    ***/

    public function orderFilterResult(Request $request)
    {
        $data['productos'] = Product::all();
        $data['countries'] = $this->countries_filtered;     
        $total = 0.00;

        
        // ordenes paginadas para el view
        $data['orders'] = ProductOrder::filters($request->all())->paginate(100);

        // ordenes totales para la contabilidad
        $total_orders = ProductOrder::filters($request->all())->get();

        // vemos el estado de las ordenes si es q eligio dicho filtro estado
        if($request->has('estado')){
            if($request['estado'] == "completada"){
                $data['productos_vendidos'] = count($total_orders);
    
                foreach ($total_orders as $amount) {
                    $total += $amount->amount;
                }
                
                $data['total'] = $total;

            }else{
                $data['productos_vendidos'] = 0;
                $data['total'] = $total;
            }
        }else{
             $data['productos_vendidos'] = 0;
            $data['total'] = $total;
        }
        
        $paginationParameters = [];

        // array con las variables de la paginacion para el url
        foreach ($request->all() as $key => $value) {
            
            $paginationParameters[$key] = $value;
           
        }
        $data['search_queries'] = $paginationParameters;

        return view('backend.product_orders_filter', $data);
                     


    }

    /*
    *
    *   VER ORDEN
    *
    */

    public function orderView($id)
    {
    	$order = ProductOrder::find($id);        
    	return view('backend.product_order_detail', ['order' => $order]);
    }

    public function orderUpdate(Request $request, $id)
    {
        $order = ProductOrder::find($id);
        $order->status = $request['status'];
        $order->update();

        // si el status es completada - osea que la compra se hizo por deposito - agregarle la revista y enviarle notificacion

       /* if($request['status'] == 'completada'){

            $mail_revistas = [];

            $user = User::find($request['user_id']);
            $revista = Product::where('id', $request['product_id'])->first();
            // seteamos el array donde vamos a meter la info del producto agregado
            
            $mail_revistas[] = $revista->name;            

            // metemos la info a la variable que ira en el mail
            $data = [
                'user_name' => $request['user_name'],
                'user_last_name' => $request['user_last_name'],
                'revistas' => $mail_revistas,
            ];

            $user->revistas()->attach($revista);

            // enviamos el correo para notificarle al gil
            Mail::to($request['user_email'])->send(new IssueAddedToUser($data));
        }*/

        // exito

        $exito = "Orden actualizada correctamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    // buscar

    public function orderSearch($code)
    {
    	$orders = ProductOrder::where('code', $code)->get();
    	return view('backend.product_order_search_result', ['orders' => $orders]);

    }

    /*
    *
    *   EXPORTAR TODAS LAS ORDENES A CSV
    *
    */

    public function exportOrdersToCsv()
    {
        $orders = ProductOrder::orderBy('id', 'desc')->get();

        $filename = "docotros/productos_ordenes_exportadas.csv"; // location and name of the file stored in the server
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array(
            'Codigo',                       
            'Status',
            'Usuario',
            'Revista',
            'Monto',
            'Creada el'
            )
        );

        foreach($orders as $order) {
            fputcsv($handle, array(
                    $order['code'],                   
                    $order['status'],
                    $order['user']['name'] . " " . $order['user']['last_name'],
                    $order['description'],
                    "S/." . $order['amount'],
                    $order['created_at']
                )
            );
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        $name_file = 'reporte_productos_ordenes-' . date('d-m-Y') . ".csv";

        return response()->download($filename, $name_file, $headers);
    }


    //
}
