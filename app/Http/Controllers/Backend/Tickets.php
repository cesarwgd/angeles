<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Mail\SupportTicket;
use App\Mail\SupportTicketReply;

class Tickets extends Controller
{
    //
    public function usersTickets()
    {
    	$tickets = Ticket::where('initial', 1)->orderBy('id', 'desc')->paginate(20);
    	return view('backend.tickets', ['tickets' => $tickets]);
    }

    public function viewTicket($id)
    {
    	$data['ticket'] = Ticket::findOrFail($id);
    	$data['mensajes'] = Ticket::where('channel_id', $id)->get();

    	return view('backend.ticket_view', $data);
    }

    public function updateStatusTicket(Request $request, $id)
    {
    	$this->validate($request, [
    		'estado' => 'required|max:30'
    	]);

    	$ticket = Ticket::findOrFail($id);
    	$ticket->estado = $request['estado'];
    	$ticket->update();

    	$exito = "Estado del ticket actualizado con éxito";

    	return redirect()->back()->with(['exito' => $exito]);

    }

    public function replySupportTicket(Request $request, $id)
    {
    	$this->validate($request, [
    		'detalle' => 'required'
    	]);

    	$data = $request->all();

    	$ticket = new Ticket();
    	$ticket->adminReplyTicket($data);

    	// Mandar el correo

    	Mail::to($data['email'])->send(new SupportTicketReply($data));

    	$exito = "Respuesta enviada con éxito";

    	return redirect()->back()->with(['exito' => $exito]);

    }

    public function searchTicketResult(Request $request)
    {
        $ticket = Ticket::where('id', $request['ticket_id'])->where('initial', 1)->first();

        if(count($ticket) == 0){
            return view('backend.ticket_search')->with(['ticket' => null]);
        }

        return view('backend.ticket_search')->with(['ticket' => $ticket]);

    }

}
