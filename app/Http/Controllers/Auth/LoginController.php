<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\NewUserRegistered;
use Socialite;
use App\User;
use App\Role;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/revistas';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /*
    *
    * REDIRECT TO FACEBOOK IF USER DID NOT AUTHORIZE EMAIL PERMISSION
    *
    **/

    public function redirectAgainToFb()
    {
        return Socialite::driver('facebook')->with(['auth_type' => 'rerequest'])->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        /*if ($request->has('error') || $request->has('error_code')) {
            return redirect('/login');
        }

        */

        try {
            $fb_user = Socialite::driver('facebook')->user();      
        }catch (\Exception $e) {            
            return redirect('/login');
        }

        if($fb_user->getEmail() == null){
            return redirect('/login')->with(['autoriza' => 'Por favor acceda con su correo y autorize el acceso a la información de su correo de Facebook.']);
        }

        $user = $this->fbUserFinOrCreate($fb_user);

        Auth::login($user, true);

        return redirect()->intended('/user/revistas');


    }

    // Find user logged in with fb or create it if it does not exist

    public function fbUserFinOrCreate($fb_user)
    {
        $user = User::where('email', $fb_user->getEmail())->orWhere('fb_id', $fb_user->getId())->first();

        if(!$user){
            // si no lo encuentra en la DB quiere decir que no existe entonces lo creamos y luego lo logueamos
            $user = new User();
            $user->name = $fb_user->getName();
            $user->email = $fb_user->getEmail();
            $user->fb_id = $fb_user->getId();

            $user->save();
            // le agregamos su respectivo rol de usuario
            $role = Role::where('nombre', 'usuario')->first();
            $user->roles()->attach($role);
            
        }

        return $user;

    }

}
