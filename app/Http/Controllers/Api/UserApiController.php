<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Revista;
use App\Models\ProductOrder;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserRevistasResource;
use App\Events\PaymentSubmitted;
use App\Events\ProductPaymentSubmitted;



class UserApiController extends Controller
{
    //
    public function user(Request $request)
    {

    	$user_email = $request->input('email');
    	$user_pass = $request->input('password');

    	$user = User::where('email', $user_email)->first();
    	$check_password = Hash::check($user_pass, $user->password);

    	if($check_password){
    		return new UserResource($user);
    	}

    	return "Go away bitch!";
    	
    }


    public function userRevistas(Request $request)
    {
        $userRevistas = Revista::where('user_id', request('user_id'))->orderBy()->get();
    }

    // UPDATE USER DATA

    public function updateUserData(Request $request)
    {
        $user = Auth::user();

        if(request('name') != null){
            $user->name = request('name');
        }

        if(request('last_name') != null){
            $user->last_name = request('last_name');
        }
        
        
        if(request('email') != null){
            $user->email = request('email');
        }

        if(request('password') != null){
            $user->password = bcrypt(request('password')); 
        }

        $user->update();

        return response()->json(['success' => 'Información actualizada exitosamente'], 200);


    }

    // GET USER REVISTAS

    public function getUserRevistas()
    {
        // una lista q contiene mapas
        //$user_revistas = Auth::user()->revistas();
        $user = Auth::user();
       //$user_revistas = $user->revistas()->get();

        return response()->json(['revistas' => UserRevistasResource::collection($user->revistas)], 200);
    }

    // PAYMENT SUCCESSFUL FOR REVISTAS - CULQI


    public function updateUserOrderRevistas(Request $request){

        // se realizo el cargo con exito
        $user = Auth::user();

        /*
            necesito los siguientes datos:
            order_id de todas las ordenes / todos los datos del objeto orden
            revistas_id de todas las revistas adquiridas 
            culqiEmail
            culqiAmount - total
            nombres del usuario

        */
        $data['revistas_id'] = $request['revistas_id'];
        $data['culqiEmail'] = $request['culqiEmail'];
        $data['order_id'] = $request['order_id'];
        $data['order_description'] = $request['order_description'];
        $data['order_code'] = $request['order_code'];
        $data['nombres'] = $user->name . " " . $user->last_name;
        $data['emailAmount'] = $request['amount'] / 100;

        event(new PaymentSubmitted($data));
        
        $exito = "Compra realizada con éxito. Disfrute de las revistas compradas.";

        return response()->json(['success' => $exito], 200);

    }

    // PAYMENT SUCCESFUL FOR PRODUCTOS - CULQI


    public function updateUserOrderProductos(Request $request){

        // se realizo el cargo con exito
        $user = Auth::user();

        // actualizar en la bd de productorder el shipping address - esto por el momento no usarlo
        $data['shipping_address'] = $request['direccion'] . ", " . $request['ciudad'] . ", " . $request['provincia'] . ", " . $request['postal_code'] . ", " . $request['country'];

        foreach ($request['order_id'] as $order) {
            $update_order = ProductOrder::findOrFail($order);
            $update_order->status = "completada";
            $update_order->shipping_address = $data['shipping_address'];
            $update_order->contact_phone_number = $request['phone_number'];
            $update_order->update();
        }
        // necesito todos los dtos referentes a la orden

        $data['culqiEmail'] = $request['culqiEmail'];
        $data['order_description'] = $request['order_description'];
        $data['order_quantity'] = $request['order_quantity'];
        $data['order_amount'] = $request['order_amount'];
        $data['order_code'] = $request['order_code'];

        $data['nombres'] = $user->name . " " . $user->last_name;
        $data['emailAmount'] = $request['culqiAmount'] / 100;

        event(new ProductPaymentSubmitted($data));
        
        $exito = "Compra realizada con éxito. Hemos sido notificados de su orden y la estamos preparando. Por favor revise su correo a donde le hemos enviado su recibo con mayores detalles.";

        //return redirect()->route('user.ordenes')->with(['exito' => $exito]);

        return response()->json(['success' => $exito], 200);

    }

}
