<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\RevistasResource;
use App\Http\Resources\RevistaItemResource;
use App\Http\Resources\RevistaUserViewResource;
use App\Http\Resources\RevistaFilesResource;
use App\Models\Revista;


class RevistasApiController extends Controller
{

	/*
		Return all amgazines
	*/

	public function __construct()
	{
		RevistasResource::withoutWrapping();
	}	


    public function revistasIndex()
    {
    	$revistas = Revista::where('published', 1)->orderBy('id', 'desc')->get();	

    	return RevistasResource::collection($revistas);
    }


    /*
		Return a specific magazine
    */

	public function show($id)
	{
		$revista = Revista::findOrFail($id);		

		return new RevistaItemResource($revista);
	}

	/*
		Return the view of the magazine for the user who bought it
	*/

	public function revistaView($id)
	{
		$revista = Revista::findOrFail($id);
		$revistaFiles = $revista->files()->where('class', 'content')->orderBy('position', 'asc')->get();		

		return RevistaFilesResource::collection($revistaFiles);
	}

	/*
		API ENDPOINT FOR THE MAGAZINES FILES
	*/

	public function revistaEachImage($id, $filename, $extension)
	{
		$image = $filename . "." . $extension;
        $file = Storage::disk('revista')->get($image);

        //$encoded = base64_encode($file);         
       
        return response($file, 200)->header('Content-Type', 'image');
	} 

	// VIDEO MOBILEE

	public function revistaMobileVideo($hash_slug)
	{
		$data['user'] = Auth::user();
        $data['revista'] = Revista::where('hash_slug', $hash_slug)->first();
        

        return view('frontend.revista.revista_single_video_mobile', $data);
	}


}
