<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\ProductOrder;
use App\Models\Product;
use Stevebauman\Location\Facades\Location;

class ProductoOrderApiController extends Controller
{


   public function placeProductoOrderMobile(Request $request)
    {
    	$order = new ProductOrder();
        $producto = Product::where('id', $request['product_id'])->first();

        if($producto == null){
        	return response()->json(['error'=> 'No se encontró el producto seleccionado'], 400);
        }

        // out of stock
        if($producto->stock == 0){
            return response()->json(['error'=> 'El producto se encuentra agotado.'], 400);
        }
       

        $data['product_id'] = $producto->id;
        $data['type'] = "producto";
        $data['user_id'] = Auth::user()->id;
        $data['amount'] = $request['amount'];
        $data['quantity'] = $request['quantity'];
        $data['delivery_amount'] = $producto->delivery_amount == null ? 0 : $producto->delivery_amount;
        $data['description'] = $producto->name;

        if($request['color'] != null){
        	$data['color'] = $request['color'];
        }
        if($request['talla'] != null){
        	$data['talla'] = $request['talla'];
        }
        // OBTENEMOS LA UBICACION DEL USUARIO POR MEDIO DE SU IP Y HACIENDO UN LLAMADO A LA BD DE MAXMIND EN NUESTRO SERVER
        $user_ip = Location::get($request->ip()); // esto falta chekear
        $data['country'] = $user_ip->countryName;
        $data['source'] = 'mobile';

        // Creamos la orden
        $order->placeOrder($data);

        return response()->json([
        	'success' => 'Su orden fue registrada',
        	'order' => [
	        	'id' => $order->id,
	            'code' => $order->code,
	            'type' => $order->type,            
	            'status' => $order->status,
	            'quantity' => $order->quantity,
	            'amount' => $order->amount,
	            'delivery_amount' => $order->delivery_amount,
	            'description' => $order->description,
	            'talla' => $order->talla,
	            'color' => $order->color,
	            'updated_at' => $order->updated_at,
	        ]
        ], 
        200);

    }


    // REMOVE FROM CART
    public function removeProductoOrdeMobile(Request $request)
    {
         $order = ProductOrder::findOrFail(request('producto-id'));
         $order->delete();

         $exito = "Orden removida de su carrito de compras";

         return response()->json(['success' => $exito], 200);

    }

    // UPDATE QUANTITY
    public function updateProductoOrderMobile(Request $request)
    {
    	
    }

}
