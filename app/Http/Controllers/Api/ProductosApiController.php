<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductosResource;
use App\Models\Product;

class ProductosApiController extends Controller
{
    //
    public function productsIndex()
    {
    	$productos = Product::where('published', 1)->orderBy('id', 'desc')->get();	

    	return ProductosResource::collection($productos);
    }
}
