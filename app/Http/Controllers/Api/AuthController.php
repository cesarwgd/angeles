<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Models\Revista;
use Carbon\Carbon;
use App\Http\Resources\UserResource;
use App\Role;



class AuthController extends Controller
{
    
	public $successStatus = 200;
  
	 public function register(Request $request) {

		 $validator = Validator::make($request->all(), [ 
		              'name' => 'required',
		              'last_name' => 'required',
		              'email' => 'required|email',
		              'password' => 'required',  
		              'c_password' => 'required|same:password', 
		    ]);   
		 if ($validator->fails()) {
		       //return response()->json(['error'=>$validator->errors()], 401);
		       return response()->json(['error'=>'Error al validar los campos, verifique que haya ingresado el nombre, apellido, correo y contraseñas correctamente'], 401);
		 	//return response()->json(['error'=>'lalito'], 401);
		 }

		 // check if user already exists in databse
		 $check_user = User::where('email', $request['email'])->get();

		 //['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8']

		 if(count($check_user) > 0){
		 	return response()->json(['error'=> 'Ya existe un usuario registrado con esta cuenta de correo'], 401);
		 }

		 $input = $request->all();  
		 $input['password'] = bcrypt($input['password']);
		 $user = User::create($input); 

		 // ASSIGN TROLE TO USER
		 event(new NewUserRegistered($user = $this->create($request->all())));

		 $objToken = $user->createToken('AppName');
		 $success['token'] =  $objToken->accessToken;

		 $success['expiration'] = $objToken->token->expires_at->diffInSeconds(Carbon::now());

		 
		 $success['userId'] = $user->id;
		 
		 return response()->json(['success'=>$success], $this->successStatus); 

	}
	  
	   
	public function login(){ 

		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

		   $user = Auth::user(); 
		   $objToken = $user->createToken('AppName');
		   $success['token'] =  $objToken->accessToken;
		   $success['expiration'] = $objToken->token->expires_at->diffInSeconds(Carbon::now());
		   $success['userId'] = $user->id;
		   return response()->json(['success' => $success], $this->successStatus); 

		}else{ 

		   return response()->json(['error'=>'Los datos proporcionados son incorrectos. Por favor revíselos'], 401); 

		} 

	}
	  
	public function getUser() {

		 $user = Auth::user();
		 //$userData = new UserResource::collection($user);

		 return new UserResource($user);

		 //return response()->json(['success' => $user], $this->successStatus); 

	}

	public function testUser($id)
	{
		$user = User::where('id', $id)->first();
		return new UserResource($user);
	}

	/*
		LOGIN WITH FACEBOOK
	*/

	public function loginFacebookApi(){
		// we first check if the user is already registered in our site if not we save the users information in our database
		$user = User::where('email', request('fb_email'))->orWhere('fb_id', request('fb_uid'))->first();

        if(!$user){
            // si no lo encuentra en la DB quiere decir que no existe entonces lo creamos y luego lo logueamos
            $user = new User();
            
            $user->email = request('fb_email');
            $user->fb_id = request('fb_uid');

            if(request('fb_first_name') != null){
            	$user->name = request('fb_first_name');
            }
            
            if(request('fb_last_name') != null){
            	$user->last_name = request('fb_last_name');
            }

            $user->save();
            // le agregamos su respectivo rol de usuario
            $role = Role::where('nombre', 'usuario')->first();
            $user->roles()->attach($role);
            
        }

        // log the user in - chekar lo q hace el auth attempt
        $objToken = $user->createToken('AppName');
		$success['token'] =  $objToken->accessToken;
		$success['expiration'] = $objToken->token->expires_at->diffInSeconds(Carbon::now());
		$success['userId'] = $user->id;

		return response()->json(['success' => $success], $this->successStatus);

	}

	/*public function getRevistas()
	{
		$revistas = Revista::where('published', 1)->orderBy('id', 'desc')->get();

		return response()->json(['data' => $revistas], $this->successStatus);
	}*/


}
