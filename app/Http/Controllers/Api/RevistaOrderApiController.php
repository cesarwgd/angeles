<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserOrdersResource;
use App\Models\Order;
use App\Models\Revista;
use App\Models\ProductOrder;
use Stevebauman\Location\Facades\Location;

class RevistaOrderApiController extends Controller
{
    //
    public function placeRevistaOrderMobile(Request $request)
    {
    	$order = new Order();
        $revista = Revista::where('id', $request['revista_id'])->first();

        if($revista == null){
        	return response()->json(['error'=> 'No se encontró la revista seleccionada'], 400);
        }

        // revisamos que no tenga ya esa revista en su lista personal O AGREGADA AL SHOPPING CART
        if(Auth::user()->hasIssueById($revista->id) || $order->hasIssueOrdered(Auth::user()->id, $revista->id)){
            $already = "Usted ya adquirió esta revista o ya la agregó a su carrito de compras";
            return response()->json(['error'=> $already], 400);
        }

        $data['revista_id'] = $revista->id;
        $data['type'] = request('type');
        $data['user_id'] = Auth::user()->id;
        $data['amount'] = $revista->price;
        $data['description'] = $revista->name;
        // OBTENEMOS LA UBICACION DEL USUARIO POR MEDIO DE SU IP Y HACIENDO UN LLAMADO A LA BD DE MAXMIND EN NUESTRO SERVER
        $user_ip = Location::get($request->ip());
        $data['country'] = $user_ip->countryName;
        $data['source'] = 'mobile';

        // Creamos la orden
        $order->placeOrder($data);

        return response()->json([
        	'success' => 'Su orden fue registrada',
        	'order' => [
	        	'id' => $order->id,
	            'code' => $order->code,
	            'type' => $order->type,            
	            'status' => $order->status,
	            'amount' => $order->amount,
	            'description' => $order->description,
	            'updated_at' => $order->updated_at,
	        ]
        ], 
        200);

    }


    // REMOVER DEL CARRITO

    public function removeRevistaOrdeMobile(Request $request)
    {
    	 $order = Order::findOrFail(request('revista-id'));
         $order->delete();

         $exito = "Orden removida de su carrito de compras";

         return response()->json(['success' => $exito], 200);

    }

    

    // GET USER REVISTA & PRODUCTOS ORDERS

    public function userOrdenesMobile()
    {
        $user = Auth::user();       
        $ordenes_revistas = Order::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        $ordenes_productos = ProductOrder::where('user_id', $user->id)->orderBy('id', 'desc')->get();

        return response()->json(['ordenes' => $ordenes_revistas, 'ordenes_productos' => $ordenes_productos], 200); // 'ordenes_productos' => $ordenes_productos
    }


}
