<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserProfile;
use App\Http\Requests\UserReplyTicket;
use App\User;
use App\Models\Revista;
use App\Models\Ticket;
use App\Mail\SupportTicket;
use App\Models\Order;
use App\Models\CategoriePost;
use App\Models\ProductOrder;

class Users extends Controller
{    

    /*
    *
    *   PERFIL
    *
    */

    public function perfil()
    {
        $user = Auth::user();
        $cats = CategoriePost::all();
        return view('frontend.user.profile', ['user' => $user, 'cats' => $cats]);
    }

    /*
    *
    *   REVISTAS
    *
    */

    public function userRevistas()
    {
        $cats = CategoriePost::all();
        $user = Auth::user();
        // si el  usuario es admin o premium que se muestren todas las revistas
        $roles = ['admin', 'premium'];
        if($user->hasAnyRole($roles)){
            $revistas = Revista::all();
        }else{
            $revistas = null;
        }
        
    	return view('frontend.user.revistas', ['user' => $user, 'revistas' => $revistas, 'cats' => $cats]);
    }

    /*
    *
    *   ORDENES
    *
    */

    public function userOrdenes()
    {
        $data['user'] = Auth::user();
        $data['cats'] = CategoriePost::all();
        $data['ordenes'] = Order::where('user_id', $data['user']->id)->orderBy('id', 'desc')->get();
        $data['productos_ordenes'] = ProductOrder::where('user_id', $data['user']->id)->orderBy('id', 'desc')->get();

        return view('frontend.user.orders', $data);
    }

    /*
    *
    *   TICKETS
    *
    */

    public function userTickets()
    {
        $data['user'] = Auth::user();
        $data['cats'] = CategoriePost::all();
        $data['tickets'] = Ticket::where('user_id', $data['user']->id)->where('initial', 1)->orderBy('id', 'desc')->paginate(24);
    	return view('frontend.user.tickets', $data);
    }    

    // TICKET INDIVIDUAL

    public function userTicketView($id)
    {
        $data['cats'] = CategoriePost::all();
        $data['ticket'] = Ticket::findOrFail($id);

        if($data['ticket']->initial != 1){
            return redirect()->route('user.tickets');
        }

        $data['user'] = Auth::user();        
        $data['mensajes'] = Ticket::where('channel_id', $id)->get();

    	return view('frontend.user.ticket_view', $data);
    }

    public function postTicketReply(UserReplyTicket $request, $id)
    {
    	
        $data = $request->all();

        $ticket = new Ticket();
        $ticket->userReplyTicket($data);

        $data['ticket_id'] = $ticket->id;

        Mail::to('soporte@angelesrevista.com')->send(new SupportTicket($data));

        $exito = "Respuesta enviada exitosamente. Le responderemos a la brevedad posible.";

        return redirect()->back()->with(['exito' => $exito]);

    }

    // CREAR NUEVO TICKET

    public function userTicketCreate()
    {
        $user = Auth::user();
        $cats = CategoriePost::all();
    	return view('frontend.user.ticket_create', ['user' => $user, 'cats' => $cats]);
    }

    public function postUserTicket(Request $request)
    {
        $this->validate($request, [
                'tipo' => 'required|max:20',
                'asunto' => 'required|max:100',
                'detalle' => 'required|max:500'
            ]);

        $data = $request->all();

        // crear nuevo ticket en la bd

        $ticket = new Ticket();
        $ticket->createTicket($data);
        
        $data['ticket_id'] = $ticket->id;

        //mandar el mail de notificacion

        Mail::to('soporte@angelesrevista.com')->send(new SupportTicket($data));

        $exito = "Ticket de soporte creado exitosamente. Le responderemos a la brevedad posible.";

        return redirect()->route('user.tickets')->with(['exito' => $exito]);

    }

    /*
    *
    *   ACTUALIZAR INFO DEL USUARIO
    *
    */

    public function updateUserInfo(UpdateUserProfile $request)
    {
        $user = Auth::user();        

        $user->name = $request['name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];

        if(!empty($request['password'])){
            $user->password = bcrypt($request['password']);
        }        

        $user->update();
        

        $exito = "Información actualizada correctamente";

        return redirect()->back()->with(['exito' => $exito]);
    }

    //premium - esto quedo como una funcionalidad para un futuro

    public function updateToPremium()
    {

    }

    // delete user
    public function deleteUser(Request $request)
    {
        $id = Auth::id();        
        $user_delete = User::where('id', $id)->first();

        Auth::logout();        

        $user_delete->delete();

        return redirect()->route('home');



    }
}
