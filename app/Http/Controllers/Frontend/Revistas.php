<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Revista;
use App\Models\Ad;
use App\Models\CategoriePost;

class Revistas extends Controller
{
    //
    public function allIssues()
    {
        $data['revistas'] = Revista::orderBy('position', 'desc')->where('published', 1)->paginate(24);
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'revistas'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();

    	return view('frontend.revista.all', $data);
    }

    public function issuePreview($slug)
    {
        $data['revista'] = Revista::where('slug', $slug)->first();

        // Si la revista no esta publicada mandarlos al home de revistas
        if($data['revista']->published == 2 || $data['revista'] == null){
            return redirect()->route('revistas');
        }

        $data['otros'] = Revista::whereNotIn('id', [$data['revista']->id])->where('published', 1)->inRandomOrder()->take(3)->get();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'revistas'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();

    	return view('frontend.revista.revista_detail', $data);
    }

    // LASTEST ISSUE

    public function latestIssue()
    {
        $data['new'] = Revista::orderBy('position', 'desc')->where('published', 1)->first();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'revistas'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();
        
        return view('frontend.revista.new', $data);
    }


    /*
    *
    *   VER LAS REVISTAS - USUARIOS
    *
    */

    public function watchIssue($hash_slug, $page)
    {
        $data['user'] = Auth::user();
        $data['revista'] = Revista::where('hash_slug', $hash_slug)->first();
        $data['revistas'] = Revista::all();
        $data['page'] = $page;

    	return view('frontend.revista.revista_single', $data);
    }

    public function watchIssueVideo($hash_slug)
    {
        $data['user'] = Auth::user();
        $data['revista'] = Revista::where('hash_slug', $hash_slug)->first();
        $data['revistas'] = Revista::all();

        return view('frontend.revista.revista_single_video', $data);
    }

    // VISTA DEL VIDEO DE LA REVISTA PARA MOBILES

    public function watchMobileIssueVideo($userId, $revistaId)
    {
        $data['user'] = Auth::user();
        $data['revista'] = Revista::where('id', $revistaId)->first();
        

        return view('frontend.revista.revista_single_video_mobile', $data);
    }

    // public images

    public function getRevistaPublicImage($filename, $extension)
    {
        $image = $filename . "." . $extension;
        $file = Storage::disk('revista')->get($image);      
        
        return response($file, 200);
    }

    // private image

    public function getRevistaPrivateImage($filename, $extension)
    {
        $image = $filename . "." . $extension;
        $file = Storage::disk('revista')->get($image);      
        
        return response($file, 200);
    }

    // FOR THE API
    /*public function getJsonRevistaImage($filename, $extension)
    {
        $image = $filename . "." . $extension;
        $file = Storage::disk('revista')->get($image);

        $encoded = base64_encode($file);  
        
        //return response()->json(['image_blob' => $encoded],200);

        return response($file, 200)->header('Content-Type', 'image');
    }*/

    /*
        BUSCAR REVISTA POR NOMBRE
    */

    public function searchIssue(Request $request)
    {

        $this->validate($request, [
            'nombre' => 'required|string'
        ]);

        $trimmed = trim($request['nombre']);

        $terms = explode(' ', $trimmed);

        $data['revistas'] = Revista::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('name', 'LIKE', '%'.$term.'%')->where('published', 1);
                    }
                })->get();
        $data['cats'] = CategoriePost::all();

        

        return view('frontend.revista.search_result', $data);

    } 


}
