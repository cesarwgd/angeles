<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Ad;

class Products extends Controller
{
    public function allProducts()
    {
    	$products = Product::where('published', 1)->paginate(20);

    	return view('frontend.products.all', ['products' => $products]);
    }

    // product detail

    public function productDetail($slug)
    {
    	$data['product'] = Product::where('slug', $slug)->first();

        // Si la revista no esta publicada mandarlos al home de revistas
        if($data['product']->published == 2 || $data['product'] == null){
            return redirect()->route('frontend.productos');
        }
    	
        $data['otros'] = Product::whereNotIn('id', [$data['product']->id])->where('published', 1)->inRandomOrder()->take(3)->get();
        
    	$data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'revistas'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();

    	return view('frontend.products.product_detail', $data);
    }

    // product search
    public function productSearchResult(Request $request)
    {
    	$terms = explode(' ', $request['nombre']);

        $products = Product::where(function($query) use ($terms){
                    foreach($terms as $term){
                        $query->where('name', 'LIKE', '%'.$term.'%');
                    }
                })->get();

        if(count($products) == 0){
            $error = "No se encontró ningun producto con ese SKU";
            return view('frontend.products.search_result', ['products' => null, 'error' => $error]);

        }

        return view('frontend.products.search_result', ['products' => $products]);
    }

}
