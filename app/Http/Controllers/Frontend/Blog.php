<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Ad;
use App\Models\Revista;
use App\Models\CategoriePost;

class Blog extends Controller
{
    //
    public function all()
    {
        $data['posts'] = Post::where('estado', 'publicado')->orderBy('created_at', 'desc')->paginate(24);
        $data['revistas'] = Revista::take(5)->orderBy('id', 'desc')->get();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'blog'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['categories'] = CategoriePost::all();
        $data['cats'] = $data['categories'];
    	return view('frontend.blog', $data);
    }

    public function blogCategoryPage($categorySlug)
    {
        $data['category'] = CategoriePost::where('slug', $categorySlug)->first();
        $data['posts'] = Post::where('categoria', $data['category']->id)->orderBy('created_at', 'desc')->paginate(24);
        $data['revistas'] = Revista::take(5)->orderBy('id', 'desc')->get();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'blog'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();
        

        return view('frontend.blog_category', $data);
    }

    public function singlePost($category, $slug)
    {

    	$data['post'] = Post::where('slug', $slug)->first();
        $data['otros'] = Post::where('estado', 'publicado')->where('id', '!=', $data['post']->id)->inRandomOrder()->take(3)->get();
        $data['revistas'] = Revista::take(5)->orderBy('id', 'desc')->get();
        $data['cats'] = CategoriePost::all();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'blog'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        // Si es que esta como borrador solo le damos permiso para verlo si es que es usuario editor y admin 
        if($data['post']->estado == "borrador"){
            if(!Auth::user()){
                return response('No post found', 404);
            }elseif(!Auth::user()->hasAnyRole(['editor', 'admin'])){
                return response('No post found', 404);
            }
            
        }
    	return view('frontend.single', $data);
    }
}
