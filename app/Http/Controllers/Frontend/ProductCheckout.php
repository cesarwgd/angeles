<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Culqi\Culqi;
use Culqi\Error;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Stevebauman\Location\Facades\Location;
use App\Models\ProductOrder;
use App\Models\Product;
use App\Events\ProductPaymentSubmitted;

class ProductCheckout extends Controller
{
    public function checkout()
    {
    	$data['user'] = Auth::user();
        $data['hasOrders'] = true;
        $data['orders'] = ProductOrder::where('user_id', $data['user']->id)->where('status', 'iniciada')->get();        
        $total = 0.00;          
        
        // vemos si es que tiene algo en el shopping cart sino, no hay que mostrarle ningun formulario
        if(count($data['orders']) == 0){
            $data['hasOrders'] = null;
            return view('frontend.user.product_checkout', $data);
        }
        
        // SI ES QUE TIENE ALGO EN EL SHOPPING CART COJEMOS LOS IDS DE LOS PRODUCTOS Q VA A COMRPAR PARA NO INCLUIRLOS EN LA COLECCION DE OTROS PRODUCTOS SUGERIDOS 
        $not_this_ids = [];       

        foreach($data['orders'] as $order){
            // SUMAMOS EL COSTO TOTAL DE LA COMPRA
            $total = $total + $order->amount; //($order->amount + $order->delivery_amount);
            // INCLUIMOS EL ID DE CADA REVISTA AGREGADA AL CART EN EL ARRAY DE IDS A NO INCLUIR EN LOS OTRAS SUGERENCIAS
            $not_this_ids[] = $order->product_id;
        }

       

        $data['total'] = $total;
        // CREAMOS LA COLECCION DE OTRAS REVISTAS SUGERIDAS PARA QUE COMPRE
        $data['otras'] = Product::whereNotIn('id', $not_this_ids)->inRandomOrder()->take(4)->get();


    	return view('frontend.user.product_checkout', $data);
    }

    // AGREGAR ORDEN AL CARRITO DE COMPRAS

    public function addIssueOrder(Request $request)
    {
        // revisar que no pueda agregar la misma revista, osea comprar la misma revista si es que ya la tiene
        $order = new ProductOrder();
        $product = Product::findOrFail($request['product_id']);

        // revisamos que no tenga ya esa revista en su lista personal O AGREGADA AL SHOPPING CART aca si puede agregar el mismo producto a su shopping cart todas las veces q quiera
        /*if($order->hasIssueOrdered(Auth::user()->id, $revista->id)){
            $already = "Usted ya tiene esta revista comprada o agregada a su carrito de compras. Por favor eliga otra revista";
            return redirect()->back()->with(['already' => $already]);
        }*/

        // si no tiene esa revista ya comprada o agregada a su carrito de compras procedemos con el flujo

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['amount'] = $product->price * $request['quantity'];
        $data['delivery_amount'] = $product->delivery_price * $request['quantity'];
        $data['description'] = $product->name;
        // Tallas y colores
        if($request->has('size')){
            $data['talla'] = $request['size'];
        }
        if($request->has('color')){
            $data['color'] = $request['color'];
        }
        
        
        // OBTENEMOS LA UBICACION DEL USUARIO POR MEDIO DE SU IP Y HACIENDO UN LLAMADO A LA BD DE MAXMIND EN NUESTRO SERVER
        $user_ip = Location::get($request->ip());
        $data['country'] = $user_ip->countryName;

        // Creamos la orden
        $order->placeOrder($data);

        $exito = true;
       
        return redirect()->route('user.product.checkout');
    }

    // REMOVE ORDER FROM SHOPPING CART

    public function removeFromCart(Request $request)
    {
        $this->validate($request, [
            'orden-id' => 'required|integer'
        ]);

        $order = ProductOrder::findOrFail($request['orden-id']);
        $order->delete();

        $exito = "Orden removida de su carrito de compras";

        return redirect()->back()->with(['exito' => $exito]);
    }

   

    /*
    *
    *   CULQI PAYMENT
    *
    *
    */

    public function postCulqiPayment(Request $request)
    {

        $this->validate($request, [
            'phone_number' => 'required|max:15'
        ]);

        // se hace el cargo de culqi - METERLO EN UN TRY AND CATCH para mostrar errores en caso los haya al momento de hacer el cobro

        //$SECRET_KEY = "sk_test_g100ek67l0cfuix6";
        $SECRET_KEY = config('services.culqi.secret');
        $culqi = new Culqi(array('api_key' => $SECRET_KEY));
        
        

        // realizamos el cargo de culqi

        try{
            $charge = $culqi->Charges->create(
            array(
                  "amount" => $request['culqiAmount'],
                  "capture" => true,
                  "currency_code" => "PEN",
                  "description" => substr($request['culqiDescription'], 0, 80),
                  "email" => $request['culqiEmail'],                  
                  "installments" => 0,              
                  "source_id" => $request['culqiToken'],
                  "antifraud_details" => [
                        "address" => $request['direccion'],
                        "address_city" => $request['ciudad'],                                        
                        "phone_number" => $request['phone_number'],
                    ],
                )
            );
        }catch(\Exception $e){
            $ejson = $e->getMessage();
            $error = json_decode($ejson, true);

            return redirect()->back()->with(['errorCulqi' => $error['merchant_message']]);
            
        }

        // se realizo el cargo con exito
        $user = Auth::user();

        // actualizar en la bd de productorder el shipping address - esto por el momento no usarlo
        $data['shipping_address'] = $request['direccion'] . ", " . $request['ciudad'] . ", " . $request['provincia'] . ", " . $request['postal_code'] . ", " . $request['country'];

        foreach ($request['order_id'] as $order) {
        	$update_order = ProductOrder::findOrFail($order);
        	$update_order->status = "completada";
        	$update_order->shipping_address = $data['shipping_address'];
        	$update_order->contact_phone_number = $request['phone_number'];
        	$update_order->update();
        }

        $data = $request->all();
        $data['nombres'] = $user->name . " " . $user->last_name;
        $data['emailAmount'] = $request['culqiAmount'] / 100;

        event(new ProductPaymentSubmitted($data));
        
        $exito = "Compra realizada con éxito. Hemos sido notificados de su orden y la estamos preparando. Por favor revise su correo a donde le hemos enviado su recibo con mayores detalles.";

        return redirect()->route('user.ordenes')->with(['exito' => $exito]);
        
    }
}
