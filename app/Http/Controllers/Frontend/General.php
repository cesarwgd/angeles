<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\WebContactMessage;
use GuzzleHttp\Client;
use App\Models\Angel;
use App\Models\Banner;
use App\Models\Revista;
use App\Models\Home;
use App\Models\Post;
use App\Models\Ad;
use App\Models\CategoriePost;

class General extends Controller
{
    //
    public function home()
    {
        $data['banners'] = Banner::orderBy('position', 'asc')->get();
        $data['boxes'] = Home::orderBy('position', 'asc')->get();
        $data['posts'] = Post::where('estado', 'publicado')->orderBy('id', 'desc')->take(4)->get();
        $data['angeles'] = Angel::orderBy('id', 'desc')->take(6)->get();
        $data['cats'] = CategoriePost::all();
        $data['ad'] = Ad::where([                
                ['location', '=', 'homepage'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();

    	return view('frontend.homepage', $data);
    }

    // mantenimiento
    public function mantenimiento()
    {
        $data['cats'] = CategoriePost::all();
        return view('frontend.mantenimiento', $data);
    }

    // NOSOTROS

    public function nosotros()
    {
        $data['cats'] = CategoriePost::all();
    	return view('frontend.nosotros', $data);
    }

    // CONTACTO

    public function contacto()
    {
        $data['cats'] = CategoriePost::all();
    	return view('frontend.contacto', $data);
    }

    //  AYUDA

    public function ayuda()
    {
        $data['cats'] = CategoriePost::all();
        return view('frontend.ayuda', $data);
    }

    public function postContacto(Request $request)
    {
    	$this->validate($request, [
                'nombres' => 'required|max:255',                
                'email' => 'required|email|max:255',
                'phone' => 'required|max:255',
                'subject' => 'required|max:255',
                'mensaje' => 'required'                
            ]);

        $captchaToken = $request['g-recaptcha-response'];

        if($captchaToken){

            $client = new Client();
            $response = $client->post("https://www.google.com/recaptcha/api/siteverify", [
                'form_params' => array(
                    'secret' => '6Lejhz4UAAAAAGxOL9L0xxS4lmZSH_XmwgMSQdmk',
                    'response' => $captchaToken 
                    )
                ]);

            $result = json_decode($response->getBody()->getContents());

            if($result->success){

                $data = $request->all();

                Mail::to('angelesrevista@gmail.com')->send(new WebContactMessage($data));

                $exito = "Mensaje enviado con éxito. Le responderemos a la brevedad posible. Gracias";

                return redirect()->back()->with(['exito' => $exito]);

            }else{

                $captcha_error = $result->error_codes;

                return redirect()->back()->with(['captcha_error' => $captcha_error]);
            
            }
            

        }else{
            return redirect()->back()->with(['captcha_error' => 'Debe completar el Captcha']);
        }
    }

    // HOT MAX

    public function hotMax()
    {

        $data['angeles'] = Angel::orderBy('created_at', 'desc')->paginate(16);
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'hot'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();

        return view('frontend.hotmax.hotmax', $data);
    }

    public function hotAngel($slug)
    {
        $data['angel'] = Angel::where('slug', $slug)->first();
        $id = $data['angel']->id;
        $data['otros'] = Angel::whereNotIn('id', [$id])->take(3)->get();
        $data['revistas'] = Revista::take(5)->orderBy('id', 'desc')->get();
        $data['ad'] = Ad::where([
                ['type', '=', 'image'],
                ['location', '=', 'hot'],
                ['active', '=', '1'],
            ])->inRandomOrder()->take(1)->first();
        $data['cats'] = CategoriePost::all();

        return view('frontend.hotmax.angel', $data);
    }

    public function terminos()
    {
        $data['cats'] = CategoriePost::all();
        return view('frontend.terminos', $data);
    }


}
