<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Culqi\Culqi;
use Culqi\Error;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Stevebauman\Location\Facades\Location;
use App\Models\Order;
use App\Models\Revista;
use App\Mail\OrderBankDeposit;
use App\Events\PaymentSubmitted;


class Checkout extends Controller
{
    // MOSTRAR LA PAGINA DEL CHECKOUT

    public function checkout()
    {
    	$data['user'] = Auth::user();
        $data['hasOrders'] = true;
        $data['orders'] = Order::where('user_id', $data['user']->id)->where('status', 'iniciada')->get();        
        $total = 0.00;          
        
        // vemos si es que tiene algo en el shopping cart sino, no hay que mostrarle ningun formulario
        if(count($data['orders']) == 0){
            $data['hasOrders'] = null;
            return view('frontend.user.checkout', $data);
        }
        
        // SI ES QUE TIENE ALGO EN EL SHOPPING CART COJEMOS LOS IDS DE LAS REVISTAS Q VA A COMRPAR PARA NO INCLUIRLOS EN LA COLECCION DE OTRAS REVS   
        $not_this_ids = [];       

        foreach($data['orders'] as $order){
            // SUMAMOS EL COSTO TOTAL DE LA COMPRA
            $total = $total + $order->amount;
            // INCLUIMOS EL ID DE CADA REVISTA AGREGADA AL CART EN EL ARRAY DE IDS A NO INCLUIR EN LOS OTRAS SUGERENCIAS
            $not_this_ids[] = $order->revista_id;
        }

        // TAMBIEN TENEMOS QUE OBVIAR LOS IDS DE LAS REVISTAS QUE EL USUARIO YA TIENE
        foreach($data['user']->revistas as $revista){
            $not_this_ids[] = $revista->id;
        }

        $data['total'] = $total;
        // CREAMOS LA COLECCION DE OTRAS REVISTAS SUGERIDAS PARA QUE COMPRE
        $data['otras'] = Revista::whereNotIn('id', $not_this_ids)->inRandomOrder()->take(4)->get();


    	return view('frontend.user.checkout', $data);
    }

    // AGREGAR ORDEN AL CARRITO DE COMPRAS

    public function addIssueOrder(Request $request)
    {
        // revisar que no pueda agregar la misma revista, osea comprar la misma revista si es que ya la tiene
        $order = new Order();
        $revista = Revista::findOrFail($request['revista_id']);

        // revisamos que no tenga ya esa revista en su lista personal O AGREGADA AL SHOPPING CART
        if(Auth::user()->hasIssueById($revista->id) || $order->hasIssueOrdered(Auth::user()->id, $revista->id)){
            $already = "Usted ya tiene esta revista comprada o agregada a su carrito de compras. Por favor eliga otra revista";
            return redirect()->back()->with(['already' => $already]);
        }

        // si no tiene esa revista ya comprada o agregada a su carrito de compras procedemos con el flujo

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['amount'] = $revista->price;
        $data['description'] = $revista->name;
        // OBTENEMOS LA UBICACION DEL USUARIO POR MEDIO DE SU IP Y HACIENDO UN LLAMADO A LA BD DE MAXMIND EN NUESTRO SERVER
        $user_ip = Location::get($request->ip());
        $data['country'] = $user_ip->countryName;

        // Creamos la orden
        $order->placeOrder($data);

        $exito = true;
       
        return redirect()->route('user.checkout');
    }

    // REMOVE ORDER FROM SHOPPING CART

    public function removeFromCart(Request $request)
    {
        $this->validate($request, [
            'orden-id' => 'required|integer'
        ]);

        $order = Order::findOrFail($request['orden-id']);
        $order->delete();

        $exito = "Orden removida de su carrito de compras";

        return redirect()->back()->with(['exito' => $exito]);
    }

    /*
    *
    *
    *   BANK PAYMENT
    *
    */

    public function bankDepositCheckout(Request $request)
    {
        //validar
        $this->validate($request, [
            'nombres' => 'required|max:100',
            'email' => 'required|email|max:100',
            'dni' => 'required|max:12',
            'phone' => 'required|max:12',
            'order_code.*' => 'required|max:250',
            'order_id.*' => 'required|integer'
        ]);
        // actualizar las ordens a pendiente

        foreach($request['order-id'] as $orden){
            $order = Order::findOrFail($orden);
            $order->status = "pendiente";
            $order->update();
        }

        // configuramos todos los datos del $data
        $data = $request->all();     


        // enviar el correo con las instrucciones al usuario y al admin
        Mail::to($data['email'])->bcc('jam.web.pe@gmail.com')->send(new OrderBankDeposit($data));

        $exito = true;

        // re enviar a la pagina de exito
        return redirect()->route('user.orderplaced.bank')->with(['exito' => $exito]);
    }

    public function orderPlacedBankdeposit()
    {
    	
    	return view('frontend.user.order_placed_bank');
    }

    /*
    *
    *   CULQI PAYMENT
    *
    *
    */

    public function postCulqiPayment(Request $request)
    {
        // se hace el cargo de culqi - METERLO EN UN TRY ADN CATCH para mostrar errores en caso los haya al momento de hacer el cobro

        //$SECRET_KEY = "sk_test_g100ek67l0cfuix6";
        $SECRET_KEY = config('services.culqi.secret');
        $culqi = new Culqi(array('api_key' => $SECRET_KEY));
        
        // realizamos el cargo de culqi

        try{
            $charge = $culqi->Charges->create(
            array(
                  "amount" => $request['culqiAmount'],
                  "capture" => true,
                  "currency_code" => "PEN",
                  "description" => "Revista Digital", //$request['culqiDescription'],
                  "email" => $request['culqiEmail'],
                  "installments" => 0,              
                  "source_id" => $request['culqiToken']
                )
            );
        }catch(\Exception $e){
            $ejson = $e->getMessage();
            $error = json_decode($ejson, true);

            return redirect()->back()->with(['errorCulqi' => $error['merchant_message']]);
            
        }

        // se realizo el cargo con exito
        $user = Auth::user();

        $data = $request->all();
        $data['nombres'] = $user->name . " " . $user->last_name;
        $data['emailAmount'] = $request['culqiAmount'] / 100;

        event(new PaymentSubmitted($data));
        
        $exito = "Compra realizada con éxito. Disfrute de las revistas compradas.";

        return redirect()->route('user.revistas')->with(['exito' => $exito]);
        
    }

    // get the order data
    public function culqiPostPaymentActions($data)
    {
        
    }
}
