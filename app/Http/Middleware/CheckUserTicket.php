<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Ticket;

class CheckUserTicket
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ticket = Ticket::findOrFail($request->route('id'));

        if($request->user()->id == $ticket->user_id){
            return $next($request);
        }
        
        return redirect()->route('home');

    }
}
