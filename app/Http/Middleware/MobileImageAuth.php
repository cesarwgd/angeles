<?php

namespace App\Http\Middleware;

use Closure;

class MobileImageAuth
{
    /**
     * Handle an incoming request. MIDDLEWARE PARA CADA FOTO DE LAS REVISTAS
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $roles = ['premium', 'admin'];

    public function handle($request, Closure $next)
    {
        if($request->user() === null){

            return response('Permiso no autorizadoooo', 402);

        }elseif($request->user()->hasAnyRole($this->roles) || $request->user()->hasIssueById($request->route('id'))){
            return $next($request);
        }

        return response('Permiso no autorizado', 402);
    }
}
