<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class MobileVideoAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $roles = ['premium', 'admin'];

    public function handle($request, Closure $next)
    {

        $user = User::findOrFail($request->route('userId'));

        if($user->hasAnyRole($this->roles) || $user->hasIssueById($request->route('revistaId'))){
            return $next($request);
        }

        

        return response('Permiso no autorizado', 401);
    }
}
