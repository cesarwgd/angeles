<?php

namespace App\Http\Middleware;

use Closure;

class IssueAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    private $roles = ['premium', 'admin'];

    public function handle($request, Closure $next)
    {

        if($request->user() === null){

            return redirect()->route('login');

        } elseif($request->user()->hasAnyRole($this->roles) || $request->user()->hasIssue($request->route('hash_slug'))){
            return $next($request);
        }

        return response('Permiso no autorizado', 401);

        
    }
}
