<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class CheckCmsAuth
{
    /**
     * Handle an incoming request. Permisos para Accesos al CMS
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $roles = ['angel', 'editor', 'admin'];


    public function handle($request, Closure $next)
    {
        if(! Auth::check()){

            return redirect()->route('backend.login');

        } else {
            
            if(Auth::user()->hasAnyRole($this->roles)){
                return $next($request);
            }
        } 

        return redirect()->route('home');

        
    }
}
