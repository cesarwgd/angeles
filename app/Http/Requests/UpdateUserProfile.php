<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        if($this->user() != null){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->user()->id;

        return [
            'name' => 'sometimes|max:100',
            'last_name' => 'sometimes|max:100',
            'email' => "required|unique:users,email,$id",
            'password' => 'sometimes|nullable|min:6',
            'confirm-password' => 'required_with:password|same:password'
        ];
    }
}
