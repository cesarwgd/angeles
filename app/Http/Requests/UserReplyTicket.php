<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Ticket;

class UserReplyTicket extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $ticket = Ticket::findOrFail($this->route('id'));

        if($this->user()->id == $ticket->user_id){
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'detalle' => 'required|max:500',
            'user_id' => 'required',
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'ticket_id' => 'required',
            'tipo' => 'required',
            'asunto' => 'required'
        ];
    }
}
