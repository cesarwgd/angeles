<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    private $roles = ['editor', 'admin'];

    public function authorize()
    {
        if($this->user()->hasAnyRole($this->roles)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'required|max:255',
            'meta_title' => 'sometimes|max:255',
            'meta_keywords' => 'sometimes|max:255',
            'meta_description' => 'sometimes|max:255',
            'estado' => 'required',
            'categoria' => 'required|integer',
            'imagen_destacada' => 'sometimes|image'
        ];
    }
}
