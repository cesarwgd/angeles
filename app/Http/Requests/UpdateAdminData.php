<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    private $role = "admin";

    public function authorize()
    {
        if($this->user()->hasRole($this->role)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');

        return [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => "required|unique:users,email,$id",
            'password' => 'sometimes|nullable|min:6',
            'profile_pic' => 'sometimes|image|max:5000'           
        ];
    }
}
