<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // many to mny relationship with role model

    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    
    //has many relationship with revistas model

    public function revistas()
    {
        return $this->belongsToMany('App\Models\Revista')->withTimestamps();
    }

    // ORDERS

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function hasIncompleteOrders()
    {
        $ordenes = $this->orders()->where('status', 'iniciada')->get();
        
        if(count($ordenes) > 0){
            return true;
        }

        return false;

    }


    // PRODUCT ORDER

    public function productOrders()
    {
        return $this->hasMany('App\Models\ProductOrder');
    }

    public function hasIncompleteProductOrders()
    {
        $product_ordenes = $this->productOrders()->where('status', 'iniciada')->get();
        
        if(count($product_ordenes) > 0){
            return true;
        }

        return false;

    }

    // TOTAL ORDERS
    public function newRevistasOrdersTotal()
    {
        $orders = $this->orders()->where('status', 'iniciada')->get();
        $total = count($orders);

        if($total == 0){
            return null;
        }

        return " - " . $total;
    }
    public function newProductsOrdersTotal()
    {
        $product_orders = $this->productOrders()->where('status', 'iniciada')->get();
        $total = count($product_orders);

        if($total == 0){
            return null;
        }

        return " - " . $total;
    }


    public function newOrdersTotal()
    {
        $orders = $this->orders()->where('status', 'iniciada')->get();
        $product_orders = $this->productOrders()->where('status', 'iniciada')->get();
        $total = count($orders) + count($product_orders);

        if($total == 0){
            return null;
        }

        return " " . $total;
    }


    // TICKETS
    public function tickets()
    {
        return $this->hasMany('App\Models\Ticket');
    }

    // has Any Role method

    public function hasAnyRole($roles)
    {
        if(is_array($roles)){
            foreach ($roles as $role) {
                if($this->hasRole($role)){
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }

        return false;
    }

    // has Role method

    public function hasRole($role)
    {
        if($this->roles()->where('nombre', $role)->first()){
            return true;
        }

        return false;

    }
    

    // HAS ISSUE BY HASH SLUG 

    public function hasIssue($hash_slug)
    {
        if($this->revistas()->where('hash_slug', $hash_slug)->first()){
            return true;
        }
        
        return false;
    }

    // has Issue by ID

    public function hasIssueById($revista_id)
    {
        $hasIssue = DB::table('revista_user')->where('user_id', $this->id)->where('revista_id', $revista_id)->get();

        if(count($hasIssue) > 0){
            return true;
        }
        
        return false;
    }



    // Store Image
    public function storeImage($image, $id)
    {
        if(!empty($this->profile_pic))
        {
            Storage::disk('users')->delete($this->profile_pic);      
        }

        $main_image_name = $image->getClientOriginalName();     

        $image_full_name = $id . time() . '-' . $main_image_name;

        Storage::disk('users')->put($image_full_name, file_get_contents($image));        

        return $image_full_name;
    }

}
