<?php

namespace App\Listeners;

use App\Events\PaymentSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\CulqiPaymentSubmitted;

class SendReceipt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentSubmitted  $event
     * @return void
     */
    public function handle(PaymentSubmitted $event)
    {
        $order = $event->order;
        Mail::to($order['culqiEmail'])->bcc('ventas@angelesrevista.com')->send(new CulqiPaymentSubmitted($order));
    }
}
