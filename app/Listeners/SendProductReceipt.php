<?php

namespace App\Listeners;

use App\Events\ProductPaymentSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\ProductCulqiPaymentSubmitted;

class SendProductReceipt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductPaymentSubmitted  $event
     * @return void
     */
    public function handle(ProductPaymentSubmitted $event)
    {
        $order = $event->order;
        Mail::to($order['culqiEmail'])->bcc('ventas@angelesrevista.com')->send(new ProductCulqiPaymentSubmitted($order));
    }
}
