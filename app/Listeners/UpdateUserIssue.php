<?php

namespace App\Listeners;

use App\Events\PaymentSubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Models\Revista;
use App\Models\Order;

class UpdateUserIssue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentSubmitted  $event
     * @return void
     */
    public function handle(PaymentSubmitted $event)
    {
        $data = $event->order;
        $user = Auth::user();

        $ordenes = $data['order_id'];
        $revistas = Revista::whereIn('id', $data['revistas_id'])->get();

        // le asignamos las revistas compradas al usuario
        //  REVISAR QUE NO LE ASIGNE LA REVISTA SI ES QUE YA LA TIENE
        foreach($revistas as $revista){
            $user->revistas()->attach($revista);
        }
        //actualizamos el status de la revista / debi poner esto en otro listener per ya q xu
        foreach($ordenes as $orden){
            $order = Order::findOrFail($orden);
            $order->status = "completada";
            $order->update();
        }

    }
}
